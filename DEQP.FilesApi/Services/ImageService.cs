﻿using DEQP.Core.Shared;
using DEQP.FilesApi.Interfaces;
using DEQP.FilesApi.Resource;
using DEQP.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.FilesApi.Services
{
    public class ImageService : IImageService
    {
        IImageStorageService imageStorageService;
        public ImageService(IImageStorageService imageStorageService)
        {
            this.imageStorageService = imageStorageService;
        }

        public ApiRequestInfo ApiRequestInfo { get; set; }

        public StorageResponse GetNewsCover(string filename)
        {
            return imageStorageService.GetNewsCover(filename);
        }

        public StorageResponse GetOfficerImage(string filename)
        {
            return imageStorageService.GetOfficerImage(filename);
        }

        public StorageResponse GetProjectCover(string filename)
        {
            return imageStorageService.GetProjectCover(filename);
        }
    }
}
