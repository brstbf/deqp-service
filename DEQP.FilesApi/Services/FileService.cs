﻿using DEQP.App.Interfaces;
using DEQP.Core.Shared;
using DEQP.FilesApi.Interfaces;
using DEQP.FilesApi.Resource;
using DEQP.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.FilesApi.Services
{
    public class FileService : IFileService
    {
        IFileStorageService fileStorageService;
        INewsService newsService;
        IProjectService projectService;
        public ApiRequestInfo ApiRequestInfo { get; set; }

        public FileService(IFileStorageService fileStorageService, INewsService newsService, IProjectService projectService)
        {
            this.newsService = newsService;
            this.fileStorageService = fileStorageService;
            this.projectService = projectService;
        }

        public StorageResponse GetNewsFile(string filename)
        {
            var fileResp = fileStorageService.GetNewsFile(filename);
            newsService.AddCountFile(filename, ApiRequestInfo.IpAddress);
            return fileResp;
        }

        public StorageResponse GetProjectFile(string filename)
        {
            var fileResp = fileStorageService.GetProjectFile(filename);
            newsService.AddCountFile(filename, ApiRequestInfo.IpAddress);
            return fileResp;
        }

        public StorageResponse GetDivisionFile(string filename)
        {
            var fileResp = fileStorageService.GetDivisionFile(filename);
            newsService.AddCountFile(filename, ApiRequestInfo.IpAddress);
            return fileResp;
        }
    }
}
