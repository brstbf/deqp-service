﻿using DEQP.FilesApi.Interfaces;
using DEQP.FilesApi.Resource;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.FilesApi.EventsHandler
{
    public class ApiRequestHandler<TService> : IApiRequestHandler<TService> where TService : class, IApiService
    {
        ApiRequestInfo info;
        ILogger<ApiRequestHandler<TService>> logger;
        TService service;
        public ApiRequestHandler(ILogger<ApiRequestHandler<TService>> logger, TService service)
        {
            this.logger = logger;
            this.service = service;
        }

        public void SetRequestInfo(ApiRequestInfo info)
        {
            this.info = info;
            service.ApiRequestInfo = info;
        }

        public TResule SendCommand<TResule>(string commandName, Func<TService, TResule> func)
        {
            var requestId = Guid.NewGuid().ToString();
            logger.LogInformation($"[COMMAND] [{requestId}] - [{commandName}] Incomming IpAddress : {info?.IpAddress}");
            try
            {
                logger.LogInformation($"[COMMAND] [{requestId}] - [{commandName}] Recive API Request");
                var result = func(service);
                return result;
            }
            catch (Exception e)
            {
                logger.LogError($"[COMMAND] [{requestId}] - [{commandName}] Exception");
                logger.LogError($"[COMMAND] [{requestId}] - [{commandName}] {e.ToString()}");
                throw;
            }
            finally
            {
                logger.LogInformation($"[COMMAND] [{requestId}] - [{commandName}] Finished API Request");
            }
        }
    }
}
