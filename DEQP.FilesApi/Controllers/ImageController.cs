﻿using DEQP.Core.Shared;
using DEQP.FilesApi.Interfaces;
using DEQP.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.FilesApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ImageController : ApiController<IImageService>
    {
        public ImageController(IApiRequestHandler<IImageService> requestHandler) : base(requestHandler)
        {

        }

        [ResponseCache(Duration = 2628000,Location = ResponseCacheLocation.Any)]
        [HttpGet]
        [Route("ProjectCover/{name}")]
        public IActionResult GetProjectCover(string name)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(name))
                    return NotFound();
                var resp = GetRequestHandler().SendCommand("Get Project Cover", s => s.GetProjectCover(name));

                if (resp.Stream == null || resp.Stream.Length == 0)
                    return NotFound();

                var defaultMime = "image/jpeg";
                var mime = resp.MimeType == "application/octet-stream" ? defaultMime : resp.MimeType;
                return File(resp.Stream, mime ?? defaultMime);
            }
            catch
            {
                
                return NotFound();
            }
        }

        [ResponseCache(Duration = 2628000, Location = ResponseCacheLocation.Any)]
        [HttpGet]
        [Route("NewsCover/{name}")]
        public IActionResult GetNewsCover(string name)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(name))
                    return NotFound();
                var resp = GetRequestHandler().SendCommand("Get News Cover", s => s.GetNewsCover(name));

                if (resp.Stream == null || resp.Stream.Length == 0)
                    return NotFound();

                var defaultMime = "image/jpeg";
                var mime = resp.MimeType == "application/octet-stream" ? defaultMime : resp.MimeType;
                return File(resp.Stream, mime ?? defaultMime);
            }
            catch
            {
                return NotFound();
            }
        }

        [ResponseCache(Duration = 2628000, Location = ResponseCacheLocation.Any)]
        [HttpGet]
        [Route("OfficerProfile/{name}")]
        public IActionResult GetOfficerProfile(string name)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(name))
                    return NotFound();
                var resp = GetRequestHandler().SendCommand("Get Officer Profile", s => s.GetOfficerImage(name));

                if (resp.Stream == null || resp.Stream.Length == 0)
                    return NotFound();

                var defaultMime = "image/jpeg";
                var mime = resp.MimeType == "application/octet-stream" ? defaultMime : resp.MimeType;
                return File(resp.Stream, mime ?? defaultMime);
            }
            catch
            {
                return NotFound();
            }
        }
    }
}
