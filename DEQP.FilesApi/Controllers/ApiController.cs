﻿using DEQP.FilesApi.Interfaces;
using DEQP.FilesApi.Resource;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.FilesApi.Controllers
{
    public class ApiController<TService> : ControllerBase where TService : class
    {
        IApiRequestHandler<TService> requestHandler;
        protected ApiController(IApiRequestHandler<TService> requestHandler)
        {
            this.requestHandler = requestHandler;
        }
        protected IApiRequestHandler<TService> GetRequestHandler()
        {
             requestHandler.SetRequestInfo(GetRequestInfo());
            return requestHandler;
        }

        protected ApiRequestInfo GetRequestInfo()
        {
            return new ApiRequestInfo
            {
                IpAddress = HttpContext.Connection.RemoteIpAddress.ToString()
            };
        }
    }
}
