﻿using DEQP.Core.Shared;
using DEQP.FilesApi.Interfaces;
using DEQP.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.FilesApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FileController : ApiController<IFileService>
    {
        public FileController(IApiRequestHandler<IFileService> requestHandler) : base(requestHandler)
        {

        }

        const string DEFAULT_MIME = "application/octet-stream";

        [ResponseCache(Duration = 2628000, Location = ResponseCacheLocation.Any)]
        [HttpGet]
        [Route("ProjectFile/{name}")]
        public IActionResult GetProjectFile(string name)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(name))
                    return NotFound();
                var resp = GetRequestHandler().SendCommand("Get Project File", s => s.GetProjectFile(name));

                if (resp.Stream == null || resp.Stream.Length == 0)
                    return NotFound();

                var mime = resp.MimeType == "application/octet-stream" ? DEFAULT_MIME : resp.MimeType;
                return File(resp.Stream, mime ?? DEFAULT_MIME);
            }
            catch
            {
                return NotFound();
            }
        }

        [ResponseCache(Duration = 2628000, Location = ResponseCacheLocation.Any)]
        [HttpGet]
        [Route("NewsFile/{name}")]
        public IActionResult GetNewsFile(string name)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(name))
                    return NotFound();
                var resp = GetRequestHandler().SendCommand("Get News File", s => s.GetNewsFile(name));

                if (resp.Stream == null || resp.Stream.Length == 0)
                    return NotFound();

                var mime = resp.MimeType == "application/octet-stream" ? DEFAULT_MIME : resp.MimeType;
                return File(resp.Stream, mime ?? DEFAULT_MIME);
            }
            catch
            {
                return NotFound();
            }
            
        }

        [ResponseCache(Duration = 2628000, Location = ResponseCacheLocation.Any)]
        [HttpGet]
        [Route("DivisionFile/{name}")]
        public IActionResult DivisionFile(string name)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(name))
                    return NotFound();
                var resp = GetRequestHandler().SendCommand("Get Division File", s => s.GetDivisionFile(name));

                if (resp.Stream == null || resp.Stream.Length == 0)
                    return NotFound();

                var mime = resp.MimeType == "application/octet-stream" ? DEFAULT_MIME : resp.MimeType;
                return File(resp.Stream, mime ?? DEFAULT_MIME);
            }
            catch
            {
                return NotFound();
            }

        }
    }
}
