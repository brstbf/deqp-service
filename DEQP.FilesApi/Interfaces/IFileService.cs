﻿using DEQP.Core.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.FilesApi.Interfaces
{
    public interface IFileService:IApiService
    {
        StorageResponse GetNewsFile(string filename);
        StorageResponse GetProjectFile(string filename);
        StorageResponse GetDivisionFile(string filename);

    }
}
