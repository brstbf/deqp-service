﻿using DEQP.Core.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.FilesApi.Interfaces
{
    public interface IImageService : IApiService
    {
        StorageResponse GetProjectCover(string filename);
        StorageResponse GetNewsCover(string filename);
        StorageResponse GetOfficerImage(string filename);
    }
}
