﻿using DEQP.FilesApi.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.FilesApi.Interfaces
{
    public interface IApiRequestHandler<TService> where TService : class
    {
        void SetRequestInfo(ApiRequestInfo info);
        TResule SendCommand<TResule>(string commandName, Func<TService, TResule> func);
    }
}
