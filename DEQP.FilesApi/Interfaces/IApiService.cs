﻿using DEQP.FilesApi.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.FilesApi.Interfaces
{
    public interface IApiService
    {
        ApiRequestInfo ApiRequestInfo { get; set; }
    }
}
