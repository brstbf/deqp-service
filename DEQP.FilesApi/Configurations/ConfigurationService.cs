﻿using DEQP.App.Interfaces;
using DEQP.App.Services;
using DEQP.Core.Domain;
using DEQP.Core.Domain.DivisionAggregate;
using DEQP.Core.Domain.NewsAggregate;
using DEQP.Core.Domain.ProjectsAggregate;
using DEQP.Core.Interfaces;
using DEQP.Core.Shared;
using DEQP.FilesApi.EventsHandler;
using DEQP.FilesApi.Interfaces;
using DEQP.FilesApi.Services;
using DEQP.Infrastructure.DBContext;
using DEQP.Infrastructure.Interfaces;
using DEQP.Infrastructure.Services;
using DEQP.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.FilesApi.Configurations
{
    public static class ConfigurationService
    {
        public static void AddFileApiService(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<StorageSettings>(configuration.GetSection("StorageSettings"));

            #region Infra
            services.AddDbContext<DEQPContext>(o =>
            {
                o.UseSqlServer(configuration.GetConnectionString("DEQPConnectionString"));
            });


            services.AddSingleton<IStorageService, StorageService>();
            services.AddSingleton<IImageStorageService, ImageStorageService>();
            services.AddSingleton<IFileStorageService, FileStorageService>();

            services.AddScoped<IFileService, FileService>();
            services.AddScoped<IImageService, ImageService>();
            services.AddScoped<INewsService, NewsService>();
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<IDEQPUnitOfWork, DEQPUnitOfWork>();
            services.AddScoped<IDeqpRepository<News>, DeqpRepository<News>>();
            services.AddScoped<IDeqpRepository<Project>, DeqpRepository<Project>>();
            services.AddScoped<IDeqpRepository<NewsCategory>, DeqpRepository<NewsCategory>>();
            services.AddScoped<IDeqpRepository<Division>, DeqpRepository<Division>>();
            services.AddScoped<IDivisionService, DivisionService>();

            services.AddScoped(typeof(IApiRequestHandler<>), typeof(ApiRequestHandler<>));

            #endregion

            services.AddAutoMapper(o =>
            {

            });
        }
    }
}
