﻿using AutoMapper;
using DEQP.App.Resources.Projects;
using DEQP.PublicAPI.Resources.Projects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.MapperProfiles
{
    public class ProjectApiProfile : Profile
    {
        public ProjectApiProfile()
        {
            CreateMap<ProjectSearchApiRequest, SearchProjectRequest>();
            CreateMap<ProjectSearchData, ProjectSearchApiResponse>()
                .ForMember(des => des.CreatedByFullname, o => o.MapFrom(s => s.DivisionName))
                .ForMember(des => des.Title, o => o.MapFrom(s => s.Name));

            CreateMap<ProjectSearchData, ProjectDashboardApiResponse>()
                .ForMember(des => des.Title, o => o.MapFrom(s => s.Name));

            CreateMap<ProjectData, ProjectGetApiResponse>()
                .ForMember(f => f.Preface, o => o.MapFrom(s => s.Content))
                .ForMember(f => f.Url1, o => o.MapFrom(s => s.ProjectUrl))
                .ForMember(f => f.Url2, o => o.MapFrom(s => s.RegisterUrl))
                .ForMember(des => des.CreatedByFullname, o => o.MapFrom(s => s.DivisionName))
                .ForMember(des => des.Title, o => o.MapFrom(s => s.Name));

            CreateMap<ProjectNewsData, ProjectNewsGetApiResponse>()
                .ForMember(f => f.Detail, o => o.MapFrom(s => s.Preface));
            CreateMap<ProjectFileData, ProjectFileGetApiResponse>()
                .ForMember(f => f.Size, o => o.MapFrom(s => s.FileSize))
                .ForMember(f => f.Title, o => o.MapFrom(s => s.OriginalName));
        }
    }
}
