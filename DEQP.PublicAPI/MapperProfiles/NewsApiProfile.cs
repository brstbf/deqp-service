﻿using AutoMapper;
using DEQP.App.Resources.News;
using DEQP.PublicAPI.Resources.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.MapperProfiles
{
    public class NewsApiProfile:Profile
    {
        public NewsApiProfile()
        {
            CreateMap<CreateNewsApiRequest, CreateNewsRequest>();
            CreateMap<NewsData,NewsGetApiResponse>()
                .ForMember(d => d.PrimaryCategoryId, o => o.MapFrom(s => s.PrimaryGroupId))
                .ForMember(d => d.SecondaryCategoryId, o => o.MapFrom(s => s.SecondaryGroupId))
                .ForMember(d => d.CreatedByFullname, o => o.MapFrom(s => s.DivisionName));
            CreateMap<NewsFileData, NewsFileGetApiResponse>()
                .ForMember(d => d.Title, o => o.MapFrom(s => s.OriginalName))
                .ForMember(d=>d.Size,o=>o.MapFrom(s=>s.FileSize));

            CreateMap<SearchNewsApiRequest, SearchNewsRequest>()
                .ForMember(d => d.DivisionId, o => o.MapFrom(s => s.Organization))
                .ForMember(d => d.CategoryType, o => o.MapFrom(s => s.CategoryType))
                .ForMember(d => d.CategoryId, o => o.MapFrom(s => s.Category));
            CreateMap<SearchNewsData, SearchNewsApiResponse>()
                .ForMember(d => d.Detail, o => o.MapFrom(s => s.Preface))
                .ForMember(d => d.CategoryId, o => o.MapFrom(s => s.PrimaryCategoryId))
                .ForMember(d => d.CategoryType, o => o.MapFrom(s => s.PrimartCategoryType))
                .ForMember(d => d.CategoryName, o => o.MapFrom(s => s.PrimaryCategory))
                .ForMember(d => d.CreatedByFullname, o => o.MapFrom(s => s.DivisionName));
            CreateMap<SearchLastedNewsApiRequest, SearchLastedNewsRequest>();
        }
    }
}
