﻿using DEQP.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Resources.News
{
    public class SearchNewsApiResponse
    {
        public string Img { get; set; }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Detail { get; set; }
        public string CreatedByFullname { get; set; }
        public string CreatedDate { get; set; }
        public long CreatedDateTick { get; set; }
        public int ViewCount { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public NewsCategoryType CategoryType { get; set; }
        public string ProjectName { get; set; }
        

    }
}
