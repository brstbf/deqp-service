﻿using DEQP.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Resources.News
{
    public class SearchLastedNewsApiRequest
    {
        public int? Limit { get; set; }
        public NewsCategoryType? CategoryType { get; set; }
        public int? CategoryId { get; set; }
        public int? LimitDay { get; set; }
        public int? DivisionId { get; set; }
    }
}
