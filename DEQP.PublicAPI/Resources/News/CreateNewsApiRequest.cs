﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Resources.News
{
    public class CreateNewsApiRequest:NewsApiRequestBase
    {
        public int? ProjectId { get; set; }
    }
}
