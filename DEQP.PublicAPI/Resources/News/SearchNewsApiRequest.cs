﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Resources.News
{
    public class SearchNewsApiRequest
    {
        public string Query { get; set; }
        public int? Organization { get; set; }
        public int? Year { get; set; }
        public int? Category { get; set; }
        public string CategoryType { get; set; }
        public int? Limit { get; set; }

        [FromQuery(Name ="dateRang[]")]
        public IList<string> DateRang { get; set; }
        public int? ProjectId { get; set; }
    }
}
