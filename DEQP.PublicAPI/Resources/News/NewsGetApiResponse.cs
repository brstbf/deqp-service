﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Resources.News
{
    public class NewsGetApiResponse
    {
        public string Title { get; set; }
        public string Img { get; set; }
        public string Content { get; set; }
        public string CreatedDate { get; set; }
        public IEnumerable<string> Tags { get; set; }
        public int? ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string CreatedByFullname { get; set; }
        public int ViewCount { get; set; }
        public string PrimaryCategory { get; set; }
        public int? PrimaryCategoryId { get; set; }
        public int? PrimaryType { get; set; }
        public string SecondaryCategory { get; set; }
        public int? SecondaryCategoryId { get; set; }
        public int? SecondaryType { get; set; }
        public IEnumerable<NewsFileGetApiResponse> Files { get; set; }
    }

    public class NewsFileGetApiResponse
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string TypeName { get; set; }
        public double? Size { get; set; }
        public int? OrderNo { get; set; }
        public int ViewCount { get; set; }
        public int DownloadCount { get; set; }
        public string Url { get; set; }
    }
}
