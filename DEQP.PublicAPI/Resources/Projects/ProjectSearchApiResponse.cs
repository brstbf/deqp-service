﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Resources.Projects
{
    public class ProjectSearchApiResponse
    {
        public string Title { get; set; }
        public string CreatedDate { get; set; }
        public int ViewCount { get; set; }
        public List<string> Tags { get; set; }
        public string CreatedByFullname { get; set; }
        public int Id { get; set; }
        public string Img { get; set; }
    }
}
