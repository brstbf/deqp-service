﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Resources.Projects
{
    public class ProjectGetApiResponse
    {
        public string Title { get; set; }
        public string Img { get; set; }
        public string CreatedByFullname { get; set; }
        public int Id { get; set; }
        public string Preface { get; set; }
        public string Url1 { get; set; }
        public string Url2 { get; set; }

        public ICollection<ProjectFileGetApiResponse> Files { get; set; }
        public ICollection<ProjectNewsGetApiResponse> News { get; set; }
    }

    public class ProjectFileGetApiResponse
    {
        public string Title { get; set; }
        public double? Size { get; set; }
        public int? OrderNo { get; private set; }
        public string DownloadUrl { get; set; }
        public int DownloadCount { get; set; }
    }

    public class ProjectNewsGetApiResponse
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Img { get; set; }
        public int ViewCount { get; set; }
        public string CreatedDate { get; set; }
        public string Detail { get; set; }
    }
}
