﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Resources.Projects
{
    public class ProjectSearchApiRequest
    {
        public int? Limit { get; set; }
        public string Query { get; set; }
    }
}
