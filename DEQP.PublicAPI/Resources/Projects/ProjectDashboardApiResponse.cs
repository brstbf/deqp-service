﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Resources.Projects
{
    public class ProjectDashboardApiResponse
    {
        public string Title { get; set; }
        public int Id { get; set; }
        public string Img { get; set; }
    }
}
