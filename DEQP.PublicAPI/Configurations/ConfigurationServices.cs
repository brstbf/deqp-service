﻿using DEQP.App.Interfaces;
using DEQP.App.MapperProfiles;
using DEQP.App.Services;
using DEQP.Core.Domain;
using DEQP.Core.Domain.AdminsAggregate;
using DEQP.Core.Domain.DivisionAggregate;
using DEQP.Core.Domain.NewsAggregate;
using DEQP.Core.Domain.ProjectsAggregate;
using DEQP.Core.Interfaces;
using DEQP.Core.Shared;
using DEQP.Infrastructure.DBContext;
using DEQP.Infrastructure.Shared;
using DEQP.PublicAPI.EventsHandlers;
using DEQP.PublicAPI.Interfaces;
using DEQP.PublicAPI.MapperProfiles;
using DEQP.PublicAPI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Configurations
{
    public static class ConfigurationServices
    {
        public static void AddPublicApiService(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<StorageSettings>(configuration.GetSection("StorageSettings"));

            #region Repository
            services.AddDbContext<DEQPContext>(o =>
            {
                o.UseSqlServer(configuration.GetConnectionString("DEQPConnectionString"));
            });

            services.AddScoped<IDEQPUnitOfWork, DEQPUnitOfWork>();

            services.AddScoped<IDeqpRepository<News>, DeqpRepository<News>>();
            //services.AddScoped<IEFRepository<Admin>, EFRepository<Admin>>();
            services.AddScoped<IDeqpRepository<NewsCategory>, DeqpRepository<NewsCategory>>();
            services.AddScoped<IDeqpRepository<Division>, DeqpRepository<Division>>();
            services.AddScoped<IDeqpRepository<Project>, DeqpRepository<Project>>();

            #endregion

            

            

            #region Service

            services.AddScoped<INewsService, NewsService>();
            services.AddScoped<INewsApiService, NewsApiService>();

            services.AddScoped<IDivisionService, DivisionService>();
            services.AddScoped<IDivisionApiService, DivisionApiService>();

            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<IProjectApiService, ProjectApiService>();

            services.AddScoped<IApiService, ApiServiceBase>();

            #endregion

            #region Handler

            services.AddScoped(typeof(IApiRequestHandler<>), typeof(ApiRequestHandler<>));

            #endregion

            #region Mapper

            services.AddAutoMapper(o =>
            {
                o.AddProfile<NewsProfile>();
                o.AddProfile<NewsApiProfile>();

                o.AddProfile<ProjectProfile>();
                o.AddProfile<ProjectApiProfile>();

                o.AddProfile<DivisionProfile>();
            });

            #endregion

        }
    }
}
