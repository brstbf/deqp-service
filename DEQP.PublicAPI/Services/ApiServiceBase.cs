﻿using DEQP.PublicAPI.Interfaces;
using DEQP.PublicAPI.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Services
{
    public class ApiServiceBase : IApiService
    {
        public ApiRequestInfo ApiRequestInfo { get; set; }
    }
}
