﻿using AutoMapper;
using DEQP.App.Interfaces;
using DEQP.App.Resources.News;
using DEQP.Core.Constants;
using DEQP.Core.Domain;
using DEQP.Core.Shared;
using DEQP.PublicAPI.Interfaces;
using DEQP.PublicAPI.Resources;
using DEQP.PublicAPI.Resources.News;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Services
{
    public class NewsApiService : ApiServiceBase, INewsApiService
    {
        ILogger<NewsApiService> logger;
        INewsService newsService;
        IMapper mapper;
        StorageSettings storageSettings;

        static CultureInfo thculture = CultureInfo.CreateSpecificCulture("th-TH");

        public string MapNewsCoverUrl(string fileRename)
        {
            if (string.IsNullOrWhiteSpace(fileRename))
                return string.Empty;
            return $"{storageSettings.NewsCoverUrl}/{fileRename}";
        }

        public string MapNewsFileUrl(string fileRename)
        {
            if (string.IsNullOrWhiteSpace(fileRename))
                return string.Empty;
            return $"{storageSettings.NewsFileUrl}/{fileRename}";
        }

        public NewsApiService(ILogger<NewsApiService> logger, INewsService newsService, IMapper mapper, IOptions<StorageSettings> storageSettings)
        {
            this.logger = logger;
            this.newsService = newsService;
            this.mapper = mapper;
            this.storageSettings = storageSettings.Value;
        }

        public int CreateNews(CreateNewsApiRequest request)
        {
            var data = mapper.Map<CreateNewsRequest>(request);
            var id = newsService.CreateNews(data);

            return id;
        }

        public NewsGetApiResponse GetNews(int id)
        {
            var resource = newsService.GetNewsDetail(id);
            if (resource == null)
                return null;
            newsService.AddViewCount(new AddNewsViewCountRequest
            {
                Id = id,
                IpAddress = ApiRequestInfo.IpAddress
            });

            var news = mapper.Map<NewsGetApiResponse>(resource);
            news.Img = MapNewsCoverUrl(resource.CoverFileRename);
            news.CreatedDate = resource.Date.ToThai();
            news.Tags = resource.Tags?.Split(',');

            var i = 0;
            var filesSource = resource.Files.ToArray();
            foreach(var file in news.Files)
            {
                var fullName = filesSource[i].Rename;
                if (!string.IsNullOrEmpty(file.TypeName))
                    fullName += "." + file.TypeName;
                file.Url = MapNewsFileUrl(fullName);
                i++;
            }


            return news;
        }

        public IEnumerable<SearchNewsApiResponse> SearchNews(SearchNewsApiRequest request)
        {
            if (request.Year == null)
                request.Year = DateTime.Now.Year;

            var requestData = mapper.Map<SearchNewsRequest>(request);

            if (request.DateRang != null && request.DateRang.Count > 0 && !string.IsNullOrWhiteSpace(request.DateRang[0]) && request.DateRang[0] != "null")
            {
                var a = DateTimeOffset.Parse(request.DateRang[0], thculture);
                var b = DateTimeOffset.Parse(request.DateRang[0]);
                var c = DateTimeOffset.Parse("2564-04-03", thculture);
            }
            if (request.DateRang != null && request.DateRang.Count > 1 && !string.IsNullOrWhiteSpace(request.DateRang[1]) && request.DateRang[1] != "null")
            {
                requestData.DateTo = DateTime.Parse(request.DateRang[1], thculture);
            }


            var respData = newsService.SearchNews(requestData);
            var news = mapper.Map<IEnumerable<SearchNewsData>, IEnumerable<SearchNewsApiResponse>>(respData);

            var i = 0;
            var respDataArr = respData.ToArray();
            foreach (var item in news)
            {
                item.CreatedDateTick = respDataArr[i].Date.Value.Ticks;
                item.Img = MapNewsCoverUrl(respDataArr[i].CoverFileRename);
                item.CreatedDate = respDataArr[i].Date.ToThai();
                i++;
            }

            return news;
        }

        public IEnumerable<NewsCategoryData> SearchNewsCategory(NewsCategoryType type)
        {
            var resource = newsService.GetNewsCategory(type);

            return resource;
        }

        public IEnumerable<NewsSummaryByCategoryData> GetNewsSummaryByCategory(NewsCategoryType type)
        {
            var resource = newsService.GetCategoryNewsSummary(type);
            return resource;
        }

        public IEnumerable<SearchNewsApiResponse> GetNews(SearchLastedNewsApiRequest request)
        {
            var requestData = mapper.Map<SearchLastedNewsRequest>(request);
            var respData = newsService.GetNews(requestData);
            var news = mapper.Map<IEnumerable<SearchNewsData>, IEnumerable<SearchNewsApiResponse>>(respData);

            var i = 0;
            var respDataArr = respData.ToArray();
            foreach (var item in news)
            {
                item.Img = MapNewsCoverUrl(respDataArr[i].CoverFileRename);
                item.CreatedDate = respDataArr[i].Date.ToThai();
                i++;
            }

            return news;
        }

        public IEnumerable<SearchNewsApiResponse> GetTopNews()
        {
            var respData = newsService.GetNews(new SearchLastedNewsRequest
            {
                Limit = 10,
                IsTopNews = true
            });
            var news = mapper.Map<IEnumerable<SearchNewsData>, IEnumerable<SearchNewsApiResponse>>(respData);

            var i = 0;
            var respDataArr = respData.ToArray();
            foreach (var item in news)
            {
                item.Img = MapNewsCoverUrl(respDataArr[i].CoverFileRename);
                item.CreatedDate = respDataArr[i].Date.ToThai();
                i++;
            }

            return news;
        }

        public IEnumerable<SearchNewsApiResponse> GetRelatedNews(int id)
        {
            var respData = newsService.GetReleatedNews(id);
            var news = mapper.Map<IEnumerable<SearchNewsData>, IEnumerable<SearchNewsApiResponse>>(respData);

            var i = 0;
            var respDataArr = respData.ToArray();
            foreach (var item in news)
            {
                item.Img = MapNewsCoverUrl(respDataArr[i].CoverFileRename);
                item.CreatedDate = respDataArr[i].Date.ToThai();
                i++;
            }

            return news;
        }

        public async Task<string> NatureRssFeed()
        {
            using(var client = new HttpClient())
            {
                var resp = await client.GetAsync(@"https://news.thaipbs.or.th/rss/news/environment");
                if (resp.StatusCode != System.Net.HttpStatusCode.OK)
                    return null;

                var xml = await resp.Content.ReadAsStringAsync();
                return xml;
            }
        }
    }
}
