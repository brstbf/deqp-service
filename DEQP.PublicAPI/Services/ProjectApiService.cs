﻿using AutoMapper;
using DEQP.App.Interfaces;
using DEQP.App.Resources.Projects;
using DEQP.Core.Constants;
using DEQP.Core.Shared;
using DEQP.PublicAPI.Interfaces;
using DEQP.PublicAPI.Resources.Projects;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Services
{
    public class ProjectApiService : ApiServiceBase, IProjectApiService
    {
        IProjectService projectService;
        IMapper mapper;
        StorageSettings storageSettings;
        INewsApiService newsApiService;
        public ProjectApiService(IProjectService projectService, IMapper mapper, IOptions<StorageSettings> storageSettings, INewsApiService newsApiService)
        {
            this.projectService = projectService;
            this.mapper = mapper;
            this.storageSettings = storageSettings.Value;
            this.newsApiService = newsApiService;
        }

        public string MapProjectCoverUrl(string fileRename)
        {
            if (string.IsNullOrWhiteSpace(fileRename))
                return string.Empty;
            return $"{storageSettings.ProjectCoverUrl}/{fileRename}";
        }

        public string MapProjectFileUrl(string fileRename)
        {
            if (string.IsNullOrWhiteSpace(fileRename))
                return string.Empty;

            return $"{storageSettings.ProjectFileUrl}/{fileRename}";
        }

        public ProjectGetApiResponse GetProject(int id)
        {
            var resource = projectService.GetProjectDetail(id);
            if (resource == null)
                return null;
            var project = mapper.Map<ProjectGetApiResponse>(resource);
            project.Img = MapProjectCoverUrl(resource.CoverFileRename);
            var i = 0;
            var resourceFile = resource.Files.ToArray();
            foreach(var file in project.Files)
            {
                var filename = resourceFile[i].Rename;
                if (!string.IsNullOrWhiteSpace(resourceFile[i].TypeName))
                    filename += "." + resourceFile[i].TypeName;
                file.DownloadUrl = MapProjectFileUrl(filename);
            }

            if (!resource.IsactiveProjectUrl)
                project.Url1 = null;
            if (!resource.IsactiveRegisterUrl)
                project.Url2 = null;

            var newsResource = resource.News.ToArray();
            i = 0;
            foreach(var news in project.News)
            {
                var newsRes = newsResource[i++];
                news.Img = newsApiService.MapNewsCoverUrl(newsRes.CoverFileRename);
                news.CreatedDate = newsRes.CreatedDate.ToThai();
            }

            return project;
        }

        public IEnumerable<ProjectSearchApiResponse> SearchProject(ProjectSearchApiRequest request)
        {
            var serviceRequest = mapper.Map<SearchProjectRequest>(request);

            var data = projectService.SearchProjects(serviceRequest);

            var respData = new List<ProjectSearchApiResponse>();
            var tags = new List<string>()
                {
                    "ข่าวสารโครงการ"
                };
            foreach (var item in data)
            {
                var proj = mapper.Map<ProjectSearchApiResponse>(item);

                proj.CreatedDate = item.CreatedDate.ToThai();
                proj.Tags = tags;
                proj.Img = MapProjectCoverUrl(item.CoverFileRename);

                respData.Add(proj);
            }


            return respData;
        }

        public IEnumerable<ProjectDropdownData> GetProjectDropdowns()
        {
            return projectService.GetProjectDropdowns();
        }
    }
}
