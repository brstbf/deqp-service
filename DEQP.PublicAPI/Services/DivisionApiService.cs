﻿using DEQP.App.Interfaces;
using DEQP.App.Resources.Divisions;
using DEQP.Core.Shared;
using DEQP.PublicAPI.Interfaces;
using DEQP.PublicAPI.Resources;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Services
{
    public class DivisionApiService : IDivisionApiService
    {
        IDivisionService divisionService;
        StorageSettings storageSettings;
        public DivisionApiService(IDivisionService divisionService, IOptions<StorageSettings> storageSettings)
        {
            this.divisionService = divisionService;
            this.storageSettings = storageSettings.Value;
        }

        public ApiRequestInfo ApiRequestInfo { get; set; }

        public string MapDivisionFileUrl(string filename,string type)
        {
            var file = filename;
            if (!string.IsNullOrWhiteSpace(type))
                file += "." + type;

            return storageSettings.DivisionFileUrl + "/" + file;
        }

        public string MapOfficerProfileUrl(string filename)
        {
            if (string.IsNullOrWhiteSpace(filename))
                return null;
            return $"{storageSettings.OfficerImageUrl}/{filename}";
        }

        public void MapData(DivisionData division)
        {
            foreach (var g in division.DocumentGroups)
                foreach (var file in g.DivisionFiles)
                    file.FileUrl = MapDivisionFileUrl(file.Rename, file.Type);
            foreach (var sub in division.SubDivisions)
                foreach (var off in sub.DivisionOfficers)
                    off.ImageUrl = MapOfficerProfileUrl(off.ImageRename);
        }

        public IEnumerable<DivisionData> Get()
        {
            var divisions =  divisionService.Get(new GetDivisionRequest());
            foreach(var division in divisions)
                MapData(division);                

            return divisions;
        }

        public DivisionData Get(int id)
        {
            var division = divisionService.Get(new GetDivisionRequest()
            {
                Id = id
            }).FirstOrDefault();

            if (division != null)
                MapData(division);

            return division;
        }

        public IEnumerable<DivisionDropdownData> GetDropdowns()
        {
            return divisionService.GetDropdowns();
        }
    }
}
