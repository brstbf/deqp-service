using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DEQP.PublicAPI.Configurations;
using DEQP.Infrastructure.DBContext.DEQPNews;
using Microsoft.EntityFrameworkCore;
using DEQP.PublicAPI.EventsHandlers;
using Microsoft.AspNetCore.Cors.Infrastructure;
using DEQP.Infrastructure.DBContext;
using DEQP.Core.Shared;
using DEQP.Infrastructure.Models;
using DEQP.PublicAPI.Controllers;
using Microsoft.Extensions.FileProviders;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace DEQP.PublicAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddPublicApiService(Configuration);

            services.AddControllers(o => {

                o.Filters.Add<ExceptionEventsHandler>();
            });

            services.AddScoped<DEQP.Infrastructure.Models.DEQPNEWSContext>();
            services.AddScoped<FileManager>();


            //services.AddCors(o =>
            //{
            //    o.AddPolicy("API_ORIGIN", o =>
            //    {
            //        o.WithOrigins("http://localhost:3000", "http://159.138.102.154:6969");
            //    });
            //});

            services.AddCors(o => o.AddPolicy("bypass", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));


            services.AddSwaggerGen(c =>
            {
               
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "DEQP.PublicAPI", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseSwagger();
                //app.UseSwaggerUI(c =>
                //{
                //    c.RoutePrefix = "";
                //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "DEQP.PublicAPI v1");
                //});
            }



            //app.UseHttpsRedirection();
            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\File")),
                RequestPath = new PathString("/File")
            });

            app.UseDirectoryBrowser(new DirectoryBrowserOptions()
            {
                FileProvider = new PhysicalFileProvider(
            Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\File")),
                RequestPath = new PathString("/File")
            });

            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(
             Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\Image")),
                RequestPath = new PathString("/Image")
            });

            app.UseDirectoryBrowser(new DirectoryBrowserOptions()
            {
                FileProvider = new PhysicalFileProvider(
            Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\Image")),
                RequestPath = new PathString("/Image")
            });


            app.UseRouting();

            //app.UseCors("API_ORIGIN");
            app.UseCors("bypass");

            app.UseAuthorization();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "DEQP.PublicAPI v1");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
