﻿using DEQP.PublicAPI.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Interfaces
{
    public interface IApiRequestHandler<TService> where TService: IApiService
    {
        void SetApiRequestInfo(ApiRequestInfo apiRequestInfo);
        TResule SendCommand<TResule>(string handlerName, Func<TService, TResule> func);
        void SendCommand(string handlerName, Action<TService> action);
    }
}
