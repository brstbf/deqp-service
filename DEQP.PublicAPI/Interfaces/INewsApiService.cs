﻿using DEQP.App.Resources.News;
using DEQP.Core.Domain;
using DEQP.PublicAPI.Resources.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Interfaces
{
    public interface INewsApiService : IApiService
    {
        NewsGetApiResponse GetNews(int id);
        int CreateNews(CreateNewsApiRequest request);
        IEnumerable<SearchNewsApiResponse> SearchNews(SearchNewsApiRequest request);
        IEnumerable<SearchNewsApiResponse> GetNews(SearchLastedNewsApiRequest request);
        IEnumerable<SearchNewsApiResponse> GetRelatedNews(int id);
        IEnumerable<SearchNewsApiResponse> GetTopNews();
        IEnumerable<NewsCategoryData> SearchNewsCategory(NewsCategoryType type);
        IEnumerable<NewsSummaryByCategoryData> GetNewsSummaryByCategory(NewsCategoryType type);
        string MapNewsCoverUrl(string fileRename);
        Task<string> NatureRssFeed();
    }
}
