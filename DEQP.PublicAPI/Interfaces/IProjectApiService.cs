﻿using DEQP.App.Resources.Projects;
using DEQP.PublicAPI.Resources.Projects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Interfaces
{
    public interface IProjectApiService:IApiService
    {
        IEnumerable<ProjectSearchApiResponse> SearchProject(ProjectSearchApiRequest request);
        ProjectGetApiResponse GetProject(int id);
        string MapProjectCoverUrl(string fileRename);
        string MapProjectFileUrl(string fileRename);
        IEnumerable<ProjectDropdownData> GetProjectDropdowns();
    }
}
