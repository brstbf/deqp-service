﻿using DEQP.App.Resources.Divisions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Interfaces
{
    public interface IDivisionApiService: IApiService
    {
        IEnumerable<DivisionDropdownData> GetDropdowns();
        IEnumerable<DivisionData> Get();
        DivisionData Get(int id);
    }
}
