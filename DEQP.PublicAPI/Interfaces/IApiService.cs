﻿using DEQP.PublicAPI.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Interfaces
{
    public interface IApiService
    {
        ApiRequestInfo ApiRequestInfo { get; set; }
    }
}
