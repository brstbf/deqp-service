﻿using DEQP.Core.Exception;
using DEQP.Core.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.EventsHandlers
{
    public sealed class ExceptionEventsHandler : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if(context.Exception is ApplicationServiceException || context.Exception is DomainException)
            {
                context.Result = new JsonResult(new
                {
                    message = context.Exception.Message
                })
                {
                    StatusCode = 400
                };
            }
            else
            {
                base.OnException(context);
            }
            
        }
    }
}
