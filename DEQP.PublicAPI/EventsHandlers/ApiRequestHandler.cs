﻿using DEQP.PublicAPI.Interfaces;
using DEQP.PublicAPI.Resources;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.EventsHandlers
{
    public sealed class ApiRequestHandler<TService> : IApiRequestHandler<TService> where TService : IApiService
    {
        TService service;
        ILogger<ApiRequestHandler<TService>> logger;
        public ApiRequestHandler(TService service, ILogger<ApiRequestHandler<TService>> logger)
        {
            this.service = service;
            this.logger = logger;
        }
        public TResule SendCommand<TResule>(string commandName, Func<TService, TResule> func)
        {
            var requestId = Guid.NewGuid().ToString();
            logger.LogInformation($"[COMMAND] [{requestId}] - [{commandName}] Incomming IpAddress : {service.ApiRequestInfo?.IpAddress}");
            try
            {
                logger.LogInformation($"[COMMAND] [{requestId}] - [{commandName}] Recive API Request");
                var result = func(service);
                return result;
            }
            catch (Exception e)
            {
                logger.LogError($"[COMMAND] [{requestId}] - [{commandName}] Exception");
                logger.LogError($"[COMMAND] [{requestId}] - [{commandName}] {e.ToString()}");
                throw;
            }
            finally
            {
                logger.LogInformation($"[COMMAND] [{requestId}] - [{commandName}] Finished API Request");
            }
        }

        public void SendCommand(string commandName, Action<TService> action)
        {
            var requestId = Guid.NewGuid().ToString();
            try
            {
                logger.LogInformation($"[COMMAND] [{requestId}] - [{commandName}] Recive API Request");
                action(service);
            }
            catch (Exception e)
            {
                logger.LogInformation($"[COMMAND] [{requestId}] - [{commandName}] Exception");
                logger.LogInformation($"[COMMAND] [{requestId}] - [{commandName}] {e.ToString()}");
                throw;
            }
            finally
            {
                logger.LogInformation($"[COMMAND] [{requestId}] - [{commandName}] Finished API Request");
            }
        }

        public void SetApiRequestInfo(ApiRequestInfo apiRequestInfo)
        {
            service.ApiRequestInfo = apiRequestInfo;
        }
    }
}
