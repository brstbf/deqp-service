﻿using DEQP.PublicAPI.Interfaces;
using DEQP.PublicAPI.Resources;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Controllers
{
    public class ApiController<TApiService> : ControllerBase where TApiService : IApiService
    {
        IApiRequestHandler<TApiService> requestHandler;
        protected ApiController(IApiRequestHandler<TApiService> requestHandler)
        {
            this.requestHandler = requestHandler;
        }

        protected IApiRequestHandler<TApiService>  GetRequestHandler()
        {
            requestHandler.SetApiRequestInfo(GetRequestInfo());
            return requestHandler;
        }

        protected ApiRequestInfo GetRequestInfo()
        {
            return new ApiRequestInfo
            {
                IpAddress = HttpContext.Connection.RemoteIpAddress.ToString()
            };
        }

    }
}
