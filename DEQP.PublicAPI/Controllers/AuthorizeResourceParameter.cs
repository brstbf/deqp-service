﻿namespace DEQP.PublicAPI.Controllers
{
    public class AuthorizeResourceParameter :BaseResourceParameter
    {
        public string Name { get; set; }
        public string Lastname { get; set; }
        public bool OnlyNotAuthorize { get; set; }
        public int Department { get; set; }
    }
}