﻿using DEQP.App.Resources.News;
using DEQP.Core.Domain;
using DEQP.Infrastructure.DBContext.DEQPNews;
using DEQP.PublicAPI.Interfaces;
using DEQP.PublicAPI.Resources.News;
using DEQP.PublicAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NewsController : ApiController<INewsApiService>
    {
        public NewsController(IApiRequestHandler<INewsApiService> newsRequestHandler) : base(newsRequestHandler)
        {

        }

        [HttpGet]
        [Route("{id:int}")]
        //[ResponseCache(Duration = 1800, Location = ResponseCacheLocation.Any)]
        public NewsGetApiResponse Get(int id)
        
        {
            var news = GetRequestHandler().SendCommand("Get News", s => s.GetNews(id));
            return news;
        }

        [HttpGet]
        [Route("SearchNews")]
        public IEnumerable<SearchNewsApiResponse> SearchNews([FromQuery] SearchNewsApiRequest request)
        {
            var news = GetRequestHandler().SendCommand("Search News", s => s.SearchNews(request));
            return news;
        }

        [HttpGet]
        [Route("SearchLastedNews")]
        public IEnumerable<SearchNewsApiResponse> SearchLastedNews([FromQuery] SearchLastedNewsApiRequest request)
        {
            var news = GetRequestHandler().SendCommand("Search Lasted News", s => s.GetNews(request));
            return news;
        }


        [HttpGet]
        [Route("TopNews")]
        public IEnumerable<SearchNewsApiResponse> GetTopNews()
        {
            var news = GetRequestHandler().SendCommand("Get Top News", s => s.GetTopNews());
            return news;
        }

        [HttpGet]
        [Route("RelatedNews/{id}")]
        public IEnumerable<SearchNewsApiResponse> GetRelatedNews(int id)
        {
            var news = GetRequestHandler().SendCommand("Get Related News", s => s.GetRelatedNews(id));
            return news;
        }

        [HttpGet]
        [Route("NewsCategory/{type}")]
        public IEnumerable<NewsCategoryData> GetNewsCategory(NewsCategoryType type)
        {
            var news = GetRequestHandler().SendCommand("Get News Category", s => s.SearchNewsCategory(type));
            return news;
        }

        [HttpGet]
        [Route("NewsSummaryByCategory/{type}")]
        public IEnumerable<NewsSummaryByCategoryData> GetNewsSummaryByCategory(NewsCategoryType type)
        {
            var news = GetRequestHandler().SendCommand("Get News Category", s => s.GetNewsSummaryByCategory(type));
            return news;
        }

        [HttpGet]
        [Route("NatureDaily")]
        public async Task<ContentResult> GetNatureDailyAsync()
        {
            var news = await GetRequestHandler().SendCommand("Get Nature Daily News", s => s.NatureRssFeed());
            return Content(news, "application/rss+xml; charset=utf-8");
        }


    }

}
