﻿namespace DEQP.PublicAPI.Controllers
{
    public class AuthorizeListDto
    {
        public int UserId { get; set; }
        public string Name { get; set; }

        public string Status { get; set; }

        public string LastModifiedDate { get; set; }
        public string Division { get; set; }
        public int DivisionId { get; set; }
        public string Remark { get; set; }
    }
}