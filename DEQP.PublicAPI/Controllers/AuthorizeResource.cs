﻿using System.Collections.Generic;

namespace DEQP.PublicAPI.Controllers
{
    public class AuthorizeResource
    {
        public int UserId { get; set; }
        public List<string> ActiveFunctions { get; set; }
        public int DivisionId { get; set; }
        public bool IsActive { get; set; }

    }
}