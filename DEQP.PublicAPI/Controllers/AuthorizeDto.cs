﻿using System.Collections.Generic;

namespace DEQP.PublicAPI.Controllers
{
    public class AuthorizeDto
    {
        public int UserId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Username { get; set; }
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public List<string> ActiveFunctions { get; set; }
        public bool IsActive { get; set; }
        public string LastModifiedDate { get; set; }
        public bool IsCreate { get; set; }

    }
}