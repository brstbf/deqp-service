﻿using DEQP.App.Resources.Projects;
using DEQP.PublicAPI.Interfaces;
using DEQP.PublicAPI.Resources.Projects;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProjectController:ApiController<IProjectApiService>
    {
        public ProjectController(IApiRequestHandler<IProjectApiService> requestHandler):base(requestHandler)
        {

        }

        [HttpGet]
        [Route("")]
        public IEnumerable<ProjectSearchApiResponse> SearchProject([FromQuery] ProjectSearchApiRequest request)
        {
            return GetRequestHandler().SendCommand("Search Project", s => s.SearchProject(request));
        }

        [HttpGet]
        [Route("{id}")]
        public ProjectGetApiResponse GetProject(int id)
        {
            return GetRequestHandler().SendCommand("Get Project", s => s.GetProject(id));
        }

        [HttpGet]
        [Route("Dropdown")]
        public IEnumerable<ProjectDropdownData> GetProjectDropdown()
        {
            return GetRequestHandler().SendCommand("Get Project Dropdown", s => s.GetProjectDropdowns());
        }
    }
}
