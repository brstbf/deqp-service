﻿using DEQP.Core.Shared;
using DEQP.Infrastructure.Models;
using DEQP.PublicAPI.Interfaces;
using DEQP.PublicAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AdminAuthorizeController : ApiController<INewsApiService>
    {
        private readonly DEQP.Infrastructure.Models.DEQPNEWSContext _context;
        private readonly FileManager _manager;
        StorageSettings storageSettings;

        public AdminAuthorizeController(IApiRequestHandler<INewsApiService> newsRequestHandler,
            DEQP.Infrastructure.Models.DEQPNEWSContext context
            , FileManager manager
             , IOptions<StorageSettings> storageSettings) : base(newsRequestHandler)
        {
            _context = context;
            _manager = manager;
            this.storageSettings = storageSettings.Value;
        }

        [HttpGet]
        [Route("search")]
        public async Task<IActionResult> Search([FromQuery] AuthorizeResourceParameter resourceParameter)
        {
            var result = from user in _context.VwDeqpProfiles
                         join auth in _context.TblAdminProfiles.Include(x => x.Divis) on user.Id equals auth.AdminRefId.Value into Details
                         from m in Details.DefaultIfEmpty()
                         select new AuthorizeListDto()
                         {
                             UserId = (int)user.Id,
                             Name = user.Firstname + " " + user.Lastname,
                             Status = m.AdminRefId == null ? "pending" : (m.Isactive == "1" ? "active" : "inactive"),
                             LastModifiedDate = m.ModifiedDate.ToString(),
                             Division = m.Divis.DiviName,
                             DivisionId = m.DivisId ?? 0,
                             Remark = "",
                         };

            if (resourceParameter.OnlyNotAuthorize)
            {
                result = result.Where(x => x.Status.Equals("pending"));
            }

            if (!string.IsNullOrWhiteSpace(resourceParameter.Name))
            {
                result = result.Where(x => x.Name.Contains(resourceParameter.Name));
            }

            if (!string.IsNullOrWhiteSpace(resourceParameter.Lastname))
            {
                result = result.Where(x => x.Name.Contains(resourceParameter.Lastname));
            }

            if (resourceParameter.Department != 0)
            {
                result = result.Where(x => x.DivisionId == resourceParameter.Department);
            }


            var pagedList = await PagedList<AuthorizeListDto>.Create(result, resourceParameter.Page, resourceParameter.PageSize);

            return Ok(pagedList);
        }

        [HttpGet]
        [Route("pending_count")]
        public async Task<IActionResult> Pending()
        {
            var result = from user in _context.VwDeqpProfiles
                         join auth in _context.TblAdminProfiles.Include(x => x.Divis) on user.Id equals auth.AdminRefId.Value into Details
                         from m in Details.DefaultIfEmpty()
                         select new AuthorizeListDto()
                         {
                             UserId = (int)user.Id,
                             Name = user.Firstname + " " + user.Lastname,
                             Status = m.AdminRefId == null ? "pending" : (m.Isactive == "1" ? "active" : "inactive"),
                             LastModifiedDate = m.ModifiedDate.ToString(),
                             Division = m.Divis.DiviName,
                             Remark = "",
                         };


            return Ok(result.Where(x => x.Status.Equals("pending")).Count());
        }

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> Create([FromBody] AuthorizeResource resource)
        {
            try
            {
                var func = resource.ActiveFunctions.Select(x => new TblAdminRole()
                {
                    FuncCode = x.ToString()
                }).ToList();

                var profile = new TblAdminProfile()
                {
                    AdminRefId = resource.UserId,
                    DivisId = resource.DivisionId,
                    Isactive = resource.IsActive ? "1" : "0",
                    TblAdminRoles = func
                };

                _context.TblAdminProfiles.Add(profile);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

        }

        [HttpGet]
        [Route("search/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var selected = await _context.TblAdminProfiles
                .Include(x => x.Divis)
                .Include(x => x.TblAdminRoles).FirstOrDefaultAsync(x => x.AdminRefId == id);

            var user = await _context.VwDeqpProfiles.FirstOrDefaultAsync(x => id == x.Id);

            if (selected == null && user != null)
            {
                return Ok(new AuthorizeDto()
                {
                    UserId = (int)user.Id,
                    Firstname = user.Firstname,
                    Lastname = user.Lastname,
                    Username = user.Username,
                    IsCreate = true
                    //DivisionId = selected.DivisId ?? 0,
                    //DivisionName = selected.Divis == null ? "" : selected.Divis.DiviName,
                    //ActiveFunctions = selected.TblAdminRoles.Select(x => x.FuncCode).ToList(),
                    //IsActive = selected.Isactive == "1",
                    //LastModifiedDate = selected.ModifiedDate?.ToString("dd/MM/yyyy")
                }); ;
            }


            if (user == null)
            {
                return NotFound(id);
            }


            return Ok(new AuthorizeDto()
            {
                UserId = (int)user.Id,
                Firstname = user.Firstname,
                Lastname = user.Lastname,
                Username = user.Username,
                DivisionId = selected.DivisId ?? 0,
                DivisionName = selected.Divis == null ? "" : selected.Divis.DiviName,
                ActiveFunctions = selected.TblAdminRoles.Select(x => x.FuncCode).ToList(),
                IsActive = selected.Isactive == "1",
                LastModifiedDate = selected.ModifiedDate?.ToString("dd/MM/yyyy")
            }); ;
        }

        [HttpPost]
        [Route("edit/{id}")]
        public async Task<IActionResult> Edit(int id, [FromBody] AuthorizeResource resource)
        {
            try
            {
                var selected = await _context.TblAdminProfiles.Include(x => x.TblAdminRoles)
                    .FirstOrDefaultAsync(x => x.AdminRefId == id);

                if (selected == null)
                {
                    return NotFound();
                }

                _context.TblAdminRoles.RemoveRange(selected.TblAdminRoles);


                var func = resource.ActiveFunctions.Select(x => new TblAdminRole()
                {
                    FuncCode = x.ToString()
                }).ToList();

                selected.DivisId = resource.DivisionId;
                selected.TblAdminRoles = func;
                selected.Isactive = resource.IsActive ? "1" : "0";

                _context.TblAdminProfiles.Update(selected);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var list = await _context.TblAdminRoles.Where(x => x.AdminId == id).ToListAsync();

                var selected = await _context.TblAdminProfiles.Where(x => x.AdminId == id).ToListAsync();

                _context.TblAdminRoles.RemoveRange(list);

                _context.TblAdminProfiles.RemoveRange(selected);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

    }
}
