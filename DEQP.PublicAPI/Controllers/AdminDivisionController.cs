﻿using DEQP.Core.Shared;
using DEQP.Infrastructure.Models;
using DEQP.PublicAPI.Interfaces;
using DEQP.PublicAPI.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AdminDivisionController : ApiController<INewsApiService>
    {
        private readonly DEQP.Infrastructure.Models.DEQPNEWSContext _context;
        private readonly FileManager _manager;
        StorageSettings storageSettings;

        public AdminDivisionController(IApiRequestHandler<INewsApiService> newsRequestHandler,
            DEQP.Infrastructure.Models.DEQPNEWSContext context
            , FileManager manager
            , IOptions<StorageSettings> storageSettings) : base(newsRequestHandler)
        {
            _context = context;
            _manager = manager;
            this.storageSettings = storageSettings.Value;

        }
        #region division

        [HttpGet()]
        public async Task<IActionResult> Get([FromQuery] DivisionResourceParameter resourceParameter)
        {
            var collection = _context.TblDivisions
                .Include(x => x.TblNews)
                .AsQueryable();
            if (!string.IsNullOrWhiteSpace(resourceParameter.OrderBy))
            {
                switch (resourceParameter.OrderBy)
                {
                    case "name":
                        collection = collection.OrderBy(x => x.DiviName);
                        break;
                    case "name_desc":
                        collection = collection.OrderByDescending(x => x.DiviName);
                        break;
                        //case "type":
                        //    collection = collection.OrderBy(x => x.NewsGrpId1Navigation.NewsGrpName);
                        //    break;
                        //case "type_desc":
                        //    collection = collection.OrderByDescending(x => x.NewsGrpId1Navigation.NewsGrpName);
                        //    break;
                        //case "date":
                        //    collection = collection.OrderBy(x => x.CreatedDatetime);
                        //    break;
                        //case "date_desc":
                        //    collection = collection.OrderByDescending(x => x.NewsDate);
                        //    break;

                }
            }
            else
            {
                //collection = collection.OrderByDescending(x => x.NewsDate);
            }

            var query = collection.Select(x => new DivisionDto()
            {
                DivisionName = x.DiviName,
                DivisionId = x.DiviId,
                DivisionShortName = x.DiviNameAbbr,
                FileCount = 0,
                NewsCount = x.TblNews.Where(x => x.NewsGrpId1 == 104).Count(),
                IsActive = x.Isactive == "1",
                UpdateBy = $"{x.ModifiedBy} {x.ModifiedDatetime}",
            }
                );

           

            var pagedList = await PagedList<DivisionDto>.Create(query, resourceParameter.Page, resourceParameter.PageSize);

            return Ok(pagedList);
        }



        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetDivisonById(int id)
        {
            try
            {
                var selected = await _context.TblDivisions
                .FirstOrDefaultAsync(x => x.DiviId == id);

                var division = new DivisionByIdDto()
                {
                    DivisionId = selected.DiviId,
                    Contract = selected.DiviContact,
                    DivisionName = selected.DiviName,
                    DivisionShortName = selected.DiviNameAbbr,
                    IsPublish = selected.Isactive == "1",
                    Preface = selected.DiviPreface,
                    Description = selected.DiviDesc
                };

                return Ok(division);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

        }

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> Create([FromBody] DivisionResource resource)
        {
            try
            {
                var division = new TblDivision()
                {
                    DiviContact = resource.Contract,
                    DiviName = resource.DivisionName,
                    DiviNameAbbr = resource.DivisionShortName,
                    Isactive = resource.IsPublish ? "1" : "0",
                    DiviPreface = resource.Preface,
                    DiviDesc = resource.Description
                };

                _context.TblDivisions.Add(division);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

        }


        [HttpPost]
        [Route("edit/{id}")]
        public async Task<IActionResult> Edit(int id, [FromBody] DivisionResource resource)
        {

            var selected = await _context.TblDivisions
               .FirstOrDefaultAsync(x => x.DiviId == id);


            try
            {
                selected.DiviContact = resource.Contract;
                selected.DiviName = resource.DivisionName;
                selected.DiviNameAbbr = resource.DivisionShortName;
                selected.Isactive = resource.IsPublish ? "1" : "0";
                selected.DiviPreface = resource.Preface;
                selected.DiviDesc = resource.Description;

                _context.TblDivisions.Update(selected);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> Delete(int id)
        {

            var selected = await _context.TblDivisions
               .FirstOrDefaultAsync(x => x.DiviId == id);

            try
            {
                _context.TblDivisions.Remove(selected);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        #endregion

        #region officer
        [HttpGet("officer")]
        public async Task<IActionResult> GetOfficer([FromQuery] DivisionOfficerResourceParameter resourceParameter)
        {
            var flag = resourceParameter.IsDirector ? "10" : "99";
            var collection = _context.TblDivisionOfficers
                .Include(x => x.DiviSub)
                .Where(x => x.OfficerType == flag 
                && (string.IsNullOrWhiteSpace(resourceParameter.SubDivisionId)
                ||
                x.DiviSubId == Convert.ToInt32(resourceParameter.SubDivisionId))
                ).AsQueryable();

            if (!string.IsNullOrWhiteSpace(resourceParameter.DivisionId))
            {
                var div = Convert.ToInt32(resourceParameter.DivisionId);
                collection = collection.Where(x => x.DiviSub.DiviId == div).AsQueryable();
            }

            if (!string.IsNullOrWhiteSpace(resourceParameter.OrderBy))
            {
                switch (resourceParameter.OrderBy)
                {
                    case "name":
                        collection = collection.OrderBy(x => x.OfficerFullName);
                        break;
                    case "name_desc":
                        collection = collection.OrderByDescending(x => x.OfficerFullName);
                        break;
                    //case "type":
                    //    collection = collection.OrderBy(x => x.NewsGrpId1Navigation.NewsGrpName);
                    //    break;
                    //case "type_desc":
                    //    collection = collection.OrderByDescending(x => x.NewsGrpId1Navigation.NewsGrpName);
                    //    break;
                    //case "date":
                    //    collection = collection.OrderBy(x => x.CreatedDatetime);
                    //    break;
                    //case "date_desc":
                    //    collection = collection.OrderByDescending(x => x.NewsDate);
                    //    break;

                }
            }
            else
            {
                //collection = collection.OrderByDescending(x => x.NewsDate);
            }

            var query = collection.Select(x => new DivisionOfficerDto()
            {
                OfficerId = x.OfficerId,
                Name = x.OfficerFullName,
                Division = x.DiviSub.DiviSubName,
                Position = x.OfficerPosition,
                IsActive = x.Isactive == "1",
                UpdateBy = $"{x.ModifiedBy} {x.ModifiedDate}"
            }
                );

            var pagedList = await PagedList<DivisionOfficerDto>.Create(query, resourceParameter.Page, resourceParameter.PageSize);

            return Ok(pagedList);
        }

        [HttpGet("officer/{id}")]
        public async Task<IActionResult> GetOfficerById(int id)
        {

            var selected = await _context.TblDivisionOfficers.FirstOrDefaultAsync(x => x.OfficerId == id);

            var dto = new OfficerByIdDto()
            {
                Id = selected.OfficerId,
                Name = selected.OfficerFullName,
                SubDivisionId = selected.DiviSubId.Value,
                IsPublish = selected.Isactive == "1",
                PositionType = selected.OfficerType,
                IsDirector = selected.DirectorFlag == "1",
                Tel = selected.OfficerNumber,
                Position = selected.OfficerPosition,
                OrganizeTel = selected.OfficerIntNumber,
                Fax = selected.OfficerFax,
                Email = selected.OfficerEmail,
                //Image = "",
                Image = string.IsNullOrWhiteSpace(selected.ImageRename) ? "" : $"{storageSettings.AdminDivisionOfficerImageUrl}/{selected.ImageRename}",
            };

            return Ok(dto);
        }

        [HttpPost]
        [Route("officer")]
        public async Task<IActionResult> CreateOfficer([FromForm] OfficerResource resource)
        {
            try
            {
                var news = new TblDivisionOfficer();
                news.OfficerFullName = resource.Name;
                news.OfficerPosition = resource.Position;
                news.OfficerType = resource.PositionType;
                news.DirectorFlag = resource.IsDirector ? "1" : "0";
                news.ImageRename = resource.Image == null ? null
                             : await _manager.SaveAttachFile(resource.Image, "Image", "OfficerCover");
                news.OfficerNumber = resource.Tel;
                news.OfficerIntNumber = resource.OrganizeTel;
                news.OfficerFax = resource.Fax;
                news.OfficerEmail = resource.Email;
                news.Isactive = resource.IsPublish ? "1" : "0";

                if (!string.IsNullOrWhiteSpace(resource.SubDivisionId))
                {
                    news.DiviSubId = Convert.ToInt32(resource.SubDivisionId);
                }

                _context.TblDivisionOfficers.Add(news);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

        }

        [HttpPost]
        [Route("officer/{id}")]
        public async Task<IActionResult> EditOfficer(int id, [FromForm] OfficerResource resource)
        {

            try
            {
                var news = await _context.TblDivisionOfficers.FirstOrDefaultAsync(x => x.OfficerId == id);

                news.OfficerFullName = resource.Name;
                news.OfficerPosition = resource.Position;
                news.OfficerType = resource.PositionType;
                news.DirectorFlag = resource.IsDirector ? "1" : "0";
                news.ImageRename = resource.Image == null ? null
                             : await _manager.SaveAttachFile(resource.Image, "Image", "OfficerCover");
                news.OfficerNumber = resource.Tel;
                news.OfficerIntNumber = resource.OrganizeTel;
                news.OfficerFax = resource.Fax;
                news.OfficerEmail = resource.Email;
                news.Isactive = resource.IsPublish ? "1" : "0";

                if (!string.IsNullOrWhiteSpace(resource.SubDivisionId))
                {
                    news.DiviSubId = Convert.ToInt32(resource.SubDivisionId);
                }
                _context.TblDivisionOfficers.Update(news);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

        }

        [HttpDelete]
        [Route("officer/{id}")]
        public async Task<IActionResult> DeleteOfficer(int id)
        {

            try
            {
                var news = await _context.TblDivisionOfficers.FirstOrDefaultAsync(x => x.OfficerId == id);

                _context.TblDivisionOfficers.Remove(news);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

        }

        [HttpDelete]
        [Route("officer/{id}/image")]
        public async Task<IActionResult> DeleteOfficerImage(int id)
        {

            try
            {
                var selected = await _context.TblDivisionOfficers.FirstOrDefaultAsync(x => x.OfficerId == id);

                selected.ImageRename = null;

                _context.TblDivisionOfficers.Update(selected);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

        }

        #endregion

        #region subdivision
        [HttpGet("sub_division")]
        public async Task<IActionResult> GetSubDivision([FromQuery] DivisionOfficerResourceParameter resourceParameter)
        {
            var div = Convert.ToInt32(resourceParameter.DivisionId);
            var collection = _context.TblDivisionSubs
                .Where(x => x.DiviId == div)
                .AsQueryable();


            var query = collection.Select(x => new SubDivisionDto()
            {
                Id = x.DiviSubId,
                Name = x.DiviSubName,
                IsActive = x.Isactive == "1",
            });

            var pagedList = await PagedList<SubDivisionDto>.Create(query, resourceParameter.Page, resourceParameter.PageSize);

            return Ok(pagedList);
        }

        [HttpGet("sub_division/{id}")]
        public async Task<IActionResult> GetSubDivisionById(int id)
        {
            var collection = await _context.TblDivisionSubs
                .FirstOrDefaultAsync(x => x.DiviId == id);

            return Ok(new
            {
                DivisionId = collection.DiviId,
                SubDivisionId = collection.DiviSubId,
                SubDivisionName = collection.DiviSubName,
                IsActive = collection.Isactive == "1"
            });
        }


        [HttpPost("sub_division")]
        public async Task<IActionResult> CreateSubDivision([FromBody] SubDivisionResource resource)
        {

            try
            {
                var sub = new TblDivisionSub();

                sub.DiviSubName = resource.Name;
                sub.Isactive = resource.IsActive ? "1" : "0";
                sub.DiviId = resource.DivisionId;

                _context.TblDivisionSubs.Add(sub);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        [HttpPost("sub_division/{id}")]
        public async Task<IActionResult> EditSubDivision(int id, [FromBody] SubDivisionResource resource)
        {

            try
            {
                var sub = await _context.TblDivisionSubs.FirstOrDefaultAsync(x => x.DiviSubId == id);

                sub.DiviSubName = resource.Name;
                sub.Isactive = resource.IsActive ? "1" : "0";
                sub.DiviId = resource.DivisionId;

                _context.TblDivisionSubs.Update(sub);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        [HttpDelete("sub_division/{id}")]
        public async Task<IActionResult> EditSubDivision(int id)
        {

            try
            {
                var sub = await _context.TblDivisionSubs.FirstOrDefaultAsync(x => x.DiviSubId == id);

                _context.TblDivisionSubs.Remove(sub);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        #endregion

        #region file

        [HttpGet("file_group/{divisionId}")]
        public async Task<IActionResult> GetFile(int divisionId, [FromQuery] DivisionFileResourceParameter resourceParameter)
        {
            var collection = _context.TblDocumentGroups
                .Include(x => x.TblDivisionFiles)
                .Where(x => x.DiviId == divisionId)
                .AsQueryable();


            var query = collection.Select(x => new FileGroupDto()
            {
                DivisionName = x.Divi.DiviName,
                DivisionId = x.DiviId.Value,
                FileGroupId = x.DocGrpId,
                FileGroupName = x.DocGrpName,
                FileGroupCount = x.TblDivisionFiles.Count,
                UpdateBy = $"{x.ModifiedBy} {x.ModifiedDate}",
            }
                );

            var pagedList = await PagedList<FileGroupDto>.Create(query, resourceParameter.Page, resourceParameter.PageSize);

            return Ok(pagedList);
        }

        [HttpGet("file_group/{divisionId}/{groupId}")]
        public async Task<IActionResult> GetFileGroupById(int divisionId, int groupId)
        {
            var collection = await _context.TblDocumentGroups
                .Include(x => x.TblDivisionFiles)
                .Include(x => x.Divi)
                .FirstOrDefaultAsync(x => x.DiviId == divisionId && x.DocGrpId == groupId);


            var dto = new FileGroupByIdDto();
            dto.DivisionName = collection.Divi.DiviName;
            dto.DivisionId = collection.DiviId.Value;
            dto.FileGroupId = collection.DocGrpId;
            dto.FileGroupName = collection.DocGrpName;
            dto.FileGroupCount = collection.TblDivisionFiles.Count;
            dto.UpdateBy = $"{collection.ModifiedBy} {collection.ModifiedDate}";
            dto.AttachFiles = collection.TblDivisionFiles.Select(f => new FileDto()
            {
                Id = f.DiviFileId,
                Url = $"{storageSettings.AdminDivisionFileUrl}/{f.FileRename}",
                Name = f.FileOriName,
            }).ToList();


            return Ok(dto);
        }

        [HttpPost("file_group/{divisionId}")]
        public async Task<IActionResult> CreateFileGroup(int divisionId, [FromForm] FileGroupResource resource)
        {
            try
            {

                List<TblDivisionFile> files = new List<TblDivisionFile>();
                if (resource.AttachFiles != null)
                {
                    var i = 1;
                    foreach (var attach in resource.AttachFiles)
                    {
                        var file = await _manager.SaveAttachFile(attach, "File", "DivisionFile");
                        files.Add(new TblDivisionFile()
                        {
                            FileOriName = attach.FileName,
                            FileRename = file,
                            FileSize = attach.Length,
                            OrderNo = i++
                        });
                    }
                }


                var group = new TblDocumentGroup();
                group.DiviId = divisionId;
                group.DocGrpName = resource.FileGroupName;
                group.TblDivisionFiles = files;

                _context.TblDocumentGroups.Add(group);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

        }

        [HttpPost("file_group/{divisionId}/{groupId}")]
        public async Task<IActionResult> CreateFileGroup(int divisionId, int groupId, [FromForm] FileGroupResource resource)
        {
            try
            {

                var selected = await _context.TblDocumentGroups
                    .Include(x => x.TblDivisionFiles)

                    .FirstOrDefaultAsync(x => x.DocGrpId == groupId);

                if (selected == null)
                {
                    return NotFound(groupId);
                }

                List<TblNewsFile> files = new List<TblNewsFile>();
                if (resource.AttachFiles != null)
                {
                    var i = 1;
                    foreach (var attach in resource.AttachFiles)
                    {
                        var file = await _manager.SaveAttachFile(attach, "File", "DivisionFile");

                        selected.TblDivisionFiles.Add(new TblDivisionFile()
                        {
                            FileOriName = attach.FileName,
                            FileRename = file,
                            FileSize = attach.Length,
                            OrderNo = i++
                        });
                    }
                }


                var group = new TblDocumentGroup();
                selected.DiviId = divisionId;
                selected.DocGrpName = resource.FileGroupName;
                //selected.TblDivisionFiles = files;

                _context.TblDocumentGroups.Update(group);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

        }

        [HttpDelete("file_group/{divisionId}/{groupId}")]
        public async Task<IActionResult> DeleteFileGroup(int divisionId, int groupId)
        {
            try
            {

                var selected = await _context.TblDocumentGroups
                    .Include(x => x.TblDivisionFiles)

                    .FirstOrDefaultAsync(x => x.DocGrpId == groupId);

                var list = await _context.TblDivisionFiles.Where(x => x.DocGrpId == groupId).ToListAsync();

                _context.TblDivisionFiles.RemoveRange(list);

                if (selected == null)
                {
                    return NotFound(groupId);
                }

                _context.TblDocumentGroups.Remove(selected);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpDelete("division_file/{fileId}")]
        public async Task<IActionResult> DeleteFile(int fileId)
        {
            try
            {

                var selected = await _context.TblDivisionFiles
                    .FirstOrDefaultAsync(x => x.DiviFileId == fileId);


                _context.TblDivisionFiles.Remove(selected);

                if (selected == null)
                {
                    return NotFound(fileId);
                }

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        #endregion



    }

    public class FileGroupResource
    {
        public string FileGroupName { get; set; }
        public List<IFormFile> AttachFiles { get; set; }
    }

    public class FileGroupByIdDto
    {
        public int DivisionId { get; set; }
        public int FileGroupId { get; set; }
        public string DivisionName { get; set; }
        public string FileGroupName { get; set; }
        public int FileGroupCount { get; set; }
        public string UpdateBy { get; set; }

        public List<FileDto> AttachFiles { get; set; }
    }

    public class FileGroupDto
    {
        public int DivisionId { get; set; }
        public int FileGroupId { get; set; }
        public string DivisionName { get; set; }
        public string FileGroupName { get; set; }
        public int FileGroupCount { get; set; }
        public string UpdateBy { get; set; }
    }

    public class OfficerByIdDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int DivisionId { get; set; }
        public int SubDivisionId { get; set; }
        public bool IsPublish { get; set; }
        public string PositionType { get; set; }
        public bool IsDirector { get; set; }
        public string Tel { get; set; }
        public string Position { get; set; }
        public string OrganizeTel { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Image { get; set; }

    }

    public class OfficerResource
    {
        public int DivisionId { get; set; }
        public string SubDivisionId { get; set; }
        public bool IsPublish { get; set; }
        public string PositionType { get; set; }
        public bool IsDirector { get; set; }
        public string Name { get; set; }
        public string Tel { get; set; }
        public string Position { get; set; }
        public string OrganizeTel { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public IFormFile Image { get; set; }
    }

    public class SubDivisionResource
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int DivisionId { get;  set; }
    }

    public class DivisionResource
    {
        public string DivisionName { get; set; }
        public string DivisionShortName { get; set; }
        public bool IsPublish { get; set; }
        public string Description { get; set; }
        public string Preface { get; set; }
        public string Contract { get; set; }
    }

    public class SubDivisionDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }

    public class DivisionDto
    {
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public string DivisionShortName { get; set; }
        public int NewsCount { get; set; }
        public int FileCount { get; set; }
        public string UpdateBy { get; set; }
        public bool IsActive { get; internal set; }
    }

    public class DivisionByIdDto
    {
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public string DivisionShortName { get; set; }
        public bool IsPublish { get; set; }
        public string Description { get; set; }
        public string Preface { get; set; }
        public string Contract { get; set; }
    }

    public class DivisionOfficerDto
    {
        public int OfficerId { get; set; }
        public string Name { get; set; }
        public string Division { get; set; }
        public string Position { get; set; }
        public bool IsActive { get; set; }
        public string UpdateBy { get; set; }
    }

    public class DivisionOfficerResourceParameter : BaseResourceParameter
    {
        public string DivisionId { get; set; }
        public string SubDivisionId { get; set; }
        public bool IsDirector { get; set; }

    }

    public class DivisionResourceParameter : BaseResourceParameter
    {

    }
    public class DivisionFileResourceParameter : BaseResourceParameter
    {
        public string DivisionId { get; set; }

    }




}
