﻿using DEQP.Core.Shared;
using DEQP.Infrastructure.Models;
using DEQP.PublicAPI.Interfaces;
using DEQP.PublicAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AdminLogController : ApiController<INewsApiService>
    {
        private readonly DEQP.Infrastructure.Models.DEQPNEWSContext _context;
        private readonly FileManager _manager;
        StorageSettings storageSettings;

        public AdminLogController(IApiRequestHandler<INewsApiService> newsRequestHandler,
            DEQP.Infrastructure.Models.DEQPNEWSContext context
            , FileManager manager
             , IOptions<StorageSettings> storageSettings) : base(newsRequestHandler
               )
        {
            _context = context;
            _manager = manager;
            this.storageSettings = storageSettings.Value;

        }


        [HttpGet]
        [Route("search")]
        public async Task<IActionResult> Search([FromQuery] LogResourceParameter resourceParameter)
        {
            var collection = _context.TblAdminLogs
                .Include(x => x.FuncCodeNavigation)
               .AsQueryable();

            if (!string.IsNullOrWhiteSpace(resourceParameter.Name))
            {

            }

            if (!string.IsNullOrWhiteSpace(resourceParameter.Start))
            {
                var startDate = DateTime.ParseExact(resourceParameter.Start, "ddMMyyyy", CultureInfo.InvariantCulture);
                collection = collection.Where(x => x.LogDatetime >= startDate);
            }

            if (!string.IsNullOrWhiteSpace(resourceParameter.Start))
            {
                var endDate = DateTime.ParseExact(resourceParameter.Start, "ddMMyyyy", CultureInfo.InvariantCulture);
                collection = collection.Where(x => x.LogDatetime <= endDate);
            }



            var query = collection.Select(x => new LogDto()
            {
                Name = x.LogAdminFullname,
                Function = x.FuncCodeNavigation.FuncName,
                Description = x.LogDesc,
                LogDate = x.LogDatetime.Value.ToString("dd/MM/yyyy HH:mm")
            });

            var pagedList = await PagedList<LogDto>.Create(query, resourceParameter.Page, resourceParameter.PageSize);

            return Ok(pagedList);
        }

    }

    public class LogDto
    {
        public string Name { get; set; }
        public string Function { get; set; }
        public string Description { get; set; }
        public string LogDate { get; set; }
    }

    public class LogResourceParameter : BaseResourceParameter
    {
        public string Name { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
    }


}
