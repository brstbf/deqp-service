﻿using DEQP.App.Resources.Divisions;
using DEQP.PublicAPI.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DivisionController : ApiController<IDivisionApiService>
    {
        public DivisionController(IApiRequestHandler<IDivisionApiService> requestHandler) : base(requestHandler)
        {
        }

        [HttpGet("dropdown")]
        public IEnumerable<DivisionDropdownData> GetDropdowns()
        {
            return GetRequestHandler().SendCommand("Get Division Dropdown", s => s.GetDropdowns());
        }

        [HttpGet]
        public IEnumerable<DivisionData> Get()
        {
            return GetRequestHandler().SendCommand("Get All Division", s => s.Get());
        }

        [HttpGet("{id:int}")]
        public DivisionData Get(int id)
        {
            return GetRequestHandler().SendCommand("Get Division", s => s.Get(id));
        }
    }
}
