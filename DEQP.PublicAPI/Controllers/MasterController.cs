﻿using DEQP.Infrastructure.Models;
using DEQP.PublicAPI.Interfaces;
using DEQP.PublicAPI.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MasterController : ApiController<INewsApiService>
    {
        private readonly DEQP.Infrastructure.Models.DEQPNEWSContext _context;
        private readonly FileManager _manager;
        public MasterController(IApiRequestHandler<INewsApiService> newsRequestHandler,
            DEQP.Infrastructure.Models.DEQPNEWSContext context
            , FileManager manager) : base(newsRequestHandler)
        {
            _context = context;
            _manager = manager;
        }

        [HttpGet]
        [Route("news_lastest_year")]
        public async Task<IActionResult> NewsLastYear()
        {
            return Ok(_context.TblNews.Where(x => x.NewsGrpId1Navigation.NewsTypeCode.Equals("10")).Max(x => x.FiscalYear));
        }

        [HttpGet]
        [Route("recuit_lastest_year")]
        public async Task<IActionResult> RecuitLastYear()
        {
            return Ok(_context.TblNews.Where(x => x.NewsGrpId1Navigation.NewsTypeCode.Equals("20")).Max(x => x.FiscalYear));
        }

        //[HttpGet]
        //[Route("project_lastest_year")]
        //public async Task<IActionResult> ProjectLastYear()
        //{
        //    return Ok(_context.TblProjects.Max(x => x.));
        //}


        [HttpGet]
        [Route("news_group")]
        public async Task<IActionResult> NewsGroup([FromQuery] string newsType)
        {

            return Ok(await _context.TblNewsGroups.Where(x => x.NewsTypeCode.Equals(newsType)).Select(x => new
            {
                Text = x.NewsGrpName,
                Value = x.NewsGrpId
            }).ToListAsync());
        }


        [HttpGet]
        [Route("division")]
        public async Task<IActionResult> Division()
        {

            return Ok(await _context.TblDivisions.Select(x => new
            {
                Text = x.DiviName,
                Value = x.DiviId
            }).ToListAsync());
        }


        [HttpGet]
        [Route("sub_division/{id}")]
        public async Task<IActionResult> SubDivision(int id)
        {

            return Ok(await _context.TblDivisionSubs.Where(x => x.DiviId == id).Select(x => new
            {
                Text = x.DiviSubName,
                Value = x.DiviSubId
            }).ToListAsync());
        }


        [HttpGet]
        [Route("function")]
        public async Task<IActionResult> Function()
        {

            return Ok(await _context.TblConfigFunctions.Where(x => x.FuncFlag == "1").Select(x => new
            {
                Text = x.FuncName,
                Value = x.FuncCode
            }).ToListAsync());
        }

    }

}
