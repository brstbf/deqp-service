﻿using DEQP.Core.Shared;
using DEQP.Infrastructure.Models;
using DEQP.PublicAPI.Interfaces;
using DEQP.PublicAPI.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AdminNewsController : ApiController<INewsApiService>
    {
        private readonly DEQP.Infrastructure.Models.DEQPNEWSContext _context;
        private readonly FileManager _manager;
        StorageSettings storageSettings;

        public AdminNewsController(IApiRequestHandler<INewsApiService> newsRequestHandler,
            DEQP.Infrastructure.Models.DEQPNEWSContext context
            , FileManager manager
            , IOptions<StorageSettings> storageSettings) : base(newsRequestHandler)
        {
            _context = context;
            _manager = manager;
            this.storageSettings = storageSettings.Value;

        }


        [HttpGet]
        [Route("search")]
        public async Task<IActionResult> Search([FromQuery] NewsResourceParameter resourceParameter)
        {
            var collection = _context.TblNews
                .Where(x =>
                //x.Isactive == "1"
                //&& 
                x.NewsGrpId1Navigation.NewsTypeCode.Equals("10")).AsQueryable();

            if (resourceParameter.IsTopNews)
            {
                var flag = resourceParameter.IsTopNews ? "1" : "0";

                collection = collection.Where(x => x.TopNewsFlag == flag);
            }

            if (resourceParameter.IsLastestNews)
            {
                var minLastestDate = DateTime.Now.AddDays(-30);
                collection = collection.Where(x => x.NewsDate >= minLastestDate && x.NewsDate < DateTime.Now);
            }

            if (!string.IsNullOrWhiteSpace(resourceParameter.Keyword))
            {
                collection = collection.Where(x => x.NewsTitle.Contains(resourceParameter.Keyword));
            }

            if (!string.IsNullOrWhiteSpace(resourceParameter.Year))
            {
                collection = collection.Where(x => x.FiscalYear.Equals(resourceParameter.Year));
            }

            if (!string.IsNullOrWhiteSpace(resourceParameter.StartDate))
            {
                var startDate = DateTime.ParseExact(resourceParameter.StartDate, "ddMMyyyy", CultureInfo.InvariantCulture);
                collection = collection.Where(x => x.NewsDate >= startDate);
            }

            if (!string.IsNullOrWhiteSpace(resourceParameter.EndDate))
            {
                var endDate = DateTime.ParseExact(resourceParameter.EndDate, "ddMMyyyy", CultureInfo.InvariantCulture);
                collection = collection.Where(x => x.NewsDate <= endDate);
            }

            if (!string.IsNullOrWhiteSpace(resourceParameter.NewsType))
            {
                collection = collection.Where(x => x.NewsGrpId1 == Convert.ToInt32(resourceParameter.NewsType) ||
                (x.NewsGrpId2 != null && x.NewsGrpId2 == Convert.ToInt32(resourceParameter.NewsType)));
            }

            if (!string.IsNullOrWhiteSpace(resourceParameter.Organize))
            {
                collection = collection.Where(x => x.DiviId == Convert.ToInt32(resourceParameter.Organize));
            }

            if (!string.IsNullOrWhiteSpace(resourceParameter.OrderBy))
            {
                switch (resourceParameter.OrderBy)
                {
                    case "name":
                        collection = collection.OrderBy(x => x.NewsTitle);
                        break;
                    case "name_desc":
                        collection = collection.OrderByDescending(x => x.NewsTitle);
                        break;
                    case "type":
                        collection = collection.OrderBy(x => x.NewsGrpId1Navigation.NewsGrpName);
                        break;
                    case "type_desc":
                        collection = collection.OrderByDescending(x => x.NewsGrpId1Navigation.NewsGrpName);
                        break;
                    case "date":
                        collection = collection.OrderBy(x => x.NewsDate);
                        break;
                    case "date_desc":
                        collection = collection.OrderByDescending(x => x.NewsDate);
                        break;

                }
            }
            else
            {
                collection = collection.OrderByDescending(x => x.NewsDate);
            }

            try
            {
                var query = collection.Select(x => new NewsListDto()
                {
                    Id = x.NewsId,
                    Topic = x.NewsTitle,
                    Year = x.FiscalYear,
                    NewsList = new List<string>()
                    {
                        x.NewsGrpId1Navigation != null ?  x.NewsGrpId1Navigation.NewsGrpName : "",
                        x.NewsGrpId2Navigation != null ?  x.NewsGrpId2Navigation.NewsGrpName : "",
                    },
                    Organize = x.Divi.DiviName,
                    IsLastestNews = (DateTime.Now - (x.NewsDate ?? DateTime.Now)).TotalDays <= 30,
                    IsTopNews = x.TopNewsFlag == "1",
                    DateTime = x.NewsDate.Value.ToString("ddMMyyyy"),
                    IsPublish = x.Isactive == "1",
                    Update = $"{x.ModifiedDate.Value.ToString("ddMMyyyy HH:mm")} By {x.ModifiedBy}"
                });

                var pagedList = await PagedList<NewsListDto>.Create(query, resourceParameter.Page, resourceParameter.PageSize);

                return Ok(pagedList);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet]
        [Route("search_by_project/{projectId}")]
        public async Task<IActionResult> SearchByProject(int projectId,[FromQuery] NewsResourceParameter resourceParameter)
        {
            var collection = _context.TblNews
                .Where(x =>
               x.ProjId == projectId).AsQueryable();

            if (resourceParameter.IsTopNews)
            {
                var flag = resourceParameter.IsTopNews ? "1" : "0";

                collection = collection.Where(x => x.TopNewsFlag == flag);
            }

            if (resourceParameter.IsLastestNews)
            {
                var minLastestDate = DateTime.Now.AddDays(-30);
                collection = collection.Where(x => x.NewsDate >= minLastestDate && x.NewsDate < DateTime.Now);
            }

            if (!string.IsNullOrWhiteSpace(resourceParameter.Keyword))
            {
                collection = collection.Where(x => x.NewsTitle.Contains(resourceParameter.Keyword));
            }

            if (!string.IsNullOrWhiteSpace(resourceParameter.Year))
            {
                collection = collection.Where(x => x.FiscalYear.Equals(resourceParameter.Year));
            }

            if (!string.IsNullOrWhiteSpace(resourceParameter.StartDate))
            {
                var startDate = DateTime.ParseExact(resourceParameter.StartDate, "ddMMyyyy", CultureInfo.InvariantCulture);
                collection = collection.Where(x => x.NewsDate >= startDate);
            }

            if (!string.IsNullOrWhiteSpace(resourceParameter.EndDate))
            {
                var endDate = DateTime.ParseExact(resourceParameter.EndDate, "ddMMyyyy", CultureInfo.InvariantCulture);
                collection = collection.Where(x => x.NewsDate <= endDate);
            }

            if (!string.IsNullOrWhiteSpace(resourceParameter.NewsType))
            {
                collection = collection.Where(x => x.NewsGrpId1 == Convert.ToInt32(resourceParameter.NewsType) ||
                (x.NewsGrpId2 != null && x.NewsGrpId2 == Convert.ToInt32(resourceParameter.NewsType)));
            }

            if (!string.IsNullOrWhiteSpace(resourceParameter.Organize))
            {
                collection = collection.Where(x => x.DiviId == Convert.ToInt32(resourceParameter.Organize));
            }

            if (!string.IsNullOrWhiteSpace(resourceParameter.OrderBy))
            {
                switch (resourceParameter.OrderBy)
                {
                    case "name":
                        collection = collection.OrderBy(x => x.NewsTitle);
                        break;
                    case "name_desc":
                        collection = collection.OrderByDescending(x => x.NewsTitle);
                        break;
                    case "type":
                        collection = collection.OrderBy(x => x.NewsGrpId1Navigation.NewsGrpName);
                        break;
                    case "type_desc":
                        collection = collection.OrderByDescending(x => x.NewsGrpId1Navigation.NewsGrpName);
                        break;
                    case "date":
                        collection = collection.OrderBy(x => x.NewsDate);
                        break;
                    case "date_desc":
                        collection = collection.OrderByDescending(x => x.NewsDate);
                        break;

                }
            }
            else
            {
                collection = collection.OrderByDescending(x => x.NewsDate);
            }

            var query = collection.Select(x => new NewsListDto()
            {
                Id = x.NewsId,
                Topic = x.NewsTitle,
                Year = x.FiscalYear,
                NewsList = new List<string>()
                    {
                        x.NewsGrpId1Navigation != null ?  x.NewsGrpId1Navigation.NewsGrpName : "",
                        x.NewsGrpId2Navigation != null ?  x.NewsGrpId2Navigation.NewsGrpName : "",
                    },
                Organize = x.Divi.DiviName,
                IsLastestNews = (DateTime.Now - x.NewsDate.Value).TotalDays <= 30,
                IsTopNews = x.TopNewsFlag == "1",
                DateTime = x.NewsDate.Value.ToString("ddMMyyyy"),
                Update = $"{x.ModifiedDate.Value.ToString("ddMMyyyy HH:mm")} By {x.ModifiedBy}",
                IsPublish = x.Isactive == "1",
            });

            var pagedList = await PagedList<NewsListDto>.Create(query, 1, 1000);

            return Ok(pagedList);
        }


        [HttpGet]
        [Route("search_by_division/{divisionId}")]
        public async Task<IActionResult> SearchByDivision(int divisionId, [FromQuery] NewsResourceParameter resourceParameter)
        {
            var collection = _context.TblNews
                .Where(x =>
               x.DiviId == divisionId
               && (x.NewsGrpId1 == 104)).AsQueryable();

            if (!string.IsNullOrWhiteSpace(resourceParameter.OrderBy))
            {
                switch (resourceParameter.OrderBy)
                {
                    case "name":
                        collection = collection.OrderBy(x => x.NewsTitle);
                        break;
                    case "name_desc":
                        collection = collection.OrderByDescending(x => x.NewsTitle);
                        break;
                    case "type":
                        collection = collection.OrderBy(x => x.NewsGrpId1Navigation.NewsGrpName);
                        break;
                    case "type_desc":
                        collection = collection.OrderByDescending(x => x.NewsGrpId1Navigation.NewsGrpName);
                        break;
                    case "date":
                        collection = collection.OrderBy(x => x.NewsDate);
                        break;
                    case "date_desc":
                        collection = collection.OrderByDescending(x => x.NewsDate);
                        break;

                }
            }
            else
            {
                collection = collection.OrderByDescending(x => x.NewsDate);
            }


            var query = collection.Select(x => new NewsListDto()
            {
                Id = x.NewsId,
                Topic = x.NewsTitle,
                Year = x.FiscalYear,
                NewsList = new List<string>()
                    {
                        x.NewsGrpId1Navigation != null ?  x.NewsGrpId1Navigation.NewsGrpName : "",
                        x.NewsGrpId2Navigation != null ?  x.NewsGrpId2Navigation.NewsGrpName : "",
                    },
                Organize = x.Divi.DiviName,
                IsLastestNews = (DateTime.Now - x.NewsDate.Value).TotalDays <= 30,
                IsTopNews = x.TopNewsFlag == "1",
                DateTime = x.NewsDate.Value.ToString("ddMMyyyy"),
                Update = $"{x.ModifiedDate.Value.ToString("ddMMyyyy HH:mm")} By {x.ModifiedBy}",
                IsPublish = x.Isactive == "1",
            });

            var pagedList = await PagedList<NewsListDto>.Create(query, resourceParameter.Page, resourceParameter.PageSize);

            return Ok(pagedList);
        }



        [HttpPost]
        [Route("")]
        public async Task<IActionResult> Create([FromForm] NewsResource resource)
        {
            List<TblNewsFile> files = new List<TblNewsFile>();
            if (resource.AttachFiles != null)
            {
                var i = 1;
                foreach (var attach in resource.AttachFiles)
                {
                    var file = await _manager.SaveAttachFile(attach, "File", "NewsFile");
                    files.Add(new TblNewsFile()
                    {
                        FileOriName = attach.FileName,
                        FileRename = file,
                        FileSize = attach.Length,
                        OrderNo = i++
                    });
                }
            }

            try
            {
                var news = new TblNews()
                {
                    NewsTitle = resource.Topic,
                    FiscalYear = resource.Year,
                    NewsTags = resource.Tags != null ? string.Join(",", resource.Tags) : null,
                    NewsDate = DateTime.ParseExact(resource.NewsDate, "ddMMyyyy", CultureInfo.InvariantCulture),
                    NewsContent = resource.Description,
                    NewsGrpId1 = Convert.ToInt32(resource.NewsType),
                    NewsGrpId2 = resource.IsInformationNews ? 101 : null,
                    TopNewsFlag = resource.IsTopNews ? "1" : "0",
                    Isactive = resource.IsPublish ? "1" : "0",
                    DiviId = Convert.ToInt32(resource.Organize),
                    CoverFileRename = resource.ImageCover == null ? null
                    : await _manager.SaveAttachFile(resource.ImageCover, "Image", "NewsCover"),
                    TblNewsFiles = files
                };

                news.ProjId = resource.ProjectId;

                _context.TblNews.Add(news);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

        }

        [HttpGet]
        [Route("search/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var selected = await _context.TblNews
                .Include(x => x.Divi)
                .Include(x => x.TblNewsFiles)
                .Include(x => x.NewsGrpId1Navigation)
                .Include(x => x.NewsGrpId2Navigation)
                .FirstOrDefaultAsync(x => x.NewsId == id);

            if (selected == null)
            {
                return NotFound(id);
            }

            return Ok(new NewsDto()
            {
                Id = selected.NewsId,
                Topic = selected.NewsTitle,
                IsInformationNews = selected.NewsGrpId2 == 101,

                CoverImage = new FileDto()
                {
                    Id = selected.NewsId,
                    Url = string.IsNullOrWhiteSpace(selected.CoverFileRename) ? "" : $"{storageSettings.NewsCoverUrl}/{selected.CoverFileRename}",
                    Name = selected.CoverFileRename,
                },
                AttachFiles = selected.TblNewsFiles.Select(f => new FileDto()
                {
                    Id = f.NewsFileId,
                    Url =  $"{storageSettings.NewsFileUrl}/{f.FileRename}",
                    Name = f.FileRename,
                }).ToList(),
                Description = selected.NewsContent,
                NewsDate = selected.NewsDate.Value.ToString("ddMMyyyy"),
                NewsType = selected.NewsGrpId1Navigation != null ? selected.NewsGrpId1Navigation.NewsGrpId.ToString() : "",
                Organize = selected.Divi.DiviId.ToString(),
                Year = selected.FiscalYear,
                IsPublish = selected.Isactive == "1",
                IsTopNews = selected.TopNewsFlag == "1",
                Tags = !string.IsNullOrWhiteSpace(selected.NewsTags) ? selected.NewsTags.Split(",").ToList() : new List<string>()
            }); ;
        }

        [HttpPost]
        [Route("edit/{id}")]
        public async Task<IActionResult> Edit(int id, [FromForm] NewsResource resource)
        {

            var selected = await _context.TblNews
               .Include(x => x.Divi)
               .Include(x => x.TblNewsFiles)
               .Include(x => x.NewsGrpId1Navigation)
               .Include(x => x.NewsGrpId2Navigation)
               .FirstOrDefaultAsync(x => x.NewsId == id);

            if (selected == null)
            {
                return NotFound(id);
            }

            List<TblNewsFile> files = new List<TblNewsFile>();
            if (resource.AttachFiles != null)
            {
                var i = 1;
                foreach (var attach in resource.AttachFiles)
                {
                    var file = await _manager.SaveAttachFile(attach, "File", "NewsFile");

                    selected.TblNewsFiles.Add(new TblNewsFile()
                    {
                        FileOriName = attach.FileName,
                        FileRename = file,
                        FileSize = attach.Length,
                        OrderNo = i++
                    });
                }
            }

            try
            {
                //_context.TblNewsFiles.RemoveRange(selected.TblNewsFiles);

                selected.NewsTitle = resource.Topic;
                selected.FiscalYear = resource.Year;
                selected.NewsTags = resource.Tags == null ? null : string.Join(",", resource.Tags);
                selected.NewsDate = DateTime.ParseExact(resource.NewsDate, "ddMMyyyy", CultureInfo.InvariantCulture);
                selected.NewsContent = resource.Description;
                selected.NewsGrpId1 = Convert.ToInt32(resource.NewsType);
                selected.NewsGrpId2 = resource.IsInformationNews ? 101 : null;

                selected.TopNewsFlag = resource.IsTopNews ? "1" : "0";
                selected.Isactive = resource.IsPublish ? "1" : "0";
                selected.DiviId = Convert.ToInt32(resource.Organize);
                selected.CoverFileRename = resource.ImageCover == null ? selected.CoverFileRename
                    : await _manager.SaveAttachFile(resource.ImageCover, "Image", "NewsCover");
                selected.ProjId = resource.ProjectId;

                _context.TblNews.Update(selected);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpDelete]
        [Route("delete_cover/{newsId}")]
        public async Task<IActionResult> DeleteCover(int newsId)
        {

            var selected = await _context.TblNews
               //.Include(x => x.Divi)
               //.Include(x => x.TblNewsFiles)
               //.Include(x => x.NewsGrpId1Navigation)
               //.Include(x => x.NewsGrpId2Navigation)
               .FirstOrDefaultAsync(x => x.NewsId == newsId);

            if (selected == null)
            {
                return NotFound(newsId);
            }

            try
            {

                selected.CoverFileRename = null;

                _context.TblNews.Update(selected);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

        }


        [HttpDelete]
        [Route("delete_file/{attachId}")]
        public async Task<IActionResult> DeleteAttachFile(int attachId)
        {
            var selected = await _context.TblNewsFiles

              .FirstOrDefaultAsync(x => x.NewsFileId == attachId);

            if (selected == null)
            {
                return NotFound(attachId);
            }

            try
            {
                _context.TblNewsFiles.Remove(selected);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpDelete]
        [Route("search/{id}")]
        public async Task<IActionResult> Delete(int id)
        {

            var selected = await _context.TblNews
               .FirstOrDefaultAsync(x => x.NewsId == id);

            if (selected == null)
            {
                return NotFound(id);
            }

            try
            {
                var attachList = await _context.TblNewsFiles.Where(x => x.NewsId == selected.NewsId).ToListAsync();
                _context.TblNewsFiles.RemoveRange(attachList);

                var count = await _context.TblCounterNews.Where(x => x.NewsId == selected.NewsId).ToListAsync();
                _context.TblCounterNews.RemoveRange(count);

                _context.TblNews.Remove(selected);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

    }





    public class FileManager
    {
        private IWebHostEnvironment _hostingEnvironment;

        public FileManager(IWebHostEnvironment environment)
        {
            _hostingEnvironment = environment;
        }

        public async Task<string> SaveAttachFile(IFormFile file, string type, string folder)
        {
            string uploads = Path.Combine(_hostingEnvironment.WebRootPath, type, folder);
            //foreach (IFormFile file in files)
            //{
            //    if (file.Length > 0)
            //    {

            var newName = $"{Guid.NewGuid()}_{file.FileName}";
            string filePath = Path.Combine(uploads, newName);
            using (Stream fileStream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }
            //    }
            //}

            return newName;
        }


    }


    public class FileDto
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
    }

    public class NewsDto : BaseResource
    {
        public int Id { get; set; }
        public FileDto CoverImage { get; set; }
        public List<FileDto> AttachFiles { get; set; }
        public string Description { get; set; }
        public bool IsPublish { get; set; }
        public string Topic { get; internal set; }
        public bool IsInformationNews { get; internal set; }
    }

    public class BaseResource
    {
        public string NewsType { get; set; }
        public string Organize { get; set; }
        public string Year { get; set; }
        public string NewsDate { get; set; }
        public List<string> Tags { get; set; }
        public bool IsTopNews { get; set; }
        public bool IsPublish { get; set; }
    }

    public class NewsResource : BaseResource
    {
        public string Topic { get; set; }
        public IFormFile ImageCover { get; set; }
        public List<IFormFile> AttachFiles { get; set; }
        public string Description { get; set; }
        public int? ProjectId { get; set; }
        public int? DivisionId { get; set; }

        public bool IsInformationNews { get; set; }
    }

    public class NewsListDto
    {
        public int Id { get; set; }
        public string Topic { get; set; }
        public string Year { get; set; }
        public List<string> NewsList { get; set; }
        public string Organize { get; set; }
        public bool IsTopNews { get; set; }
        public bool IsLastestNews { get; set; }
        public string DateTime { get; set; }
        public string Update { get; set; }
        public bool IsPublish { get; set; }

    }

    public class BaseResourceParameter
    {
        public string Keyword { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }

        public bool? IsPublish { get; set; }
        public string OrderBy { get; set; }
    }

    public class NewsResourceParameter : BaseResourceParameter
    {
        public string Year { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Organize { get; set; }
        public string NewsType { get; set; }
        public bool IsTopNews { get; set; }
        public bool IsLastestNews { get; set; }

    }
}
