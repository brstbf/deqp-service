﻿using DEQP.Core.Shared;
using DEQP.Infrastructure.Models;
using DEQP.PublicAPI.Interfaces;
using DEQP.PublicAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.PublicAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AdminProjectController : ApiController<INewsApiService>
    {
        private readonly DEQP.Infrastructure.Models.DEQPNEWSContext _context;
        private readonly FileManager _manager;
        StorageSettings storageSettings;

        public AdminProjectController(IApiRequestHandler<INewsApiService> newsRequestHandler,
            DEQP.Infrastructure.Models.DEQPNEWSContext context
            , FileManager manager
             , IOptions<StorageSettings> storageSettings) : base(newsRequestHandler
               )
        {
            _context = context;
            _manager = manager;
            this.storageSettings = storageSettings.Value;

        }


        [HttpGet]
        [Route("search")]
        public async Task<IActionResult> Search([FromQuery] BaseResourceParameter resourceParameter)
        {
            var collection = _context.TblProjects
               .AsQueryable();

            if (!string.IsNullOrWhiteSpace(resourceParameter.OrderBy))
            {
                switch (resourceParameter.OrderBy)
                {
                    case "name":
                        collection = collection.OrderBy(x => x.ProjName);
                        break;
                    case "name_desc":
                        collection = collection.OrderByDescending(x => x.ProjName);
                        break;
                    //case "type":
                    //    collection = collection.OrderBy(x => x.NewsGrpId1Navigation.NewsGrpName);
                    //    break;
                    //case "type_desc":
                    //    collection = collection.OrderByDescending(x => x.NewsGrpId1Navigation.NewsGrpName);
                    //    break;
                    //case "date":
                    //    collection = collection.OrderBy(x => x.NewsDate);
                    //    break;
                    //case "date_desc":
                    //    collection = collection.OrderByDescending(x => x.NewsDate);
                    //    break;

                }
            }
            else
            {
                //collection = collection.OrderByDescending(x => x.NewsDate);
            }

            var query = collection.Select(x => new ProjectListDto()
            {
                Id = x.ProjId,
                Name = x.ProjName,
                ProjectUrl = x.ProjUrl,
                RegisterUrl = x.RegisUrl,
                IsPublish = x.Isactive == 1,
                NewsCount = _context.TblNews.Count(n => n.ProjId == x.ProjId),
                AttachCount = _context.TblProjectFiles.Where(xp => xp.ProjFileId == x.ProjId).Count(),
                Update = $"{x.ModifiedDate.Value.ToString("ddMMyyyy HH:mm")} By {x.ModifiedBy}",
            });

            var pagedList = await PagedList<ProjectListDto>.Create(query, resourceParameter.Page, resourceParameter.PageSize);

            return Ok(pagedList);
        }

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> Create([FromForm] ProjectResource resource)
        {

            try
            {
                var project = new TblProject()
                {
                    ProjName = resource.Name,
                    ProjUrl = resource.ProjectUrl,
                    RegisUrl = resource.RegisterUrl,
                    ProjContent = resource.Content,
                    IsactiveProjUrl = resource.IsProjectUrlActive ? 1 : 0,
                    IsactiveRegisUrl = resource.IsRegisterUrlActive ? 1 : 0,
                    CreatedDate = DateTime.Now,
                    Isactive = resource.IsPublish ? 1 : null,
                    CoverFileRename = resource.Cover == null ? null : await _manager.SaveAttachFile(resource.Cover, "Image", "ProjectCover")
                };

                _context.TblProjects.Add(project);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

        }

        [HttpGet]
        [Route("search/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var selected = await _context.TblProjects
                .FirstOrDefaultAsync(x => x.ProjId == id);

            if (selected == null)
            {
                return NotFound(id);
            }

            return Ok(new ProjectDto()
            {
                Id = selected.ProjId,
                Name = selected.ProjName,
                CoverImage = new FileDto()
                {
                    Id = selected.ProjId,
                    Url = string.IsNullOrWhiteSpace(selected.CoverFileRename) ? "" : $"{storageSettings.ProjectCoverUrl}/{selected.CoverFileRename}",
                    Name = selected.CoverFileRename,
                },
                IsRegisterUrlActive = selected.IsactiveRegisUrl == 1,
                IsProjectUrlActive = selected.IsactiveProjUrl == 1,
                ProjectUrl = selected.ProjUrl,
                RegisterUrl = selected.RegisUrl,
                Description = selected.ProjContent,
                IsPublish = selected.Isactive == 1,
            }); ;
        }

        [HttpGet]
        [Route("attach/{id}")]
        public async Task<IActionResult> AttachById(int id)
        {
            var selected = await _context.TblProjectFiles
                .Where(x => x.ProjId == id).ToListAsync();

            if (selected == null)
            {
                return NotFound(id);
            }

            return Ok(selected.Select(x => new FileDto
            {
                Id = x.ProjFileId,
                Url = $"{storageSettings.ProjectFileUrl}/{x.FileRename}",
                Name = x.FileOriName,
            })); ;
        }

        [HttpPost]
        [Route("edit/{id}")]
        public async Task<IActionResult> Edit(int id, [FromForm] ProjectResource resource)
        {
            var selected = await _context.TblProjects
               .FirstOrDefaultAsync(x => x.ProjId == id);

            if (selected == null)
            {
                return NotFound(id);
            }

            try
            {
                selected.ProjName = resource.Name;
                selected.ProjUrl = resource.ProjectUrl;
                selected.RegisUrl = resource.RegisterUrl;
                selected.ProjContent = resource.Content;
                selected.ModifiedDate = DateTime.Now;
                selected.IsactiveProjUrl = resource.IsProjectUrlActive ? 1 : 0;
                selected.IsactiveRegisUrl = resource.IsRegisterUrlActive ? 1 : 0;
                selected.CoverFileRename = resource.Cover == null ? selected.CoverFileRename : await _manager.SaveAttachFile(resource.Cover, "Image", "ProjectCover");
                selected.Isactive = resource.IsPublish ? 1 : null;

                _context.TblProjects.Update(selected);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        public class ProjectAttach
        {
            public IFormFile File { get; set; }
        }

        [HttpPost]
        [Route("attach/{id}")]
        public async Task<IActionResult> Attach(int id, [FromForm] ProjectAttach projectAttach)
        {
            try
            {
                var filename = await _manager.SaveAttachFile(projectAttach.File, "File", "ProjectFile");

                var selected = new TblProjectFile()
                {
                    ProjId = id,
                    FileSize = projectAttach.File.Length,
                    FileOriName = projectAttach.File.FileName,
                    FileRename = filename
                };

                _context.TblProjectFiles.Add(selected);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpDelete]
        [Route("attach/{id}")]
        public async Task<IActionResult> DeleteAttach(int id)
        {
            try
            {
                var selected = await _context.TblProjectFiles.FirstOrDefaultAsync(x => x.ProjFileId == id);

                if (selected != null)
                    _context.TblProjectFiles.Remove(selected);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var selected = await _context.TblProjects.FirstOrDefaultAsync(x => x.ProjId == id);

                if (selected != null)
                    _context.TblProjects.Remove(selected);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpDelete]
        [Route("cover/{id}")]
        public async Task<IActionResult> DeleteCover(int id)
        {
            try
            {
                var selected = await _context.TblProjects.FirstOrDefaultAsync(x => x.ProjId == id);

                if (selected != null)
                    selected.CoverFileRename = null;

                _context.TblProjects.Update(selected);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

    }

    public class ProjectResource
    {
        public string Name { get; set; }
        public string ProjectUrl { get; set; }
        public string RegisterUrl { get; set; }
        public bool IsProjectUrlActive { get; set; }
        public bool IsRegisterUrlActive { get; set; }

        public bool IsPublish { get; set; }
        public string Content { get; set; }
        public IFormFile Cover { get; set; }
    }

    public class ProjectListDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ProjectUrl { get; set; }
        public string RegisterUrl { get; set; }
        public int NewsCount { get; set; }
        public int AttachCount { get; set; }
        public string Update { get; set; }
        public bool IsPublish { get; set; }
    }


    public class ProjectDto
    {
        public int Id { get; set; }
        public FileDto CoverImage { get; set; }
        public string ProjectUrl { get; set; }
        public string RegisterUrl { get; set; }
        public bool IsProjectUrlActive { get; set; }
        public bool IsRegisterUrlActive { get; set; }
        public string Description { get; set; }
        public bool IsPublish { get; set; }
        public string Name { get; internal set; }
    }


}
