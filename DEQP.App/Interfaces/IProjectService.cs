﻿using DEQP.App.Resources.Projects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Interfaces
{
    public interface IProjectService
    {
        IEnumerable<ProjectSearchData> SearchProjects(SearchProjectRequest request);
        ProjectData GetProjectDetail(int id);
        void AddFileViewCount(string filename, string ipAddr);
        IEnumerable<ProjectDropdownData> GetProjectDropdowns();
    }
}
