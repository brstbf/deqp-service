﻿using DEQP.App.Resources.News;
using DEQP.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Interfaces
{
    public interface INewsService
    {
        int CreateNews(CreateNewsRequest request);
        NewsData GetNewsDetail(int id);
        void AddViewCount(AddNewsViewCountRequest request);
        IEnumerable<SearchNewsData> SearchNews(SearchNewsRequest request);
        IEnumerable<SearchNewsData> GetNews(SearchLastedNewsRequest request);
        IEnumerable<NewsCategoryData> GetNewsCategory(NewsCategoryType type);
        IEnumerable<NewsSummaryByCategoryData> GetCategoryNewsSummary(NewsCategoryType type);
        IEnumerable<SearchNewsData> GetReleatedNews(int id);
        void AddCountFile(string filename,string ipAddr);
    }
}
