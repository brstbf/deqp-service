﻿using DEQP.App.Resources.Divisions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Interfaces
{
    public interface IDivisionService
    {
        IEnumerable<DivisionData> Get(GetDivisionRequest request);
        IEnumerable<DivisionDropdownData> GetDropdowns();
    }
}
