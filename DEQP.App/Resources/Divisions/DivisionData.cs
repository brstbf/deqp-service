﻿using DEQP.Core.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace DEQP.App.Resources.Divisions
{
    public class DivisionData
    {
        public int Id { get; set; }
        public string DivisionName { get; private set; }
        public string DivisionNameAbbr { get; private set; }
        public string Preface { get; private set; }
        public string Description { get; private set; }
        public string Contact { get; set; }
        public DateTime? CreatedDatetime { get; private set; }
        public string CreatedDate => CreatedDatetime.ToThai();
        public DateTime? ModifiedDatetime { get; private set; }
        public ICollection<SubDivisionData> SubDivisions  { get;set;}
        public ICollection<DocumentGroupData> DocumentGroups { get; set; }
    }

    public class SubDivisionData {
        public int Id { get; set; }
        public string Name { get; private set; }
        public int OrderNo { get; private set; }
        public DateTime? CreatedDatetime { get; private set; }
        public DateTime? ModifiedDatetime { get; private set; }
        public ICollection<DivisionOfficerData> DivisionOfficers { get; set; }
    }
    public class DivisionOfficerData
    {
        public int Id { get; set; }
        public string Name { get; private set; }
        public string Position { get; private set; }
        public string Type { get; private set; }
        public bool IsDirector { get; private set; }
        [JsonIgnore]
        public string ImageRename { get; private set; }
        public string ImageUrl { get; set; }
        public string Number { get; private set; }
        public string IntNumber { get; private set; }
        public string Fax { get; private set; }
        public string Email { get; private set; }
        public DateTime? CreatedDatetime { get; private set; }
        public DateTime? ModifiedDatetime { get; private set; }
    }
    public class DocumentGroupData
    {
        public int Id { get; set; }
        public string Name { get; private set; }
        public int OrderNo { get; private set; }
        public DateTime? CreatedDatetime { get; private set; }
        public DateTime? ModifiedDatetime { get; private set; }
        public ICollection<DivisionFileData> DivisionFiles { get; set; }
    }

    public class DivisionFileData {
        public int Id { get; set; }
        public string OriginalName { get; private set; }
        [JsonIgnore]
        public string Rename { get; set; }
        public string FileUrl { get; set; }
        public string Type { get; set; }
        public decimal Size { get; set; }
        public int OrderNo { get; set; }
    }


}
