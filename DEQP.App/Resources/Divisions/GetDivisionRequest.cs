﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Resources.Divisions
{
    public class GetDivisionRequest
    {
        public int? Id { get; set; }
    }
}

