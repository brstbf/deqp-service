﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Resources.Divisions
{
    public class DivisionDropdownData
    {
        public int Id { get; set; }
        public string DivisionName { get; private set; }
        public string DivisionNameAbbr { get; private set; }
    }
}
