﻿using DEQP.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Resources.News
{
    public class SearchNewsData
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string DivisionName { get; set; }
        public DateTime? Date { get; set; }
        public int ViewCount { get; set; }
        public string CoverFileRename { get; set; }
        public string Tags { get; set; }
        public string PrimaryCategory { get; set; }
        public int PrimaryCategoryId { get; set; }
        public string SecondaryCategory { get; set; }
        public int? SecondaryCategoryId { get; set; }
        public string ProjectName { get; set; }
        public string Preface { get; set; }
        public NewsCategoryType PrimartCategoryType { get; set; }
        public NewsCategoryType SecondaryCategoryType { get; set; }
    }
}
