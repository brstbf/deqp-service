﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Resources.News
{
    public class NewsRequestBase
    {
        public string Title { get; set;}
        public string CoverFileRename { get; set;}
        public string Preface { get; set;}
        public string Content { get; set;}
        public IEnumerable<string> Tags { get; set;}
        public int? CategoryId { get; set;}
        public string FiscalYear { get; set;}
        public bool TopNewsFlag { get; set;}
        public int? DivisionId { get; set;}
        public bool IsPublicNews { get; set; }
        public DateTime? PublishDate { get; set; }
    }
}
