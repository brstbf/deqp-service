﻿using DEQP.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Resources.News
{
    public class SearchNewsRequest
    {
        public string Query { get; set; }
        public int? DivisionId { get; set; }
        public int FiscalYear { get; set; }
        public int? CategoryId { get; set; }
        public string CategoryType { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int? Limit { get; set; }
        public int? Id { get; set; }
        public int? ProjectId { get; set; }
    }
}
