﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Resources.News
{
    public class CreateNewsRequest : NewsRequestBase
    {
        public int? ProjectId { get; set; }
        public int CreatorId { get; set; }
    }
}
