﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Resources.News
{
    public class EditNewsRequest : NewsRequestBase
    {
        public int? ModifierId { get; private set; }
    }
}
