﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Resources.News
{
    public class NewsData
    {
        public string Title { get; set; }
        public string CoverFileRename { get; set; }
        public string Preface { get; set; }
        public string Content { get; set; }
        public DateTime? Date { get; set; }
        public string Tags { get; set; }
        public int? ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string DivisionName { get; set; }
        public int ViewCount { get; set; }
        public string PrimaryCategory { get; set; }
        public int? PrimaryGroupId { get; set; }
        public int? PrimaryType { get; set; }
        public string SecondaryCategory { get; set; }
        public int? SecondaryGroupId { get; set; }
        public int? SecondaryType { get; set; }
        public ICollection<NewsFileData> Files { get; set; } = new List<NewsFileData>();

    }

    public class NewsFileData
    {
        public string Id { get; set; }
        public string OriginalName { get; set; }
        public string Rename { get; set; }
        public string TypeName { get; set; }
        public double? FileSize { get; set; }
        public int? OrderNo { get; set; }
        public int ViewCount { get; set; }
        public int DownloadCount { get; set; }
    }


}
