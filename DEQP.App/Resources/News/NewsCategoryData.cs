﻿using DEQP.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Resources.News
{
    public class NewsCategoryData
    {
        public int Id { get; set; }
        public string CategoryName { get; private set; }
        public NewsCategoryType Type { get; private set; }
    }
}
