﻿using DEQP.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Resources.News
{
    public class SearchLastedNewsRequest
    {
        public int? Limit { get; set; }
        public NewsCategoryType? CategoryType { get; set; }
        public int? CategoryId { get; set; }
        public bool? IsTopNews { get; set; }
        public int? LimitDay { get; set; }
        public int? Id { get; set; }
        public int? DivisionId { get; set; }
    }
}
