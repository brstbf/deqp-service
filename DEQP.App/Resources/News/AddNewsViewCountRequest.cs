﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Resources.News
{
    public class AddNewsViewCountRequest
    {
        public int Id { get; set; }
        public string IpAddress { get; set; }
    }
}
