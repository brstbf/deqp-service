﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Resources.Projects
{
    public class SearchProjectRequest
    {
        public int? Limit { get; set; }
        public string Query { get; set; }
    }

    //public class ProjectOrderColumn
    //{
    //    public const 
    //}
}
