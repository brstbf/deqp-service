﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Resources.Projects
{
    public class ProjectNewsData
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Preface { get; set; }
        public string CoverFileRename { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
