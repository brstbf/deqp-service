﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Resources.Projects
{
    public class ProjectSearchData
    {
        public int Id { get; set; }
        public string CoverFileRename { get; set; }
        public string Name { get; private set; }
        public string DivisionName { get; set; }
        public DateTime? CreatedDate { get; private set; }
    }
}
