﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Resources.Projects
{
    public class ProjectFileData
    {
        public string OriginalName { get; private set; }
        public string Rename { get; private set; }
        public string TypeName { get; private set; }
        public double? FileSize { get; private set; }
        public int? OrderNo { get; private set; }
    }
}
