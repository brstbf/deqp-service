﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Resources.Projects
{
    public class ProjectData
    {
        public int Id { get; set; }
        public string Name { get; private set; }
        public string CoverFileRename { get; private set; }
        public string Content { get; private set; }
        public string DivisionName { get; set; }
        public string ProjectUrl { get; set; }
        public string RegisterUrl { get; set; }
        public bool IsactiveProjectUrl { get; set; }
        public bool IsactiveRegisterUrl { get; set; }
        public IEnumerable<ProjectNewsData> News { get; set; }
        public IEnumerable<ProjectFileData> Files { get; set; }

    }
}
