﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Resources.Projects
{
    public class ProjectDropdownData
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}
