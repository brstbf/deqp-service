﻿using AutoMapper;
using DEQP.App.Resources.Divisions;
using DEQP.Core.Domain.DivisionAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.MapperProfiles
{
    public class DivisionProfile: Profile
    {
        public DivisionProfile()
        {
            CreateMap<Division, DivisionData>();
            CreateMap<SubDivision, SubDivisionData>();
            CreateMap<DivisionOfficer, DivisionOfficerData>();
            CreateMap<DocumentGroup, DocumentGroupData>();
            CreateMap<DivisionFile, DivisionFileData>();
        }
    }
}
