﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DEQP.App.Interfaces;
using DEQP.App.Resources.News;
using DEQP.Core.Constants;
using DEQP.Core.Domain;
using DEQP.Core.Domain.AdminsAggregate;
using DEQP.Core.Domain.NewsAggregate;
using DEQP.Core.Exceptions;
using DEQP.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Services
{
    public class NewsService : INewsService
    {
        IMapper mapper;
        ILogger<NewsService> logger;
        IDEQPUnitOfWork uow;
        public NewsService(IMapper mapper, ILogger<NewsService> logger, IDEQPUnitOfWork uow)
        {
            this.mapper = mapper;
            this.logger = logger;
            this.uow = uow;
        }

        public void AddViewCount(AddNewsViewCountRequest request)
        {
            var news = uow.NewsRepository.GetById(request.Id);
            if (news == null)
                return;

            news.AddViewCount(request.IpAddress);

            uow.SaveChange();
        }

        public int CreateNews(CreateNewsRequest request)
        {
            //logger.LogInformation("Create News Request");

            //var tag = request.Tags == null ? "" : string.Join(",", request.Tags);

            //var publicCategoryId = CategoryConstant.PUBLIC_CATEGORY_ID;
            //int? subCatId = request.IsPublicNews ? publicCategoryId : null;

            //var adminProfile = uow.AdminRepository.GetById(request.CreatorId);
            //if (adminProfile == null || adminProfile.IsActive != true)
            //    throw new ApplicationServiceException("Creator not found.");

            //if(adminProfile.DivisionPermission == DivisionRoleType.Specific)
            //{
            //    if (adminProfile.DivisionId != request.DivisionId)
            //        throw new ApplicationServiceException("Permission denied for this Division.");
            //}

            //var existCat = uow.NewsCategoryRepository.Exist(request.CategoryId.Value);
            //if (!existCat)
            //    throw new ApplicationServiceException("Category not found.");

            //check project exist



            //var news = new News(
            //    request.Title,
            //    request.CoverFileRename,
            //    request.Preface,
            //    request.Content,
            //    request.PublishDate,
            //    tag,
            //    request.CategoryId,
            //    subCatId,
            //    request.FiscalYear,
            //    request.TopNewsFlag,
            //    request.DivisionId,
            //    request.ProjectId,
            //    adminProfile);

            //uow.NewsRepository.Add(news);

            uow.SaveChange();

            //logger.LogInformation($"Create News Success, ID: {news.Id}");
            return 1;

        }

        public IEnumerable<NewsSummaryByCategoryData> GetCategoryNewsSummary(NewsCategoryType type)
        {
            var sql = @"
                    SELECT g.news_grp_id AS Id,
                           g.news_grp_name AS CategoryName,
                           g.news_type_code AS Type,
                           v.num AS Count
                    FROM dbo.tbl_news_group g
                        OUTER APPLY
                    (
                        SELECT COUNT(n.news_id) AS num
                        FROM dbo.tbl_news n
                        WHERE n.news_grp_id1 = g.news_grp_id
                              OR n.news_grp_id2 = g.news_grp_id
                    ) v
                    where g.news_type_code = @type;";
            var param = new { type };
            return uow.NewsRepository.Query<NewsSummaryByCategoryData>(sql, param);
        }

        public NewsData GetNewsDetail(int id)
        {
            var sql = @"
                    SELECT n.news_title AS Title,
                           n.cover_file_rename AS CoverFileRename,
                           n.news_preface AS Preface,
                           n.news_content AS Content,
                           n.news_date AS Date,
                           n.news_tags AS Tags,
                           n.proj_id AS ProjectId,
                           p.proj_name AS ProjectName,
                           d.divi_name AS DivisionName,
                           c.num AS ViewCount,
                           g1.news_grp_name AS PrimaryCategory,
                           g1.news_grp_id AS PrimaryGroupId,
                           g1.news_type_code AS PrimaryType,
                           g2.news_grp_name AS SecondaryCategory,
                           g2.news_grp_id AS SecondaryGroupId,
                           g2.news_type_code AS SecondaryType
                    FROM dbo.tbl_news n
                        INNER JOIN dbo.tbl_division d
                            ON d.divi_id = n.divi_id
                        CROSS APPLY
                    (
                        SELECT COUNT(c.news_id) AS num
                        FROM dbo.tbl_counter_news c
                        WHERE c.news_id = n.news_id
                    ) AS c
                        INNER JOIN dbo.tbl_news_group g1
                            ON g1.news_grp_id = n.news_grp_id1
                        LEFT JOIN dbo.tbl_news_group g2
                            ON g2.news_grp_id = n.news_grp_id2
                        LEFT JOIN dbo.tbl_project p
                            ON p.proj_id = n.proj_id
                    WHERE n.isactive = 1
                          AND n.news_date < GETDATE()
                          AND n.news_id = @newId;";

            sql += @"
                SELECT file_ori_name as OriginalName,
                       file_rename as Rename,
                       file_type_name as TypeName,
                       file_size as FileSize,
                       order_no as OrderNo,
                       v.num as ViewCount,
                       d.num as DownloadCount,
                        news_file_id as Id
                FROM dbo.tbl_news_file f
                    CROSS APPLY
                (
                    SELECT COUNT(count_file_id) AS num
                    FROM dbo.tbl_counter_file cf
                    WHERE cf.count_file_id = f.news_file_id
                          AND cf.count_type_code = '10'
                ) v
                    CROSS APPLY
                (
                    SELECT COUNT(count_file_id) AS num
                    FROM dbo.tbl_counter_file cf
                    WHERE cf.count_file_id = f.news_file_id
                          AND cf.count_type_code = '20'
                ) d
                WHERE f.news_id = @newId;";

            var param = new
            {
                newId = id
            };

            using (var multi = uow.NewsRepository.QueryMultiple(sql, param))
            {
                var news = multi.Read<NewsData>().FirstOrDefault();
                if (news == null)
                    return null;

                news.Files = multi.Read<NewsFileData>().ToList();

                return news;
            }

        }

        public IEnumerable<SearchNewsData> SearchNews(SearchNewsRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Query))
                request.Query = null;

            var sql = "SELECT n.news_id AS Id" +
                ", n.news_title AS Title" +
                ", d.divi_name AS DivisionName" +
                ", n.news_date AS Date" +
                ", c.num AS ViewCount" +
                ", n.cover_file_rename AS CoverFileRename" +
                ", n.news_preface as Preface" +
                ", n.news_tags AS Tags" +
                ", g1.news_grp_name AS PrimaryCategory" +
                ", g2.news_grp_name AS SecondaryCategory" +
                ", g1.news_grp_id AS PrimaryCategoryId" +
                ", g2.news_grp_id AS SecondaryCategoryId" +
                ", g1.news_type_code as PrimartCategoryType"+
                ", g2.news_type_code as PrimartCategoryType" +
                ", p.proj_name as ProjectName" +
                " FROM dbo.tbl_news n" +
                " INNER JOIN dbo.tbl_division d" +
                " ON d.divi_id = n.divi_id" +
                " CROSS APPLY(" +
                    " SELECT COUNT(c.news_id) AS num" +
                    " FROM dbo.tbl_counter_news c" +
                    " WHERE c.news_id = n.news_id )AS c" +
                 " INNER JOIN dbo.tbl_news_group g1" +
                 " ON g1.news_grp_id = n.news_grp_id1" +
                 " LEFT JOIN dbo.tbl_news_group g2" +
                 " ON g2.news_grp_id = n.news_grp_id2" +
                 " LEFT JOIN dbo.tbl_project p" +
                 " ON n.proj_id = p.proj_id"+
                 " WHERE n.isactive =1" +
                 " AND n.news_date < GETDATE()" +
                 " And ( @catType is null or @catType = g1.news_type_code or @catType = g2.news_type_code)" +
                 " And ( @catId is null or @catId = g1.news_grp_id or @catId = g2.news_grp_id)" +
                 " And ( @projId is null or @projId = n.proj_id)" +
                 " AND ( @div is null or n.divi_id = @div )"+
                 " AND ( @title is null or n.news_title like '%'+@title+'%' )";

            var param = new
            {
                catType = request.CategoryType,
                div = request.DivisionId,
                catId = request.CategoryId,
                projId = request.ProjectId,
                title = request.Query
            };

            return uow.NewsRepository.Query<SearchNewsData>(sql, param);
        }

        public IEnumerable<NewsCategoryData> GetNewsCategory(NewsCategoryType type)
        {
            var sql = @"
                    SELECT news_grp_id AS Id,
                           news_grp_name AS CategoryName,
                           news_type_code AS Type
                    FROM dbo.tbl_news_group
                    WHERE news_type_code = @type;";

            return uow.NewsRepository.Query<NewsCategoryData>(sql, new { type });
        }

        public IEnumerable<SearchNewsData> GetNews(SearchLastedNewsRequest request)
        {
            var limit = request.Limit ?? 10;
            limit = limit > 100 ? 100 : limit;
            var sql = $"SELECT top {limit} n.news_id AS Id" +
                ", n.news_title AS Title" +
                ", d.divi_name AS DivisionName" +
                ", n.news_date AS Date" +
                ", c.num AS ViewCount" +
                ", n.cover_file_rename AS CoverFileRename" +
                ", n.news_preface as Preface" +
                ", n.news_tags AS Tags" +
                ", g1.news_grp_name AS PrimaryCategory" +
                ", g1.news_grp_id AS PrimaryCategoryId" +
                ", g2.news_grp_name AS SecondaryCategory" +
                ", g2.news_grp_id AS SecondaryCategoryId" +
                " FROM dbo.tbl_news n" +
                " INNER JOIN dbo.tbl_division d" +
                " ON d.divi_id = n.divi_id" +
                " CROSS APPLY(" +
                    " SELECT COUNT(c.news_id) AS num" +
                    " FROM dbo.tbl_counter_news c" +
                    " WHERE c.news_id = n.news_id )AS c" +
                 " INNER JOIN dbo.tbl_news_group g1" +
                 " ON g1.news_grp_id = n.news_grp_id1" +
                 " LEFT JOIN dbo.tbl_news_group g2" +
                 " ON g2.news_grp_id = n.news_grp_id2" +
                 " WHERE n.isactive = 1" +
                 " AND n.news_date < GETDATE()" +
                 " And ( @catType is null or @catType = g1.news_type_code or @catType = g2.news_type_code)" +
                 " And ( @catId is null or @catId = g1.news_grp_id or @catId = g2.news_grp_id)" +
                 " And ( @isTopNews is null or n.top_news_flag = @isTopNews)" +
                 " And ( @date is null or n.news_date >= @date)" +
                 " And ( @id is null or n.news_id = @id)" +
                 " And ( @divId is null or n.divi_id = @divId)" +
                 " Order By n.news_date DESC";


            DateTime? date = null;
            if (request.LimitDay.GetValueOrDefault() > 0)
            {
                date = DateTime.Now.Date.AddDays(request.LimitDay.Value * -1);
            }
            var param = new
            {
                catType = request.CategoryType,
                catId = request.CategoryId,
                isTopNews = request.IsTopNews,
                date,
                id = request.Id,
                divId = request.DivisionId
            };

            
            return uow.NewsRepository.Query<SearchNewsData>(sql, param);
        }

        public void AddCountFile(string filename, string ipAddr)
        {
            filename =  Path.GetFileNameWithoutExtension(filename);
            var query = uow.NewsRepository.Query<int>("select news_id from dbo.tbl_news_file where file_rename = @filename", new { filename });
            if (!query.Any())
                return;

            var newsId = query.First();
            var news = uow.NewsRepository
                .List(m => m.Id == newsId)
                .Include("_files")
                .FirstOrDefault();
            if (news == null) return;
            news.AddFileViewCount(filename, ipAddr);

            uow.SaveChange();
        }

        public IEnumerable<SearchNewsData> GetReleatedNews(int id)
        {
            var query = uow.NewsRepository.Query<dynamic>("select news_grp_id1 as g1,news_grp_id2 as g2  from tbl_news where news_id = @id", new { id });
            if (!query.Any())
                return null;
            var row = query.First();
            var group1 = (int)row.g1;
            int? group2 = null;
            if (row.g2 != null)
                group2 = (int)row.g2;

            var newsId1Query = uow.NewsRepository
                .Query<int>("select top 2 news_id from tbl_news n where isactive = 1 and n.news_date < GETDATE() and news_id <> @id and news_id < @id", new { id });

            var newsId2Query = uow.NewsRepository
                .Query<int>("select top 2 news_id from tbl_news n where isactive = 1 and n.news_date < GETDATE() and news_id <> @id and news_id > @id", new { id });

            var newsList = new List<SearchNewsData>();
            var newsIdList = new List<int>();

            if(newsId1Query.Any())
            {
                newsIdList.AddRange(newsId1Query.ToList());
            }
            if (newsId2Query.Any())
            {
                newsIdList.AddRange(newsId2Query.ToList());
            }

            if(newsIdList.Count > 2)
            {
                var t = newsIdList[1];
                newsIdList[1] = newsIdList[2];
                newsIdList[2] = t;
            }


            foreach(var newsId in newsIdList)
            {
                if (newsList.Count >= 2)
                    break;

                var news = GetNews(new SearchLastedNewsRequest { Id = newsId }).First();
                newsList.Add(news);
            }

            return newsList;
        }
    }
}
