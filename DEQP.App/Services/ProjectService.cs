﻿using AutoMapper;
using DEQP.App.Interfaces;
using DEQP.App.Resources.Projects;
using DEQP.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Services
{
    public class ProjectService : IProjectService
    {
        IDEQPUnitOfWork uow;
        ILogger<ProjectService> logger;
        IMapper mapper;
        public ProjectService(IDEQPUnitOfWork uow, ILogger<ProjectService> logger, IMapper mapper)
        {
            this.logger = logger;
            this.uow = uow;
            this.mapper = mapper;
        }
        public IEnumerable<ProjectSearchData> SearchProjects(SearchProjectRequest request)
        {
            var limit = request.Limit.HasValue ? $"Top {request.Limit}" : "";
            var sql = @$"
                SELECT {limit} p.proj_id AS Id,
                       p.proj_name AS Name,
                       d.divi_name AS DivisionName,
                       p.created_date AS CreatedDate,
                       p.cover_file_rename AS CoverFileRename
                FROM dbo.tbl_project p
                    OUTER APPLY
                (
                    SELECT d.divi_name
                    FROM dbo.tbl_admin_profile a
                        INNER JOIN dbo.tbl_division d
                            ON d.divi_id = a.divis_id
                    WHERE a.admin_id = p.created_by
                ) d
                WHERE p.isactive = 1
                ORDER BY p.order_no DESC;";

            var param = new { };

            return uow.ProjectRepository.Query<ProjectSearchData>(sql, param);
        }

        public ProjectData GetProjectDetail(int id)
        {
            var sql = @"
                SELECT p.proj_id AS Id,
                       p.proj_name AS Name,
                       p.cover_file_rename AS CoverFileRename,
                       p.proj_content AS Content,
                       d.divi_name AS DivisionName,
                       p.proj_url AS ProjectUrl,
                       p.regis_url AS RegisterUrl,
                       p.isactive_proj_url AS IsactiveProjectUrl,
                       p.isactive_regis_url AS IsactiveRegisterUrl,
                       p.created_date AS CreatedDate
                FROM dbo.tbl_project p
                    OUTER APPLY
                (
                    SELECT d.divi_name
                    FROM dbo.tbl_admin_profile a
                        INNER JOIN dbo.tbl_division d
                            ON d.divi_id = a.divis_id
                    WHERE a.admin_id = p.created_by
                ) d
                WHERE p.isactive = 1
                      AND p.proj_id = @id;

                SELECT n.news_id AS Id,
                       n.news_title AS Title,
                       n.news_preface AS Preface,
                       n.cover_file_rename AS CoverFileRename,
                       n.news_date AS CreatedDate
                FROM dbo.tbl_news n
                WHERE n.proj_id = @id;

                SELECT file_ori_name AS OriginalName,
                       file_rename AS Rename,
                       file_type_name AS TypeName,
                       file_size AS FileSize,
                       order_no AS OrderNo
                FROM dbo.tbl_project_file
                WHERE proj_id = @id;";

            using (var multi = uow.ProjectRepository.QueryMultiple(sql, new { id }))
            {
                var project = multi.Read<ProjectData>().FirstOrDefault();
                if (project == null)
                    return null;

                project.News = multi.Read<ProjectNewsData>().ToList();
                project.Files = multi.Read<ProjectFileData>().ToList();

                return project;
            }
        }

        public void AddFileViewCount(string filename, string ipAddr)
        {
            filename = Path.GetFileNameWithoutExtension(filename);
            var query = uow.ProjectRepository.Query<int>("select proj_id from dbo.tbl_project_file where file_rename = @filename", new { filename });
            if (!query.Any())
                return;
            var projectId = query.First();

            var project = uow.ProjectRepository.List(m => m.Id == projectId).Include("_files").FirstOrDefault();
            if (project == null) return;
            //project.CoverFileRename
        }

        public IEnumerable<ProjectDropdownData> GetProjectDropdowns()
        {
            var sql = "SELECT proj_name AS Name,proj_id AS Id FROM dbo.tbl_project WHERE isactive = 1 ORDER BY order_no";
            return uow.ProjectRepository.Query<ProjectDropdownData>(sql);
        }
    }
}
