﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DEQP.App.Interfaces;
using DEQP.App.Resources.Divisions;
using DEQP.Core.Domain.DivisionAggregate;
using DEQP.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.App.Services
{
    public class DivisionService : IDivisionService
    {
        IDEQPUnitOfWork uow;
        IMapper mapper;
        public DivisionService(IDEQPUnitOfWork uow,IMapper mapper)
        {
            this.uow = uow;
            this.mapper = mapper;
        }

        public IEnumerable<DivisionData> Get(GetDivisionRequest request)
        {
            var divisions = uow.DivisionRepository
                .List(m => m.IsActive == true)
                .AsNoTracking();
            if (request.Id != null)
                divisions = divisions.Where(m => m.Id == request.Id);

            return mapper.Map<IEnumerable<Division>, IEnumerable<DivisionData>>(divisions);
        }

        public IEnumerable<DivisionDropdownData> GetDropdowns()
        {
            var query = @"SELECT divi_id as id,
                           divi_name AS DivisionName,
                           divi_name_abbr AS DivisionNameAbbr,
                           modified_datetime
                        FROM dbo.tbl_division
                        WHERE isactive = 1;";

            return uow.DivisionRepository.Query<DivisionDropdownData>(query);                   
        }




    }
}
