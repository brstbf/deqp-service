﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Core.Exceptions
{
    public class StorageException:System.Exception
    {
        public StorageException(string message):base(message)
        {

        }
    }
}
