﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Core.Exceptions
{
    public class ApplicationServiceException : System.Exception
    {
        public ApplicationServiceException(string message) : base(message)
        {

        }
    }
}
