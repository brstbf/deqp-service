﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DEQP.Core.Domain.DivisionAggregate;
using DEQP.Core.Interfaces;
using DEQP.Core.Shared;

namespace DEQP.Core.Domain.AdminsAggregate
{
    public class Admin : Entity,IAggregateRoot
    {
        public int? AdminRefId { get; private set; }
        public int? DivisionId { get; private set; }
        public ProjectRoleType? ProjectPermission { get; private set; }
        public DivisionRoleType? DivisionPermission { get; private set; }
        public bool IsActive { get; private set; }
        public int? ApprovedBy { get; private set; }
        public int? ModifiedBy { get; private set; }
        public DateTime? ApprovedDate { get; private set; }
        public DateTime? ModifiedDate { get; private set; }

        private Division _division;
        public string DivisionName => _division?.DivisionName;

        private Admin()
        {

        }

        public Admin(int? adminRefId, int? divisionId, ProjectRoleType? projectPermission, DivisionRoleType? divisionPermission)
        {
            AdminRefId = adminRefId;
            DivisionId = divisionId;
            ProjectPermission = projectPermission;
            DivisionPermission = divisionPermission;
        }



        //public virtual ICollection<TblAdminLog> TblAdminLogs { get; private set; }
        //public virtual ICollection<TblAdminProjectRole> TblAdminProjectRoles { get; private set; }
        //public virtual ICollection<TblAdminRole> TblAdminRoles { get; private set; }
        //public virtual ICollection<TblDivision> TblDivisionCreatedByNavigations { get; private set; }
        //public virtual ICollection<TblDivision> TblDivisionModifiedByNavigations { get; private set; }
    }

    public enum ProjectRoleType
    {
        All = 99,
        Specific = 10
    }

    public enum DivisionRoleType
    {
        All = 99,
        Specific = 10
    }
}
