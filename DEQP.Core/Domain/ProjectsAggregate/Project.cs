﻿using DEQP.Core.Domain.AdminsAggregate;
using DEQP.Core.Domain.NewsAggregate;
using DEQP.Core.Interfaces;
using DEQP.Core.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Core.Domain.ProjectsAggregate
{
    public class Project : Entity, IAggregateRoot
    {
        public string Name { get; private set; }
        public string CoverFileRename { get; private set; }
        public string Content { get; private set; }
        public string ProjectUrl { get; private set; }
        public string RegisterUrl { get; private set; }
        public bool IsactiveProjectUrl { get; private set; }
        public bool IsactiveRegisterUrl { get; private set; }
        public bool Isactive { get; private set; }
        public int? OrderNo { get; private set; }
        public int? CreatedById { get; private set; }
        public int? ModifiedById { get; private set; }
        public DateTime? CreatedDate { get; private set; }
        public DateTime? ModifiedDate { get; private set; }

        private List<ProjectFile> _files = new List<ProjectFile>();
        private List<News> _news = new List<News>();

        [NotMapped]
        public IReadOnlyCollection<ProjectFile> Files => _files.AsReadOnly();
        [NotMapped]
        public IReadOnlyCollection<News> News => _news.AsReadOnly();

        public string DivisionName => _creator?.DivisionName;

        private Admin _creator;
        private Admin _modifier;

        private Project()
        {

        }

        public Project(string name, string coverFileRename, string content, string projectUrl, string registerUrl, Admin creator)
        {
            CheckNullOfEmptyString(name, nameof(name));
            CheckNullOfEmptyString(coverFileRename, "CoverFile");
            CheckNull(creator, nameof(creator));

            Name = name;
            CoverFileRename = coverFileRename;
            Content = content;
            ProjectUrl = projectUrl;
            RegisterUrl = registerUrl;
            _creator = creator;

            Isactive = true;
            IsactiveProjectUrl = true;
            IsactiveRegisterUrl = true;

            CreatedDate = DateTime.Now;
        }
    }
}
