﻿using DEQP.Core.Domain.NewsAggregate;
using DEQP.Core.Interfaces;
using DEQP.Core.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Core.Domain
{
    public class NewsCategory : Entity, IAggregateRoot
    {
        public string CategoryName { get; private set; }
        public NewsCategoryType Type { get; private set; }
        public int? OrderNo { get; private set; }

        private List<News> _primaryNews = new List<News>();
        private List<News> _secondaryNews = new List<News>();


    }

    public enum NewsCategoryType
    {
        Announcement = 10,
        Procuer = 20,
        Project = 30
    }
}

