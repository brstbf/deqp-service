﻿using DEQP.Core.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Core.Domain.NewsAggregate
{
    public class NewsViewCounter:Entity
    {
        public int? NewsId { get; private set; }
        public string IpAddress { get; private set; }
        public DateTime? CountDate { get; private set; }

        private  News _news;

        private NewsViewCounter()
        {

        }
        internal NewsViewCounter(int newsId,string ipAddr)
        {
            NewsId = newsId;
            IpAddress = ipAddr;
            CountDate = DateTime.Now;
        }
    }
}
