﻿using DEQP.Core.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Core.Domain.NewsAggregate
{
    public class FileCounter:Entity
    {
        public int? FileId { get; set; }
        public FileTpye? TypeCode { get; set; }
        public string IpAddress { get; set; }
        public DateTime? CountDate { get; set; }

        private FileCounter()
        {

        }

        internal FileCounter(int? fileId, FileTpye typeCode, string ipAddress)
        {
            FileId = fileId;
            TypeCode = typeCode;
            IpAddress = ipAddress;
            CountDate = DateTime.Now;
        }
    }

    public enum FileTpye
    {
        View = 10,
        Download = 20
    }
}
