﻿using DEQP.Core.Domain.AdminsAggregate;
using DEQP.Core.Domain.DivisionAggregate;
using DEQP.Core.Domain.ProjectsAggregate;
using DEQP.Core.Interfaces;
using DEQP.Core.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Core.Domain.NewsAggregate
{
    public class News : Entity, IAggregateRoot
    {
        public string Title { get; private set; }
        public string CoverFileRename { get; private set; }
        public string Preface { get; private set; }
        public string Content { get; private set; }
        public DateTime? Date { get; private set; }
        public string Tags { get; private set; }
        public int? PrimaryGroupId { get; private set; }
        public int? SecondaryGroupId { get; private set; }
        public string FiscalYear { get; private set; }
        public bool? TopNewsFlag { get; private set; }
        public bool? IsActive { get; private set; }
        public int? DivisionId { get; private set; }
        public int? ProjectId { get; private set; }
        public int? CreatedById { get; private set; }
        public int? ModifiedById { get; private set; }
        public DateTime? CreatedDate { get; private set; }
        public DateTime? ModifiedDate { get; private set; }
        public string DivisionName => _division?.DivisionName;
        private List<NewsViewCounter> _viewCounters = new List<NewsViewCounter>();
        public string PrimaryCategory => _primaryNewsCategory?.CategoryName;
        public NewsCategoryType? PrimaryType => _primaryNewsCategory?.Type;
        public string SecondaryCategory => _secondaryNewsCategory?.CategoryName;
        public NewsCategoryType? SecondaryType => _secondaryNewsCategory?.Type;
        public IReadOnlyCollection<NewsFile> Files => _files.AsReadOnly();
        public string ProjectName => _project?.Name;


        private Division _division;
        private NewsCategory _primaryNewsCategory;
        private NewsCategory _secondaryNewsCategory;
        private Admin _createdBy;
        private Admin _modifiedBy;
        private List<NewsFile> _files = new List<NewsFile>();
        private Project _project;

        private News()
        {

        }

        public News(string title, string coverFileRename, string preface, string content, DateTime? date, string tags, int? primaryGroupId, int? secondaryGroupId, string fiscalYear, bool? topNewsFlag, Division division, Project project, Admin creator)
        {
            CheckNullOfEmptyString(title, nameof(title));
            CheckNullOfEmptyString(coverFileRename, "Cover Image");
            CheckNull(creator, nameof(creator));
            CheckNull(division, nameof(division));


            Title = title;
            CoverFileRename = coverFileRename;
            Preface = preface;
            Content = content;
            Date = date;
            Tags = tags;
            PrimaryGroupId = primaryGroupId;
            SecondaryGroupId = secondaryGroupId;
            FiscalYear = fiscalYear;
            TopNewsFlag = topNewsFlag;
            _division = division;
            _project = project;
            _createdBy = creator;

            if (date == null)
                date = DateTime.Now;

            CreatedDate = DateTime.Now;
            IsActive = true;
        }

        public void AddViewCount(string ipAddr)
        {
            _viewCounters.Add(new NewsViewCounter(Id, ipAddr));
        }

        public void AddFile(string originalName, string rename, string typeName, double? fileSize)
        {
            var lastOrder = _files.Count == 0 ? 0 : _files.Max(a => a.OrderNo.GetValueOrDefault());
            _files.Add(new NewsFile(originalName, rename, typeName, fileSize, lastOrder + 1, Id));
        }
        public void AddFileViewCount(string filename,string ipAddr)
        {
            var file = _files.FirstOrDefault(f => f.Rename.ToLower() == filename.ToLower());
            if (file == null) return;
            file.AddViewCount(ipAddr);
        }


        public void SetProject(Project project)
        {
            CheckNull(project, nameof(project));

            this._project = project;
        }
    }
}
