﻿using DEQP.Core.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Core.Domain.NewsAggregate
{
    public class NewsFile: Entity
    {
        public string OriginalName { get; set; }
        public string Rename { get; set; }
        public string TypeName { get; set; }
        public double? FileSize { get; set; }
        public int? OrderNo { get; set; }
        public int? NewsId { get; set; }

        public int ViewCount => _fileCounters.Where(m => m.TypeCode == FileTpye.View).Count();
        public int DownloadCount => _fileCounters.Where(m => m.TypeCode == FileTpye.Download).Count();
        List<FileCounter> _fileCounters = new List<FileCounter>();

        private NewsFile()
        {

        }

        internal NewsFile(string originalName, string rename, string typeName, double? fileSize, int? orderNo, int? newsId)
        {
            OriginalName = originalName;
            Rename = rename;
            TypeName = typeName;
            FileSize = fileSize;
            OrderNo = orderNo;
            NewsId = newsId;
        }

        public void AddViewCount(string ipAddr)
        {
            _fileCounters.Add(new FileCounter(Id, FileTpye.View, ipAddr));
        }

        public void AddDownloadCount(string ipAddr)
        {
            _fileCounters.Add(new FileCounter(Id, FileTpye.Download, ipAddr));
        }
    }
}
