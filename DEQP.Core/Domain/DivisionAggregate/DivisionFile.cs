﻿using DEQP.Core.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Core.Domain.DivisionAggregate
{
    public class DivisionFile:Entity
    {
        public string OriginalName { get; private set; }
        public string Rename { get; set; }
        public string Type { get; set; }
        public double Size { get; set; }
        public int OrderNo { get; set; }
        public int DocumentGroupId { get; set; }

        public DocumentGroup DocumentGroup { get; private set; }
    }
}
