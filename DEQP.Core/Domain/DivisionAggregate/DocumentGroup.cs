﻿using DEQP.Core.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Core.Domain.DivisionAggregate
{
    public class DocumentGroup:Entity
    {
        public string Name { get; private set; }
        public int DivisionId { get; private set; }
        public int OrderNo { get; private set; }
        public int? CreatedBy { get; private set; }
        public int? ModifiedBy { get; private set; }
        public DateTime? CreatedDatetime { get; private set; }
        public DateTime? ModifiedDatetime { get; private set; }

        private List<DivisionFile> _divisionFiles = new List<DivisionFile>();
        public IReadOnlyCollection<DivisionFile> DivisionFiles => _divisionFiles.AsReadOnly();
        public Division Division { get; private set; }
    }
}
