﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DEQP.Core.Domain.AdminsAggregate;
using DEQP.Core.Domain.NewsAggregate;
using DEQP.Core.Interfaces;
using DEQP.Core.Shared;

namespace DEQP.Core.Domain.DivisionAggregate
{
    public class Division: Entity,IAggregateRoot
    {
        public string DivisionName { get; private set; }
        public string DivisionNameAbbr { get; private set; }
        public string Preface { get; private set; }
        public string Description { get; private set; }
        public string Contact { get; set; }
        public bool IsActive { get; private set; }
        public int? CreatedBy { get; private set; }
        public int? ModifiedBy { get; private set; }
        public DateTime? CreatedDatetime { get; private set; }
        public DateTime? ModifiedDatetime { get; private set; }

        private List<News> _news = new List<News>();
        private List<SubDivision> _subDivisions = new List<SubDivision>();
        public IReadOnlyCollection<SubDivision> SubDivisions => _subDivisions.Where(m => m.IsActive == true).ToList().AsReadOnly();

        private List<DocumentGroup> _documentGroups = new List<DocumentGroup>();
        public IReadOnlyCollection<DocumentGroup> DocumentGroups => _documentGroups.AsReadOnly();

        private readonly Admin _createdBy;
    }
}
