﻿using DEQP.Core.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Core.Domain.DivisionAggregate
{
    public class DivisionOfficer:Entity
    {
        public string Name { get; private set; }
        public string Position { get; private set; }
        public string Type { get; private set; }
        public bool IsDirector { get; private set; }
        public string ImageRename { get; private set; }
        public int SubDivisionId { get; private set; }
        public string Number { get; private set; }
        public string IntNumber { get; private set; }
        public string Fax { get; private set; }
        public string Email { get; private set; }
        public bool IsActive { get; private set; }
        public int? CreatedBy { get; private set; }
        public int? ModifiedBy { get; private set; }
        public DateTime? CreatedDatetime { get; private set; }
        public DateTime? ModifiedDatetime { get; private set; }

        public SubDivision SubDivision { get; private set; }
    }
}
