﻿using DEQP.Core.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Core.Domain.DivisionAggregate
{
    public class SubDivision:Entity
    {
        public string Name { get; private set; }
        public int OrderNo { get; private set; }
        public bool IsActive { get; private set; }
        public int DivisionId { get; private set; }
        public int? CreatedBy { get; private set; }
        public int? ModifiedBy { get; private set; }
        public DateTime? CreatedDatetime { get; private set; }
        public DateTime? ModifiedDatetime { get; private set; }

        public Division Division { get; private set; }

        private List<DivisionOfficer> _divisionOfficers = new List<DivisionOfficer>();
        public IReadOnlyCollection<DivisionOfficer> DivisionOfficers => _divisionOfficers.Where(m => m.IsActive == true).ToList().AsReadOnly();


    }
}
