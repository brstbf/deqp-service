﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace DEQP.Core.Shared
{
    public static class ConvertDateToThaiExtendsion
    {
        static CultureInfo th = CultureInfo.CreateSpecificCulture("th-TH");
        public static string ToThai(this DateTime date)
        {
            return date.ToString("dd MMMM yyyy", th);
        }

        public static string ToThai(this DateTime date,string format)
        {
            return date.ToString(format, th);
        }

        public static string ToThai(this DateTime? date)
        {
            if (date == null)
                return string.Empty;
            return ToThai(date.Value);
        }

        public static string ToThai(this DateTime? date, string format)
        {
            if (date == null)
                return string.Empty;
            return ToThai(date.Value, format);
        }
    }
}
