﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Core.Shared
{
    public class StorageResponse
    {
        public Stream Stream { get; set; }
        public string MimeType { get; set; }
        public long Length { get; set; }
        public string Name { get; set; }
    }
}
