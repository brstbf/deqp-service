﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Core.Shared
{
    public sealed class StorageSettings
    {
        public string RootDrive { get; set; }
        public string ImageServerUrl { get; set; }
        public string FileServerUrl { get; set; }


        public string ProjectRootFolder => RootDrive + @"\project";
        public string ProjectCoverFolder => ProjectRootFolder + @"\cover";
        public string ProjectFileFolder => ProjectRootFolder + @"\file";
        public string ProjectCoverUrl => ImageServerUrl + "/Image/ProjectCover";
        public string ProjectFileUrl => FileServerUrl + "/File/ProjectFile";

        public string AdminDivisionFileUrl => FileServerUrl + "/File/DivisionFile";
        public string AdminDivisionImageUrl => ImageServerUrl + "/Image/DivisionCover";
        public string AdminDivisionOfficeFileUrl => FileServerUrl + "/File/OfficerFile";
        public string AdminDivisionOfficerImageUrl => ImageServerUrl + "/Image/OfficerCover";


        public string NewsRootFolder => RootDrive + @"\news";
        public string NewsCoverFolder => NewsRootFolder + @"\cover";
        public string NewsFileFolder => NewsRootFolder + @"\file";
        public string NewsFileUrl => FileServerUrl + "/File/NewsFile";
        public string NewsCoverUrl => ImageServerUrl + "/Image/NewsCover";

        public string DivisionRootFolder => RootDrive + @"\division";
        public string DivisionFileFolder => DivisionRootFolder + "@\file";
        public string DivisionFileUrl => FileServerUrl + "/File/DivisionFile";

        public string OfficerRootFolder => DivisionRootFolder + @"\officer";
        public string OfficerImageFolder => OfficerRootFolder + @"\profilePic";
        public string OfficerImageUrl => FileServerUrl + "/Image/OfficerCover";
    }
}
