﻿using DEQP.Core.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Core.Shared
{
    public class Entity
    {
        public int Id { get; protected set; }

        protected static void CheckNullOfEmptyString(string str,string name)
        {
            if (string.IsNullOrWhiteSpace(str))
                throw new DomainException($"Value cannot be empty, param: {name}");
        }

        protected static void CheckNull(object obj,string name)
        {
            if (obj == null)
                throw new DomainException($"Value cannot be null, param: {name}");

        }
    }
}
