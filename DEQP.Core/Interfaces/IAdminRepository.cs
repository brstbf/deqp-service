﻿using DEQP.Core.Domain.AdminsAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Core.Interfaces
{
    public interface IAdminRepository : IEFRepository<Admin>
    {
    }
}
