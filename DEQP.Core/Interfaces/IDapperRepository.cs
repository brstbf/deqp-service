﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace DEQP.Core.Interfaces
{
    public interface IDapperRepository
    {
        IEnumerable<T> Query<T>(string sql, object param = null);
        GridReader QueryMultiple(string sql, object param = null);
    }
}
