﻿using DEQP.Core.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Infrastructure.Interfaces
{
    public interface IStorageService
    {
        void Save(Stream st, string path);
        StorageResponse GetFile(string path);
    }
}
