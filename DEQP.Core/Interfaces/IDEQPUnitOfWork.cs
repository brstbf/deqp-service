﻿using DEQP.Core.Domain;
using DEQP.Core.Domain.AdminsAggregate;
using DEQP.Core.Domain.DivisionAggregate;
using DEQP.Core.Domain.NewsAggregate;
using DEQP.Core.Domain.ProjectsAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Core.Interfaces
{
    public interface IDEQPUnitOfWork : IUnitOfWork
    {
        IDeqpRepository<News> NewsRepository { get; }
        //IEFRepository<Admin> AdminRepository { get; }
        IDeqpRepository<NewsCategory> NewsCategoryRepository { get; }
        IDeqpRepository<Division> DivisionRepository { get; }
        IDeqpRepository<Project> ProjectRepository { get; }
    }
}
