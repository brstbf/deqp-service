﻿using DEQP.Core.Domain.NewsAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Core.Interfaces
{
    public interface IUnitOfWork
    {
        void SaveChange();
    }
}
