﻿using DEQP.Core.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Core.Interfaces
{
    public interface IEFRepository<TEntity> : IRepository<TEntity> where TEntity : Entity, IAggregateRoot
    {
        TEntity GetById(int id);
        IQueryable<TEntity> List(Expression<Func<TEntity, bool>> expression);
        TEntity First();
        TEntity First(Expression<Func<TEntity, bool>> expression);
    }
}
