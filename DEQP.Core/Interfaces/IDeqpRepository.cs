﻿using DEQP.Core.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Core.Interfaces
{
    public interface IDeqpRepository<TEntity>: IDapperRepository,IEFRepository<TEntity> where TEntity : Entity, IAggregateRoot
    {

    }
}
