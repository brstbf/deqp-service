﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Core.Entities
{
    public partial class TblConfigFunction
    {
        public TblConfigFunction()
        {
            TblAdminLogs = new HashSet<TblAdminLog>();
            TblAdminRoles = new HashSet<TblAdminRole>();
        }

        public string FuncCode { get; set; }
        public string FuncName { get; set; }
        public string FuncFlag { get; set; }
        public string LogFlag { get; set; }

        public virtual ICollection<TblAdminLog> TblAdminLogs { get; set; }
        public virtual ICollection<TblAdminRole> TblAdminRoles { get; set; }
    }
}
