﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Core.Entities
{
    public partial class TblNewsGroup
    {
        public TblNewsGroup()
        {
            TblNewsNewsGrpId1Navigations = new HashSet<TblNews>();
            TblNewsNewsGrpId2Navigations = new HashSet<TblNews>();
        }

        public int NewsGrpId { get; set; }
        public string NewsGrpName { get; set; }
        public string NewsTypeCode { get; set; }
        public int? OrderNo { get; set; }

        public virtual ICollection<TblNews> TblNewsNewsGrpId1Navigations { get; set; }
        public virtual ICollection<TblNews> TblNewsNewsGrpId2Navigations { get; set; }
    }
}
