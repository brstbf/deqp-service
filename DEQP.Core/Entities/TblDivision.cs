﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Core.Entities
{
    public partial class TblDivision
    {
        public TblDivision()
        {
            TblAdminProfiles = new HashSet<TblAdminProfile>();
            TblNews = new HashSet<TblNews>();
        }

        public int DiviId { get; set; }
        public string DiviName { get; set; }
        public string DiviNameAbbr { get; set; }
        public string Isactive { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? CreatedDatetime { get; set; }
        public DateTime? ModifiedDatetime { get; set; }

        public virtual TblAdminProfile CreatedByNavigation { get; set; }
        public virtual TblAdminProfile ModifiedByNavigation { get; set; }
        public virtual ICollection<TblAdminProfile> TblAdminProfiles { get; set; }
        public virtual ICollection<TblNews> TblNews { get; set; }
    }
}
