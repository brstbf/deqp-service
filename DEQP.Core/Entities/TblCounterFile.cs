﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Core.Entities
{
    public partial class TblCounterFile
    {
        public int CountFileId { get; set; }
        public int? NewsFileId { get; set; }
        public string CountTypeCode { get; set; }
        public string IpAddress { get; set; }
        public DateTime? CountDate { get; set; }

        public virtual TblNewsFile NewsFile { get; set; }
    }
}
