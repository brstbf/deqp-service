﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Core.Entities
{
    public partial class TblAdminProjectRole
    {
        public int RoleId { get; set; }
        public int? AdminId { get; set; }
        public int? ProjId { get; set; }

        public virtual TblAdminProfile Admin { get; set; }
        public virtual TblProject Proj { get; set; }
    }
}
