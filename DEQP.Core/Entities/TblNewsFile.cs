﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Core.Entities
{
    public partial class TblNewsFile
    {
        public TblNewsFile()
        {
            TblCounterFiles = new HashSet<TblCounterFile>();
        }

        public int NewsFileId { get; set; }
        public string FileOriName { get; set; }
        public string FileRename { get; set; }
        public string FileTypeName { get; set; }
        public double? FileSize { get; set; }
        public int? OrderNo { get; set; }
        public int? NewsId { get; set; }

        public virtual TblNews News { get; set; }
        public virtual ICollection<TblCounterFile> TblCounterFiles { get; set; }
    }
}
