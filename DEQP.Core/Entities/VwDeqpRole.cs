﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Core.Entities
{
    public partial class VwDeqpRole
    {
        public int ConSysId { get; set; }
        public long AdminId { get; set; }
    }
}
