﻿using DEQP.Core.Shared;
using DEQP.Infrastructure.Interfaces;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Infrastructure.Services
{
    public class FileStorageService : IFileStorageService
    {
        IStorageService storageService;
        StorageSettings storageSettings;
        public FileStorageService(IStorageService storageService, IOptions<StorageSettings> storageSettings)
        {
            this.storageService = storageService;
            this.storageSettings = storageSettings.Value;
        }

        public StorageResponse GetDivisionFile(string filename)
        {
            var path = Path.Combine(storageSettings.DivisionFileFolder, filename);
            var resp = storageService.GetFile(path);
            return resp;
        }

        public StorageResponse GetNewsFile(string filename)
        {
            var path = Path.Combine(storageSettings.NewsFileFolder, filename);
            var resp = storageService.GetFile(path);
            return resp;
        }

        public StorageResponse GetProjectFile(string filename)
        {
            var path = Path.Combine(storageSettings.ProjectFileFolder, filename);
            var resp = storageService.GetFile(path);
            return resp;
        }
    }
}
