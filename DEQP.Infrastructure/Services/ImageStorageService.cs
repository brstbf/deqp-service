﻿using DEQP.Core.Constants;
using DEQP.Core.Shared;
using DEQP.Infrastructure.Interfaces;
using DEQP.Infrastructure.Resources;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Infrastructure.Services
{
    public class ImageStorageService : IImageStorageService
    {
        IStorageService storageService;
        StorageSettings storageSettings;
        public ImageStorageService(IStorageService storageService,IOptions<StorageSettings> storageSettings)
        {
            this.storageService = storageService;
            this.storageSettings = storageSettings.Value;
        }

        public StorageResponse GetNewsCover(string filename)
        {
            var path = Path.Combine(storageSettings.NewsCoverFolder, filename);
            var resp = storageService.GetFile(path);
            return resp;
        }

        public StorageResponse GetOfficerImage(string filename)
        {
            var path = Path.Combine(storageSettings.OfficerImageFolder, filename);
            var resp = storageService.GetFile(path);
            return resp;
        }

        public StorageResponse GetProjectCover(string filename)
        {
            var path = Path.Combine(storageSettings.ProjectCoverFolder, filename);
            var resp = storageService.GetFile(path);
            return resp;
        }

        public string SaveProjectCover(SaveFileRequest request)
        {
            var mainExe = Path.GetExtension(request.Filename);
            var ranName = Path.GetRandomFileName();
            if (!string.IsNullOrWhiteSpace(mainExe))
                ranName += mainExe;

            var path = Path.Combine(storageSettings.ProjectCoverFolder, ranName);
            storageService.Save(request.Stream, path);

            return ranName;
        }
    }
}
