﻿using DEQP.Core.Exceptions;
using DEQP.Core.Shared;
using DEQP.Infrastructure.Interfaces;
using HeyRed.Mime;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Infrastructure.Services
{
    public class StorageService : IStorageService
    {
        ILogger<StorageService> logger;
        public StorageService(ILogger<StorageService> logger)
        {
            this.logger = logger;
        }
        public StorageResponse GetFile(string path)
        {
            var id = Guid.NewGuid().ToString();
            try
            {
                logger.LogInformation($"[Storage] [{id}] - Get File Path: {path}");

                if (string.IsNullOrWhiteSpace(path))
                    throw new StorageException("path cannot be empty.");

                var fileInfo = new FileInfo(path);
                if (!fileInfo.Exists)
                    throw new StorageException("This file not exist.");
                var res = new StorageResponse()
                {
                    Stream = fileInfo.OpenRead(),
                    Length = fileInfo.Length,
                    Name = fileInfo.Name,
                    MimeType = MimeTypesMap.GetMimeType(fileInfo.Name)
                };
                return res;
            }
            catch (Exception e)
            {
                logger.LogError($"[Storage] [{id}] - Exception Raise");
                logger.LogError($"[Storage] [{id}] - {e.ToString()}");
                throw;
            }
        }

        public void Save(Stream st, string path)
        {
            var id = Guid.NewGuid().ToString();
            try
            {
                logger.LogInformation($"[Storage] [{id}] - Request Save Path: {path}");

                if (st == null || st.Length == 0)
                    throw new StorageException("No Content to save.");

                if (string.IsNullOrWhiteSpace(path))
                    throw new StorageException("path cannot be empty.");

                var fileInfo = new FileInfo(path);
                if (fileInfo.Exists)
                    throw new StorageException("This file already exist.");

                st.Position = 0;
                using (var wt = new FileStream(path, FileMode.CreateNew, FileAccess.Write))
                {
                    st.CopyTo(wt);
                }


                logger.LogError($"[Storage] [{id}] - Save Success.");
            }
            catch (Exception e)
            {
                logger.LogError($"[Storage] [{id}] - Exception Raise");
                logger.LogError($"[Storage] [{id}] - {e.ToString()}");
                throw;
            }



        }
    }
}
