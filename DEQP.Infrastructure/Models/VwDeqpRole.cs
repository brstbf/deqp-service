﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Infrastructure.Models
{
    public partial class VwDeqpRole
    {
        public int ConSysId { get; set; }
        public long AdminId { get; set; }
    }
}
