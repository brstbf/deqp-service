﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Infrastructure.Models
{
    public partial class TblDivision
    {
        public TblDivision()
        {
            TblAdminProfiles = new HashSet<TblAdminProfile>();
            TblDivisionFiles = new HashSet<TblDivisionFile>();
            TblDivisionSubs = new HashSet<TblDivisionSub>();
            TblDocumentGroups = new HashSet<TblDocumentGroup>();
            TblNews = new HashSet<TblNews>();
        }

        public int DiviId { get; set; }
        public string DiviName { get; set; }
        public string DiviNameAbbr { get; set; }
        public string DiviPreface { get; set; }
        public string DiviDesc { get; set; }
        public string DiviContact { get; set; }
        public string Isactive { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? CreatedDatetime { get; set; }
        public DateTime? ModifiedDatetime { get; set; }

        public virtual TblAdminProfile CreatedByNavigation { get; set; }
        public virtual TblAdminProfile ModifiedByNavigation { get; set; }
        public virtual ICollection<TblAdminProfile> TblAdminProfiles { get; set; }
        public virtual ICollection<TblDivisionFile> TblDivisionFiles { get; set; }
        public virtual ICollection<TblDivisionSub> TblDivisionSubs { get; set; }
        public virtual ICollection<TblDocumentGroup> TblDocumentGroups { get; set; }
        public virtual ICollection<TblNews> TblNews { get; set; }
    }
}
