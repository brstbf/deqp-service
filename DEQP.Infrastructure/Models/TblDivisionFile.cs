﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Infrastructure.Models
{
    public partial class TblDivisionFile
    {
        public int DiviFileId { get; set; }
        public string FileOriName { get; set; }
        public string FileRename { get; set; }
        public string FileTypeName { get; set; }
        public double? FileSize { get; set; }
        public int? OrderNo { get; set; }
        public int? DocGrpId { get; set; }
        public int? DiviId { get; set; }

        public virtual TblDivision Divi { get; set; }
        public virtual TblDocumentGroup DocGrp { get; set; }
    }
}
