﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Infrastructure.Models
{
    public partial class TblCounterNews
    {
        public int CountNewsId { get; set; }
        public int? NewsId { get; set; }
        public string IpAddress { get; set; }
        public DateTime? CountDate { get; set; }

        public virtual TblNews News { get; set; }
    }
}
