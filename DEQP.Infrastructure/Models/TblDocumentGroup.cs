﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Infrastructure.Models
{
    public partial class TblDocumentGroup
    {
        public TblDocumentGroup()
        {
            TblDivisionFiles = new HashSet<TblDivisionFile>();
        }

        public int DocGrpId { get; set; }
        public string DocGrpName { get; set; }
        public int? DiviId { get; set; }
        public int? OrderNo { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual TblAdminProfile CreatedByNavigation { get; set; }
        public virtual TblDivision Divi { get; set; }
        public virtual TblAdminProfile ModifiedByNavigation { get; set; }
        public virtual ICollection<TblDivisionFile> TblDivisionFiles { get; set; }
    }
}
