﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Infrastructure.Models
{
    public partial class Xdata01
    {
        public int? NewsId { get; set; }
        public string NewsTitle { get; set; }
        public string CoverFileRename { get; set; }
        public string NewsPreface { get; set; }
        public string NewsContent { get; set; }
        public DateTime? NewsDate { get; set; }
        public string NewsTags { get; set; }
        public int? NewsGrpId1 { get; set; }
        public int? NewsGrpId2 { get; set; }
        public int? FiscalYear { get; set; }
        public string TopNewsFlag { get; set; }
        public string Isactive { get; set; }
        public int? DiviId { get; set; }
        public int? ProjId { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string DeptName { get; set; }
        public string NewsGrpName { get; set; }
        public string NewsBy { get; set; }
        public string File01 { get; set; }
        public string File02 { get; set; }
        public string File03 { get; set; }
    }
}
