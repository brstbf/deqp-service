﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Infrastructure.Models
{
    public partial class TblNews
    {
        public TblNews()
        {
            TblCounterNews = new HashSet<TblCounterNews>();
            TblNewsFiles = new HashSet<TblNewsFile>();
        }

        public int NewsId { get; set; }
        public string NewsTitle { get; set; }
        public string CoverFileRename { get; set; }
        public string NewsPreface { get; set; }
        public string NewsContent { get; set; }
        public DateTime? NewsDate { get; set; }
        public string NewsTags { get; set; }
        public int? NewsGrpId1 { get; set; }
        public int? NewsGrpId2 { get; set; }
        public string FiscalYear { get; set; }
        public string TopNewsFlag { get; set; }
        public string Isactive { get; set; }
        public int? DiviId { get; set; }
        public int? ProjId { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual TblAdminProfile CreatedByNavigation { get; set; }
        public virtual TblDivision Divi { get; set; }
        public virtual TblAdminProfile ModifiedByNavigation { get; set; }
        public virtual TblNewsGroup NewsGrpId1Navigation { get; set; }
        public virtual TblNewsGroup NewsGrpId2Navigation { get; set; }
        public virtual ICollection<TblCounterNews> TblCounterNews { get; set; }
        public virtual ICollection<TblNewsFile> TblNewsFiles { get; set; }
    }
}
