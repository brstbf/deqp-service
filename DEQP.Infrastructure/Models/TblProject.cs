﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Infrastructure.Models
{
    public partial class TblProject
    {
        public TblProject()
        {
            TblAdminProjectRoles = new HashSet<TblAdminProjectRole>();
            TblProjectFiles = new HashSet<TblProjectFile>();
        }

        public int ProjId { get; set; }
        public string ProjName { get; set; }
        public string CoverFileRename { get; set; }
        public string ProjContent { get; set; }
        public string ProjUrl { get; set; }
        public string RegisUrl { get; set; }
        public int? IsactiveProjUrl { get; set; }
        public int? IsactiveRegisUrl { get; set; }
        public int? Isactive { get; set; }
        public int? OrderNo { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<TblAdminProjectRole> TblAdminProjectRoles { get; set; }
        public virtual ICollection<TblProjectFile> TblProjectFiles { get; set; }
    }
}
