﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Infrastructure.Models
{
    public partial class TblAdminLog
    {
        public int LogId { get; set; }
        public string FuncCode { get; set; }
        public string LogDesc { get; set; }
        public int? AdminId { get; set; }
        public string LogAdminFullname { get; set; }
        public DateTime? LogDatetime { get; set; }

        public virtual TblAdminProfile Admin { get; set; }
        public virtual TblConfigFunction FuncCodeNavigation { get; set; }
    }
}
