﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Infrastructure.Models
{
    public partial class TblAdminRole
    {
        public int RoleId { get; set; }
        public int? AdminId { get; set; }
        public string FuncCode { get; set; }

        public virtual TblAdminProfile Admin { get; set; }
        public virtual TblConfigFunction FuncCodeNavigation { get; set; }
    }
}
