﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Infrastructure.Models
{
    public partial class TblDivisionOfficer
    {
        public int OfficerId { get; set; }
        public string OfficerFullName { get; set; }
        public string OfficerPosition { get; set; }
        public string OfficerType { get; set; }
        public string DirectorFlag { get; set; }
        public string ImageRename { get; set; }
        public int? DiviSubId { get; set; }
        public string OfficerNumber { get; set; }
        public string OfficerIntNumber { get; set; }
        public string OfficerFax { get; set; }
        public string OfficerEmail { get; set; }
        public string Isactive { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual TblAdminProfile CreatedByNavigation { get; set; }
        public virtual TblDivisionSub DiviSub { get; set; }
        public virtual TblAdminProfile ModifiedByNavigation { get; set; }
    }
}
