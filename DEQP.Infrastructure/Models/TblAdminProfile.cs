﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Infrastructure.Models
{
    public partial class TblAdminProfile
    {
        public TblAdminProfile()
        {
            TblAdminLogs = new HashSet<TblAdminLog>();
            TblAdminProjectRoles = new HashSet<TblAdminProjectRole>();
            TblAdminRoles = new HashSet<TblAdminRole>();
            TblDivisionCreatedByNavigations = new HashSet<TblDivision>();
            TblDivisionModifiedByNavigations = new HashSet<TblDivision>();
            TblDivisionOfficerCreatedByNavigations = new HashSet<TblDivisionOfficer>();
            TblDivisionOfficerModifiedByNavigations = new HashSet<TblDivisionOfficer>();
            TblDocumentGroupCreatedByNavigations = new HashSet<TblDocumentGroup>();
            TblDocumentGroupModifiedByNavigations = new HashSet<TblDocumentGroup>();
            TblNewsCreatedByNavigations = new HashSet<TblNews>();
            TblNewsModifiedByNavigations = new HashSet<TblNews>();
        }

        public int AdminId { get; set; }
        public int? AdminRefId { get; set; }
        public int? DivisId { get; set; }
        public string DivisRoleType { get; set; }
        public string ProjRoleType { get; set; }
        public string Isactive { get; set; }
        public int? ApprovedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual TblDivision Divis { get; set; }
        public virtual ICollection<TblAdminLog> TblAdminLogs { get; set; }
        public virtual ICollection<TblAdminProjectRole> TblAdminProjectRoles { get; set; }
        public virtual ICollection<TblAdminRole> TblAdminRoles { get; set; }
        public virtual ICollection<TblDivision> TblDivisionCreatedByNavigations { get; set; }
        public virtual ICollection<TblDivision> TblDivisionModifiedByNavigations { get; set; }
        public virtual ICollection<TblDivisionOfficer> TblDivisionOfficerCreatedByNavigations { get; set; }
        public virtual ICollection<TblDivisionOfficer> TblDivisionOfficerModifiedByNavigations { get; set; }
        public virtual ICollection<TblDocumentGroup> TblDocumentGroupCreatedByNavigations { get; set; }
        public virtual ICollection<TblDocumentGroup> TblDocumentGroupModifiedByNavigations { get; set; }
        public virtual ICollection<TblNews> TblNewsCreatedByNavigations { get; set; }
        public virtual ICollection<TblNews> TblNewsModifiedByNavigations { get; set; }
    }
}
