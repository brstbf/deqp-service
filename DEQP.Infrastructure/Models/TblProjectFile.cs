﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Infrastructure.Models
{
    public partial class TblProjectFile
    {
        public int ProjFileId { get; set; }
        public string FileOriName { get; set; }
        public string FileRename { get; set; }
        public string FileTypeName { get; set; }
        public double? FileSize { get; set; }
        public int? OrderNo { get; set; }
        public int? ProjId { get; set; }

        public virtual TblProject Proj { get; set; }
    }
}
