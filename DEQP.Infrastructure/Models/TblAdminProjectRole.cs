﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Infrastructure.Models
{
    public partial class TblAdminProjectRole
    {
        public int RoleId { get; set; }
        public int? AdminId { get; set; }
        public int? ProjId { get; set; }

        public virtual TblAdminProfile Admin { get; set; }
        public virtual TblProject Proj { get; set; }
    }
}
