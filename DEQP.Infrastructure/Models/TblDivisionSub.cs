﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DEQP.Infrastructure.Models
{
    public partial class TblDivisionSub
    {
        public TblDivisionSub()
        {
            TblDivisionOfficers = new HashSet<TblDivisionOfficer>();
        }

        public int DiviSubId { get; set; }
        public string DiviSubName { get; set; }
        public int? OrderNo { get; set; }
        public string Isactive { get; set; }
        public int? DiviId { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual TblDivision Divi { get; set; }
        public virtual ICollection<TblDivisionOfficer> TblDivisionOfficers { get; set; }
    }
}
