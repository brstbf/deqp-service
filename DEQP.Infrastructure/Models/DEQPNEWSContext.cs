﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace DEQP.Infrastructure.Models
{
    public partial class DEQPNEWSContext : DbContext
    {
        public DEQPNEWSContext()
        {
        }

        public DEQPNEWSContext(DbContextOptions<DEQPNEWSContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TblAdminLog> TblAdminLogs { get; set; }
        public virtual DbSet<TblAdminProfile> TblAdminProfiles { get; set; }
        public virtual DbSet<TblAdminProjectRole> TblAdminProjectRoles { get; set; }
        public virtual DbSet<TblAdminRole> TblAdminRoles { get; set; }
        public virtual DbSet<TblConfigFunction> TblConfigFunctions { get; set; }
        public virtual DbSet<TblCounterFile> TblCounterFiles { get; set; }
        public virtual DbSet<TblCounterNews> TblCounterNews { get; set; }
        public virtual DbSet<TblDivision> TblDivisions { get; set; }
        public virtual DbSet<TblDivisionFile> TblDivisionFiles { get; set; }
        public virtual DbSet<TblDivisionOfficer> TblDivisionOfficers { get; set; }
        public virtual DbSet<TblDivisionSub> TblDivisionSubs { get; set; }
        public virtual DbSet<TblDocumentGroup> TblDocumentGroups { get; set; }
        public virtual DbSet<TblNews> TblNews { get; set; }
        public virtual DbSet<TblNewsFile> TblNewsFiles { get; set; }
        public virtual DbSet<TblNewsGroup> TblNewsGroups { get; set; }
        public virtual DbSet<TblProject> TblProjects { get; set; }
        public virtual DbSet<TblProjectFile> TblProjectFiles { get; set; }
        public virtual DbSet<VwDeqpProfile> VwDeqpProfiles { get; set; }
        public virtual DbSet<VwDeqpRole> VwDeqpRoles { get; set; }
        public virtual DbSet<Xdata01> Xdata01s { get; set; }
        public virtual DbSet<Xdata02> Xdata02s { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=159.138.102.154;Initial Catalog=DEQPNEWS;User Id=deqpnews;Password=P@ssW0rd@news;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Thai_CI_AS");

            modelBuilder.Entity<TblAdminLog>(entity =>
            {
                entity.HasKey(e => e.LogId);

                entity.ToTable("tbl_admin_log");

                entity.Property(e => e.LogId).HasColumnName("log_id");

                entity.Property(e => e.AdminId).HasColumnName("admin_id");

                entity.Property(e => e.FuncCode)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("func_code");

                entity.Property(e => e.LogAdminFullname)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("log_admin_fullname");

                entity.Property(e => e.LogDatetime)
                    .HasColumnType("datetime")
                    .HasColumnName("log_datetime");

                entity.Property(e => e.LogDesc)
                    .IsUnicode(false)
                    .HasColumnName("log_desc");

                entity.HasOne(d => d.Admin)
                    .WithMany(p => p.TblAdminLogs)
                    .HasForeignKey(d => d.AdminId)
                    .HasConstraintName("FK_tbl_admin_log_tbl_admin_profile");

                entity.HasOne(d => d.FuncCodeNavigation)
                    .WithMany(p => p.TblAdminLogs)
                    .HasForeignKey(d => d.FuncCode)
                    .HasConstraintName("FK_tbl_admin_log_tbl_admin_log");
            });

            modelBuilder.Entity<TblAdminProfile>(entity =>
            {
                entity.HasKey(e => e.AdminId);

                entity.ToTable("tbl_admin_profile");

                entity.Property(e => e.AdminId).HasColumnName("admin_id");

                entity.Property(e => e.AdminRefId).HasColumnName("admin_ref_id");

                entity.Property(e => e.ApprovedBy).HasColumnName("approved_by");

                entity.Property(e => e.ApprovedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("approved_date");

                entity.Property(e => e.DivisId).HasColumnName("divis_id");

                entity.Property(e => e.DivisRoleType)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("divis_role_type");

                entity.Property(e => e.Isactive)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("isactive");

                entity.Property(e => e.ModifiedBy).HasColumnName("modified_by");

                entity.Property(e => e.ModifiedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("modified_date");

                entity.Property(e => e.ProjRoleType)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("proj_role_type");

                entity.HasOne(d => d.Divis)
                    .WithMany(p => p.TblAdminProfiles)
                    .HasForeignKey(d => d.DivisId)
                    .HasConstraintName("FK_tbl_admin_profile_tbl_division");
            });

            modelBuilder.Entity<TblAdminProjectRole>(entity =>
            {
                entity.HasKey(e => e.RoleId);

                entity.ToTable("tbl_admin_project_role");

                entity.Property(e => e.RoleId).HasColumnName("role_id");

                entity.Property(e => e.AdminId).HasColumnName("admin_id");

                entity.Property(e => e.ProjId).HasColumnName("proj_id");

                entity.HasOne(d => d.Admin)
                    .WithMany(p => p.TblAdminProjectRoles)
                    .HasForeignKey(d => d.AdminId)
                    .HasConstraintName("FK_tbl_admin_project_role_tbl_admin_profile");

                entity.HasOne(d => d.Proj)
                    .WithMany(p => p.TblAdminProjectRoles)
                    .HasForeignKey(d => d.ProjId)
                    .HasConstraintName("FK_tbl_admin_project_role_tbl_project");
            });

            modelBuilder.Entity<TblAdminRole>(entity =>
            {
                entity.HasKey(e => e.RoleId);

                entity.ToTable("tbl_admin_role");

                entity.Property(e => e.RoleId).HasColumnName("role_id");

                entity.Property(e => e.AdminId).HasColumnName("admin_id");

                entity.Property(e => e.FuncCode)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("func_code");

                entity.HasOne(d => d.Admin)
                    .WithMany(p => p.TblAdminRoles)
                    .HasForeignKey(d => d.AdminId)
                    .HasConstraintName("FK_tbl_admin_role_tbl_admin_profile");

                entity.HasOne(d => d.FuncCodeNavigation)
                    .WithMany(p => p.TblAdminRoles)
                    .HasForeignKey(d => d.FuncCode)
                    .HasConstraintName("FK_tbl_admin_role_tbl_config_function");
            });

            modelBuilder.Entity<TblConfigFunction>(entity =>
            {
                entity.HasKey(e => e.FuncCode);

                entity.ToTable("tbl_config_function");

                entity.Property(e => e.FuncCode)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("func_code");

                entity.Property(e => e.FuncFlag)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("func_flag");

                entity.Property(e => e.FuncName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("func_name");

                entity.Property(e => e.LogFlag)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("log_flag");
            });

            modelBuilder.Entity<TblCounterFile>(entity =>
            {
                entity.HasKey(e => e.CountFileId);

                entity.ToTable("tbl_counter_file");

                entity.Property(e => e.CountFileId).HasColumnName("count_file_id");

                entity.Property(e => e.CountDate)
                    .HasColumnType("datetime")
                    .HasColumnName("count_date");

                entity.Property(e => e.CountTypeCode)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("count_type_code");

                entity.Property(e => e.IpAddress)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("ip_address");

                entity.Property(e => e.NewsFileId).HasColumnName("news_file_id");

                entity.HasOne(d => d.NewsFile)
                    .WithMany(p => p.TblCounterFiles)
                    .HasForeignKey(d => d.NewsFileId)
                    .HasConstraintName("FK_tbl_counter_file_tbl_news_file");
            });

            modelBuilder.Entity<TblCounterNews>(entity =>
            {
                entity.HasKey(e => e.CountNewsId);

                entity.ToTable("tbl_counter_news");

                entity.Property(e => e.CountNewsId).HasColumnName("count_news_id");

                entity.Property(e => e.CountDate)
                    .HasColumnType("datetime")
                    .HasColumnName("count_date");

                entity.Property(e => e.IpAddress)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("ip_address");

                entity.Property(e => e.NewsId).HasColumnName("news_id");

                entity.HasOne(d => d.News)
                    .WithMany(p => p.TblCounterNews)
                    .HasForeignKey(d => d.NewsId)
                    .HasConstraintName("FK_tbl_counter_news_tbl_news");
            });

            modelBuilder.Entity<TblDivision>(entity =>
            {
                entity.HasKey(e => e.DiviId);

                entity.ToTable("tbl_division");

                entity.Property(e => e.DiviId).HasColumnName("divi_id");

                entity.Property(e => e.CreatedBy).HasColumnName("created_by");

                entity.Property(e => e.CreatedDatetime)
                    .HasColumnType("datetime")
                    .HasColumnName("created_datetime");

                entity.Property(e => e.DiviContact)
                    .IsUnicode(false)
                    .HasColumnName("divi_contact");

                entity.Property(e => e.DiviDesc)
                    .IsUnicode(false)
                    .HasColumnName("divi_desc");

                entity.Property(e => e.DiviName)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("divi_name");

                entity.Property(e => e.DiviNameAbbr)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("divi_name_abbr");

                entity.Property(e => e.DiviPreface)
                    .IsUnicode(false)
                    .HasColumnName("divi_preface");

                entity.Property(e => e.Isactive)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("isactive");

                entity.Property(e => e.ModifiedBy).HasColumnName("modified_by");

                entity.Property(e => e.ModifiedDatetime)
                    .HasColumnType("datetime")
                    .HasColumnName("modified_datetime");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.TblDivisionCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_tbl_division_tbl_admin_profile");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.TblDivisionModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_tbl_division_tbl_admin_profile1");
            });

            modelBuilder.Entity<TblDivisionFile>(entity =>
            {
                entity.HasKey(e => e.DiviFileId);

                entity.ToTable("tbl_division_file");

                entity.Property(e => e.DiviFileId).HasColumnName("divi_file_id");

                entity.Property(e => e.DiviId).HasColumnName("divi_id");

                entity.Property(e => e.DocGrpId).HasColumnName("doc_grp_id");

                entity.Property(e => e.FileOriName)
                    .IsUnicode(false)
                    .HasColumnName("file_ori_name");

                entity.Property(e => e.FileRename)
                    .IsUnicode(false)
                    .HasColumnName("file_rename");

                entity.Property(e => e.FileSize).HasColumnName("file_size");

                entity.Property(e => e.FileTypeName)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasColumnName("file_type_name");

                entity.Property(e => e.OrderNo).HasColumnName("order_no");

                entity.HasOne(d => d.Divi)
                    .WithMany(p => p.TblDivisionFiles)
                    .HasForeignKey(d => d.DiviId)
                    .HasConstraintName("FK_tbl_division_file_tbl_division");

                entity.HasOne(d => d.DocGrp)
                    .WithMany(p => p.TblDivisionFiles)
                    .HasForeignKey(d => d.DocGrpId)
                    .HasConstraintName("FK_tbl_division_file_tbl_document_group");
            });

            modelBuilder.Entity<TblDivisionOfficer>(entity =>
            {
                entity.HasKey(e => e.OfficerId);

                entity.ToTable("tbl_division_officer");

                entity.Property(e => e.OfficerId).HasColumnName("officer_id");

                entity.Property(e => e.CreatedBy).HasColumnName("created_by");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("created_date");

                entity.Property(e => e.DirectorFlag)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("director_flag");

                entity.Property(e => e.DiviSubId).HasColumnName("divi_sub_id");

                entity.Property(e => e.ImageRename)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("image_rename");

                entity.Property(e => e.Isactive)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("isactive");

                entity.Property(e => e.ModifiedBy).HasColumnName("modified_by");

                entity.Property(e => e.ModifiedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("modified_date");

                entity.Property(e => e.OfficerEmail)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("officer_email");

                entity.Property(e => e.OfficerFax)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("officer_fax");

                entity.Property(e => e.OfficerFullName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("officer_full_name");

                entity.Property(e => e.OfficerIntNumber)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("officer_int_number");

                entity.Property(e => e.OfficerNumber)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("officer_number");

                entity.Property(e => e.OfficerPosition)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("officer_position");

                entity.Property(e => e.OfficerType)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("officer_type");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.TblDivisionOfficerCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_tbl_division_officer_tbl_admin_profile");

                entity.HasOne(d => d.DiviSub)
                    .WithMany(p => p.TblDivisionOfficers)
                    .HasForeignKey(d => d.DiviSubId)
                    .HasConstraintName("FK_tbl_division_officer_tbl_division_sub");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.TblDivisionOfficerModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_tbl_division_officer_tbl_admin_profile1");
            });

            modelBuilder.Entity<TblDivisionSub>(entity =>
            {
                entity.HasKey(e => e.DiviSubId);

                entity.ToTable("tbl_division_sub");

                entity.Property(e => e.DiviSubId).HasColumnName("divi_sub_id");

                entity.Property(e => e.CreatedBy).HasColumnName("created_by");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("created_date");

                entity.Property(e => e.DiviId).HasColumnName("divi_id");

                entity.Property(e => e.DiviSubName)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("divi_sub_name");

                entity.Property(e => e.Isactive)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("isactive");

                entity.Property(e => e.ModifiedBy).HasColumnName("modified_by");

                entity.Property(e => e.ModifiedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("modified_date");

                entity.Property(e => e.OrderNo).HasColumnName("order_no");

                entity.HasOne(d => d.Divi)
                    .WithMany(p => p.TblDivisionSubs)
                    .HasForeignKey(d => d.DiviId)
                    .HasConstraintName("FK_tbl_division_sub_tbl_division");
            });

            modelBuilder.Entity<TblDocumentGroup>(entity =>
            {
                entity.HasKey(e => e.DocGrpId);

                entity.ToTable("tbl_document_group");

                entity.Property(e => e.DocGrpId).HasColumnName("doc_grp_id");

                entity.Property(e => e.CreatedBy).HasColumnName("created_by");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("created_date");

                entity.Property(e => e.DiviId).HasColumnName("divi_id");

                entity.Property(e => e.DocGrpName)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("doc_grp_name");

                entity.Property(e => e.ModifiedBy).HasColumnName("modified_by");

                entity.Property(e => e.ModifiedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("modified_date");

                entity.Property(e => e.OrderNo).HasColumnName("order_no");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.TblDocumentGroupCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_tbl_document_group_tbl_admin_profile");

                entity.HasOne(d => d.Divi)
                    .WithMany(p => p.TblDocumentGroups)
                    .HasForeignKey(d => d.DiviId)
                    .HasConstraintName("FK_tbl_document_group_tbl_division");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.TblDocumentGroupModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_tbl_document_group_tbl_admin_profile1");
            });

            modelBuilder.Entity<TblNews>(entity =>
            {
                entity.HasKey(e => e.NewsId);

                entity.ToTable("tbl_news");

                entity.Property(e => e.NewsId).HasColumnName("news_id");

                entity.Property(e => e.CoverFileRename)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("cover_file_rename");

                entity.Property(e => e.CreatedBy).HasColumnName("created_by");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("created_date");

                entity.Property(e => e.DiviId).HasColumnName("divi_id");

                entity.Property(e => e.FiscalYear)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasColumnName("fiscal_year");

                entity.Property(e => e.Isactive)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("isactive");

                entity.Property(e => e.ModifiedBy).HasColumnName("modified_by");

                entity.Property(e => e.ModifiedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("modified_date");

                entity.Property(e => e.NewsContent)
                    .IsUnicode(false)
                    .HasColumnName("news_content");

                entity.Property(e => e.NewsDate)
                    .HasColumnType("datetime")
                    .HasColumnName("news_date");

                entity.Property(e => e.NewsGrpId1).HasColumnName("news_grp_id1");

                entity.Property(e => e.NewsGrpId2).HasColumnName("news_grp_id2");

                entity.Property(e => e.NewsPreface)
                    .IsUnicode(false)
                    .HasColumnName("news_preface");

                entity.Property(e => e.NewsTags)
                    .IsUnicode(false)
                    .HasColumnName("news_tags");

                entity.Property(e => e.NewsTitle)
                    .IsUnicode(false)
                    .HasColumnName("news_title");

                entity.Property(e => e.ProjId).HasColumnName("proj_id");

                entity.Property(e => e.TopNewsFlag)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("top_news_flag");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.TblNewsCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_tbl_news_tbl_news_created_by");

                entity.HasOne(d => d.Divi)
                    .WithMany(p => p.TblNews)
                    .HasForeignKey(d => d.DiviId)
                    .HasConstraintName("FK_tbl_news_tbl_division");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.TblNewsModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_tbl_news_tbl_news_modified");

                entity.HasOne(d => d.NewsGrpId1Navigation)
                    .WithMany(p => p.TblNewsNewsGrpId1Navigations)
                    .HasForeignKey(d => d.NewsGrpId1)
                    .HasConstraintName("FK_tbl_news_tbl_news_group");

                entity.HasOne(d => d.NewsGrpId2Navigation)
                    .WithMany(p => p.TblNewsNewsGrpId2Navigations)
                    .HasForeignKey(d => d.NewsGrpId2)
                    .HasConstraintName("FK_tbl_news_tbl_news_group1");
            });

            modelBuilder.Entity<TblNewsFile>(entity =>
            {
                entity.HasKey(e => e.NewsFileId);

                entity.ToTable("tbl_news_file");

                entity.Property(e => e.NewsFileId).HasColumnName("news_file_id");

                entity.Property(e => e.FileOriName)
                    .IsUnicode(false)
                    .HasColumnName("file_ori_name");

                entity.Property(e => e.FileRename)
                    .IsUnicode(false)
                    .HasColumnName("file_rename");

                entity.Property(e => e.FileSize).HasColumnName("file_size");

                entity.Property(e => e.FileTypeName)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasColumnName("file_type_name");

                entity.Property(e => e.NewsId).HasColumnName("news_id");

                entity.Property(e => e.OrderNo).HasColumnName("order_no");

                entity.HasOne(d => d.News)
                    .WithMany(p => p.TblNewsFiles)
                    .HasForeignKey(d => d.NewsId)
                    .HasConstraintName("FK_tbl_news_file_tbl_news");
            });

            modelBuilder.Entity<TblNewsGroup>(entity =>
            {
                entity.HasKey(e => e.NewsGrpId);

                entity.ToTable("tbl_news_group");

                entity.Property(e => e.NewsGrpId)
                    .ValueGeneratedNever()
                    .HasColumnName("news_grp_id");

                entity.Property(e => e.NewsGrpName)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("news_grp_name");

                entity.Property(e => e.NewsTypeCode)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("news_type_code");

                entity.Property(e => e.OrderNo).HasColumnName("order_no");
            });

            modelBuilder.Entity<TblProject>(entity =>
            {
                entity.HasKey(e => e.ProjId);

                entity.ToTable("tbl_project");

                entity.Property(e => e.ProjId).HasColumnName("proj_id");

                entity.Property(e => e.CoverFileRename)
                    .IsUnicode(false)
                    .HasColumnName("cover_file_rename");

                entity.Property(e => e.CreatedBy).HasColumnName("created_by");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("created_date");

                entity.Property(e => e.Isactive).HasColumnName("isactive");

                entity.Property(e => e.IsactiveProjUrl).HasColumnName("isactive_proj_url");

                entity.Property(e => e.IsactiveRegisUrl).HasColumnName("isactive_regis_url");

                entity.Property(e => e.ModifiedBy).HasColumnName("modified_by");

                entity.Property(e => e.ModifiedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("modified_date");

                entity.Property(e => e.OrderNo).HasColumnName("order_no");

                entity.Property(e => e.ProjContent)
                    .IsUnicode(false)
                    .HasColumnName("proj_content");

                entity.Property(e => e.ProjName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("proj_name");

                entity.Property(e => e.ProjUrl)
                    .IsUnicode(false)
                    .HasColumnName("proj_url");

                entity.Property(e => e.RegisUrl)
                    .IsUnicode(false)
                    .HasColumnName("regis_url");
            });

            modelBuilder.Entity<TblProjectFile>(entity =>
            {
                entity.HasKey(e => e.ProjFileId);

                entity.ToTable("tbl_project_file");

                entity.Property(e => e.ProjFileId).HasColumnName("proj_file_id");

                entity.Property(e => e.FileOriName)
                    .IsUnicode(false)
                    .HasColumnName("file_ori_name");

                entity.Property(e => e.FileRename)
                    .IsUnicode(false)
                    .HasColumnName("file_rename");

                entity.Property(e => e.FileSize).HasColumnName("file_size");

                entity.Property(e => e.FileTypeName)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasColumnName("file_type_name");

                entity.Property(e => e.OrderNo).HasColumnName("order_no");

                entity.Property(e => e.ProjId).HasColumnName("proj_id");

                entity.HasOne(d => d.Proj)
                    .WithMany(p => p.TblProjectFiles)
                    .HasForeignKey(d => d.ProjId)
                    .HasConstraintName("FK_tbl_project_file_tbl_project");
            });

            modelBuilder.Entity<VwDeqpProfile>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_deqp_profile");

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("firstname");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.Lastname)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("lastname");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("username");
            });

            modelBuilder.Entity<VwDeqpRole>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_deqp_role");

                entity.Property(e => e.AdminId).HasColumnName("admin_id");

                entity.Property(e => e.ConSysId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("con_sys_id");
            });

            modelBuilder.Entity<Xdata01>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("xdata01");

                entity.Property(e => e.CoverFileRename)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("cover_file_rename");

                entity.Property(e => e.CreatedBy).HasColumnName("created_by");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("created_date");

                entity.Property(e => e.DeptName)
                    .HasMaxLength(255)
                    .HasColumnName("dept_name");

                entity.Property(e => e.DiviId).HasColumnName("divi_id");

                entity.Property(e => e.File01)
                    .HasMaxLength(255)
                    .HasColumnName("file01");

                entity.Property(e => e.File02)
                    .HasMaxLength(255)
                    .HasColumnName("file02");

                entity.Property(e => e.File03)
                    .HasMaxLength(255)
                    .HasColumnName("file03");

                entity.Property(e => e.FiscalYear).HasColumnName("fiscal_year");

                entity.Property(e => e.Isactive)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("isactive");

                entity.Property(e => e.ModifiedBy).HasColumnName("modified_by");

                entity.Property(e => e.ModifiedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("modified_date");

                entity.Property(e => e.NewsBy)
                    .HasMaxLength(255)
                    .HasColumnName("news_by");

                entity.Property(e => e.NewsContent)
                    .IsUnicode(false)
                    .HasColumnName("news_content");

                entity.Property(e => e.NewsDate)
                    .HasColumnType("datetime")
                    .HasColumnName("news_date");

                entity.Property(e => e.NewsGrpId1).HasColumnName("news_grp_id1");

                entity.Property(e => e.NewsGrpId2).HasColumnName("news_grp_id2");

                entity.Property(e => e.NewsGrpName)
                    .HasMaxLength(255)
                    .HasColumnName("news_grp_name");

                entity.Property(e => e.NewsId).HasColumnName("news_id");

                entity.Property(e => e.NewsPreface)
                    .IsUnicode(false)
                    .HasColumnName("news_preface");

                entity.Property(e => e.NewsTags)
                    .IsUnicode(false)
                    .HasColumnName("news_tags");

                entity.Property(e => e.NewsTitle).HasColumnName("news_title");

                entity.Property(e => e.ProjId).HasColumnName("proj_id");

                entity.Property(e => e.TopNewsFlag)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("top_news_flag");
            });

            modelBuilder.Entity<Xdata02>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("xdata02");

                entity.Property(e => e.CoverFileRename)
                    .HasMaxLength(100)
                    .HasColumnName("cover_file_rename");

                entity.Property(e => e.CreatedBy).HasColumnName("created_by");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("created_date");

                entity.Property(e => e.DeptName)
                    .HasMaxLength(255)
                    .HasColumnName("dept_name");

                entity.Property(e => e.DiviId).HasColumnName("divi_id");

                entity.Property(e => e.File01)
                    .HasMaxLength(255)
                    .HasColumnName("file01");

                entity.Property(e => e.Fille02)
                    .HasMaxLength(255)
                    .HasColumnName("fille02");

                entity.Property(e => e.FiscalYear).HasColumnName("fiscal_year");

                entity.Property(e => e.Isactive)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("isactive");

                entity.Property(e => e.ModifiedBy).HasColumnName("modified_by");

                entity.Property(e => e.ModifiedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("modified_date");

                entity.Property(e => e.NewsBy)
                    .HasMaxLength(255)
                    .HasColumnName("news_by");

                entity.Property(e => e.NewsContent).HasColumnName("news_content");

                entity.Property(e => e.NewsDate)
                    .HasColumnType("datetime")
                    .HasColumnName("news_date");

                entity.Property(e => e.NewsGrpId1).HasColumnName("news_grp_id1");

                entity.Property(e => e.NewsGrpId2).HasColumnName("news_grp_id2");

                entity.Property(e => e.NewsGrpName)
                    .HasMaxLength(255)
                    .HasColumnName("news_grp_name");

                entity.Property(e => e.NewsId).HasColumnName("news_id");

                entity.Property(e => e.NewsPreface).HasColumnName("news_preface");

                entity.Property(e => e.NewsTags).HasColumnName("news_tags");

                entity.Property(e => e.NewsTitle)
                    .IsUnicode(false)
                    .HasColumnName("news_title");

                entity.Property(e => e.ProjId).HasColumnName("proj_id");

                entity.Property(e => e.TopNewsFlag)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("top_news_flag");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
