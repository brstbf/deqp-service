﻿using DEQP.Core.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Infrastructure.Interfaces
{
    public interface IFileStorageService
    {
        StorageResponse GetNewsFile(string filename);
        StorageResponse GetProjectFile(string filename);
        StorageResponse GetDivisionFile(string filename);
    }
}
