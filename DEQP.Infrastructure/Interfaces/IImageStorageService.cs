﻿using DEQP.Core.Shared;
using DEQP.Infrastructure.Resources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Infrastructure.Interfaces
{
    public interface IImageStorageService
    {
        string SaveProjectCover(SaveFileRequest request);
        StorageResponse GetProjectCover(string filename);
        StorageResponse GetNewsCover(string filename);
        StorageResponse GetOfficerImage(string filename);
    }
}
