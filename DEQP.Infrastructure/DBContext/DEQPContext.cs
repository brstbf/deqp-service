﻿using DEQP.Core.Domain;
using DEQP.Core.Domain.AdminsAggregate;
using DEQP.Core.Domain.DivisionAggregate;
using DEQP.Core.Domain.NewsAggregate;
using DEQP.Core.Domain.ProjectsAggregate;
using DEQP.Infrastructure.DBContext.DEQPNews.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Infrastructure.DBContext
{
    public class DEQPContext: DbContext
    {
        public DEQPContext()
        {

        }
        public DEQPContext(DbContextOptions<DEQPContext> options)
            : base(options)
        {

        }

        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<NewsCategory> NewsCategories { get; set; }
        public virtual DbSet<Admin> Admins { get; set; }
        public virtual DbSet<Division> Divisions { get; set; }

        public virtual DbSet<Project> Projects { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Console.WriteLine);
            if(!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=119.59.123.116;Initial Catalog=DEQPNEWS;User Id=deqpnews;Password=P@ssW0rd@news;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.ApplyConfiguration(new NewsEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new AdminEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new DivisionEntityTypConfiguration());

            modelBuilder.ApplyConfiguration(new NewsCategoryEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new ProjectEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new NewsViewCounterEntityTypeConfiguration());

        }

    }
}
