﻿using DEQP.Core.Domain;
using DEQP.Core.Domain.AdminsAggregate;
using DEQP.Core.Domain.DivisionAggregate;
using DEQP.Core.Domain.NewsAggregate;
using DEQP.Core.Domain.ProjectsAggregate;
using DEQP.Core.Interfaces;
using DEQP.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Infrastructure.DBContext
{
    public class DEQPUnitOfWork : IDEQPUnitOfWork
    {
        DEQPContext db;
        ILogger<DEQPUnitOfWork> logger;
        IDeqpRepository<News> newsRepository;
        IDeqpRepository<NewsCategory> newsCategoryRepository;
        IDeqpRepository<Project> projectRepository;
        IDeqpRepository<Division> divisionRepository;

        public DEQPUnitOfWork(DEQPContext db
            , ILogger<DEQPUnitOfWork> logger
            , IDeqpRepository<News> newsRepository
            , IDeqpRepository<NewsCategory> newsCategoryRepository
            , IDeqpRepository<Project> projectRepository
            , IDeqpRepository<Division> divisionRepository)
        {
            this.db = db;
            this.logger = logger;

            this.newsCategoryRepository = newsCategoryRepository;
            this.newsRepository = newsRepository;
            this.projectRepository = projectRepository;
            this.divisionRepository = divisionRepository;
        }

        public IDeqpRepository<News> NewsRepository => newsRepository;

        public IDeqpRepository<NewsCategory> NewsCategoryRepository => newsCategoryRepository;

        public IDeqpRepository<Project> ProjectRepository => projectRepository;
        public IDeqpRepository<Division> DivisionRepository => divisionRepository;

        public void SaveChange()
        {
            try
            {
                logger.LogInformation("Try Save");
                db.SaveChanges();
                logger.LogInformation("Save Success");
            }
            catch (Exception e)
            {
                logger.LogError("Error when Savechange");
                logger.LogError(e.ToString());
                throw;
            }

        }
    }
}
