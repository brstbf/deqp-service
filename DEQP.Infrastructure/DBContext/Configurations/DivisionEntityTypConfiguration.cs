﻿using DEQP.Core.Domain.DivisionAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Infrastructure.DBContext.DEQPNews.Configurations
{
    internal sealed class DivisionEntityTypConfiguration : IEntityTypeConfiguration<Division>
    {
        public void Configure(EntityTypeBuilder<Division> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Ignore(e => e.DocumentGroups);
            builder.Ignore(e => e.SubDivisions);

            builder.ToTable("tbl_division");

            builder.Property(e => e.Id).HasColumnName("divi_id");

            builder.Property(e => e.CreatedBy).HasColumnName("created_by");

            builder.Property(e => e.CreatedDatetime)
                .HasColumnType("datetime")
                .HasColumnName("created_datetime");

            builder.Property(e => e.DivisionName)
                .HasMaxLength(250)
                .IsUnicode(false)
                .HasColumnName("divi_name");

            builder.Property(e => e.DivisionNameAbbr)
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasColumnName("divi_name_abbr");

            builder.Property(e => e.Preface)
                .IsUnicode(false)
                .HasColumnName("divi_preface");

            builder.Property(e => e.Description)
                .IsUnicode(false)
                .HasColumnName("divi_desc");

            builder.Property(e => e.Contact)
                .IsUnicode(false)
                .HasColumnName("divi_contact");


            builder.Property(e => e.IsActive)
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasColumnName("isactive")
                .HasConversion(new BoolToStringConverter("0", "1"));

            builder.Property(e => e.ModifiedBy).HasColumnName("modified_by");

            builder.Property(e => e.ModifiedDatetime)
                .HasColumnType("datetime")
                .HasColumnName("modified_datetime");



            builder.OwnsMany<SubDivision>("_subDivisions", e =>
             {
                 e.WithOwner(e=>e.Division)
                    .HasForeignKey(e=>e.DivisionId);

                 e.ToTable("tbl_division_sub")
                    .Ignore(o=>o.DivisionOfficers)
                    .HasKey(o => o.Id);

                 e.Property(o => o.Id)
                    .HasColumnName("divi_sub_id")
                    .IsRequired();

                 e.Property(o => o.Name)
                    .HasColumnName("divi_sub_name")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                 e.Property(o => o.OrderNo)
                    .HasColumnName("order_no");

                 e.Property(o => o.IsActive)
                    .HasColumnName("isactive")
                    .HasConversion(new BoolToStringConverter("0", "1"));

                 e.Property(o => o.DivisionId)
                    .HasColumnName("divi_id");

                 e.Property(o => o.ModifiedBy)
                    .HasColumnName("modified_by");

                 e.Property(o => o.CreatedBy)
                    .HasColumnName("created_by");

                 e.Property(o => o.ModifiedDatetime)
                    .HasColumnName("modified_date");

                 e.Property(o => o.CreatedDatetime)
                    .HasColumnName("created_date");

                 e.OwnsMany<DivisionOfficer>("_divisionOfficers", o =>
                 {
                     o.WithOwner(e=>e.SubDivision)
                        .HasForeignKey(e => e.SubDivisionId);

                     o.ToTable("tbl_division_officer")
                        .HasKey(e => e.Id);

                     o.Property(e => e.Id)
                        .HasColumnName("officer_id")
                        .IsRequired();

                     o.Property(e => e.Name)
                        .HasColumnName("officer_full_name")
                        .HasMaxLength(100)
                        .IsUnicode(false);

                     o.Property(e => e.Position)
                        .HasColumnName("officer_position")
                        .HasMaxLength(100)
                        .IsUnicode(false);

                     o.Property(e => e.Type)
                        .HasColumnName("officer_type")
                        .HasMaxLength(2)
                        .IsUnicode(false);

                     o.Property(e => e.IsDirector)
                        .HasColumnName("director_flag")
                        .HasConversion(new BoolToStringConverter("0", "1"));

                     o.Property(e => e.ImageRename)
                        .HasColumnName("image_rename")
                        .HasMaxLength(100)
                        .IsUnicode(false);

                     o.Property(e => e.SubDivisionId)
                        .HasColumnName("divi_sub_id");

                     o.Property(e => e.Number)
                        .HasColumnName("officer_number")
                        .HasMaxLength(30)
                        .IsUnicode(false);

                     o.Property(e => e.IntNumber)
                        .HasColumnName("officer_int_number")
                        .HasMaxLength(30)
                        .IsUnicode(false);

                     o.Property(e => e.Fax)
                        .HasColumnName("officer_fax")
                        .HasMaxLength(30)
                        .IsUnicode(false);

                     o.Property(e => e.Email)
                        .HasColumnName("officer_email")
                        .HasMaxLength(30)
                        .IsUnicode(false);

                     o.Property(e => e.IsActive)
                        .HasColumnName("isactive")
                        .HasConversion(new BoolToStringConverter("0", "1"));

                     o.Property(e => e.CreatedBy)
                        .HasColumnName("created_by");

                     o.Property(e => e.ModifiedBy)
                        .HasColumnName("modified_by");

                     o.Property(e => e.CreatedBy)
                        .HasColumnName("created_by");

                     o.Property(e => e.CreatedDatetime)
                        .HasColumnName("created_date");

                     o.Property(e => e.ModifiedDatetime)
                        .HasColumnName("modified_date");


                 });

             });

            builder.OwnsMany<DocumentGroup>("_documentGroups", o =>
            {
                o.WithOwner(e => e.Division)
                   .HasForeignKey(e => e.DivisionId);

                o.ToTable("tbl_document_group")
                   .Ignore(e => e.DivisionFiles)
                   .HasKey(e => e.Id);

                o.Property(e => e.Id)
                    .HasColumnName("doc_grp_id");

                o.Property(e => e.Name)
                   .HasColumnName("doc_grp_name")
                   .HasMaxLength(50)
                   .IsUnicode(false);

                o.Property(e => e.DivisionId)
                   .HasColumnName("divi_id");

                o.Property(e => e.OrderNo)
                   .HasColumnName("order_no");

                o.Property(e => e.ModifiedBy)
                   .HasColumnName("modified_by");

                o.Property(e => e.CreatedBy)
                   .HasColumnName("created_by");

                o.Property(e => e.CreatedDatetime)
                   .HasColumnName("created_date");

                o.Property(e => e.ModifiedDatetime)
                   .HasColumnName("modified_date");

                o.OwnsMany<DivisionFile>("_divisionFiles", o =>
                {
                    o.WithOwner(e => e.DocumentGroup)
                        .HasForeignKey(e => e.DocumentGroupId);

                    o.ToTable("tbl_division_file")
                        .HasKey(e => e.Id);

                    o.Property(e => e.Id)
                        .HasColumnName("divi_file_id")
                        .IsRequired();

                    o.Property(e => e.OriginalName)
                        .HasColumnName("file_ori_name")
                        .IsUnicode(false);

                    o.Property(e => e.Rename)
                        .HasColumnName("file_rename")
                        .IsUnicode(false);

                    o.Property(e => e.Type)
                        .HasColumnName("file_type_name")
                        .HasMaxLength(4)
                        .IsUnicode(false);

                    o.Property(e => e.Size)
                        .HasColumnName("file_size");

                    o.Property(e => e.OrderNo)
                        .HasColumnName("order_no");

                    o.Property(e => e.DocumentGroupId)
                        .HasColumnName("doc_grp_id");

                });
            });

        }
    }
}
