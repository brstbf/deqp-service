﻿using DEQP.Core.Domain.NewsAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Infrastructure.DBContext.DEQPNews.Configurations
{
    internal sealed class NewsEntityTypeConfiguration : IEntityTypeConfiguration<News>
    {
        public void Configure(EntityTypeBuilder<News> builder)
        {
            builder.HasKey(e => e.Id);

            builder.ToTable("tbl_news");

            builder.Property(e => e.Id).HasColumnName("news_id");

            builder.Property(e => e.CoverFileRename)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasColumnName("cover_file_rename");

            builder.Property(e => e.CreatedById).HasColumnName("created_by");

            builder.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");

            builder.Property(e => e.DivisionId).HasColumnName("divi_id");

            builder.Property(e => e.FiscalYear)
                .HasMaxLength(4)
                .IsUnicode(false)
                .HasColumnName("fiscal_year");

            builder.Property(e => e.IsActive)
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasColumnName("isactive")
                .HasConversion(new BoolToStringConverter("0", "1"));

            builder.Property(e => e.ModifiedById).HasColumnName("modified_by");

            builder.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");

            builder.Property(e => e.Content)
                .IsUnicode(false)
                .HasColumnName("news_content");

            builder.Property(e => e.Date)
                .HasColumnType("datetime")
                .HasColumnName("news_date");

            builder.Property(e => e.PrimaryGroupId).HasColumnName("news_grp_id1");

            builder.Property(e => e.SecondaryGroupId).HasColumnName("news_grp_id2");

            builder.Property(e => e.Preface)
                .IsUnicode(false)
                .HasColumnName("news_preface");

            builder.Property(e => e.Tags)
                .IsUnicode(false)
                .HasColumnName("news_tags");

            builder.Property(e => e.Title)
                .HasMaxLength(300)
                .IsUnicode(false)
                .HasColumnName("news_title");

            builder.Property(e => e.ProjectId).HasColumnName("proj_id");

            builder.Property(e => e.TopNewsFlag)
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasColumnName("top_news_flag")
                .HasConversion(new BoolToStringConverter("0", "1"));

            //--------------------- Navigation
            #region Navigation

            builder.HasOne("_division")
                 .WithMany("_news")
                 .HasForeignKey("DivisionId");

            builder.HasOne("_primaryNewsCategory")
                .WithMany("_primaryNews")
                .HasForeignKey("PrimaryGroupId");

            builder.HasOne("_secondaryNewsCategory")
                .WithMany("_secondaryNews")
                .HasForeignKey("SecondaryGroupId");

            builder.Ignore(e => e.Files);

            builder.OwnsMany<NewsFile>("_files", e =>
            {
                e.WithOwner()
                    .HasForeignKey(o => o.NewsId);

                e.HasKey(e => e.Id);

                e.ToTable("tbl_news_file");

                e.Property(e => e.Id).HasColumnName("news_file_id");

                e.Property(e => e.OriginalName)
                    .IsUnicode(false)
                    .HasColumnName("file_ori_name");

                e.Property(e => e.Rename)
                    .IsUnicode(false)
                    .HasColumnName("file_rename");

                e.Property(e => e.FileSize).HasColumnName("file_size");

                e.Property(e => e.TypeName)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasColumnName("file_type_name");

                e.Property(e => e.NewsId).HasColumnName("news_id");

                e.Property(e => e.OrderNo).HasColumnName("order_no");

                //----------------------------Navigation

                e.OwnsMany<FileCounter>("_fileCounters", o =>
                {
                    o.WithOwner()
                        .HasForeignKey(e => e.FileId);

                    o.HasKey(e => e.Id);

                    o.ToTable("tbl_counter_file");

                    o.Property(e => e.Id).HasColumnName("count_file_id");

                    o.Property(e => e.FileId).HasColumnName("news_file_id");

                    o.Property(e => e.CountDate)
                        .HasColumnType("datetime")
                        .HasColumnName("count_date");

                    o.Property(e => e.TypeCode)
                        .HasConversion(v => ((int)v).ToString(), v => (FileTpye)Enum.Parse(typeof(FileTpye), v))
                        .HasMaxLength(2)
                        .IsUnicode(false)
                        .HasColumnName("count_type_code");

                    o.Property(e => e.IpAddress)
                        .HasMaxLength(20)
                        .IsUnicode(false)
                        .HasColumnName("ip_address");
                });
            });

            #region Admin
            builder.HasOne("_createdBy")
                .WithMany()
                .HasForeignKey("CreatedById");

            builder.HasOne("_modifiedBy")
                .WithMany()
                .HasForeignKey("ModifiedById");
            #endregion

            #region Project

            builder.HasOne("_project")
                .WithMany("_news")
                .HasForeignKey("ProjectId");

            #endregion

            #endregion
        }
    }
}
