﻿using DEQP.Core.Domain.AdminsAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Infrastructure.DBContext.DEQPNews.Configurations
{
    public class AdminEntityTypeConfiguration : IEntityTypeConfiguration<Admin>
    {
        public void Configure(EntityTypeBuilder<Admin> builder)
        {
            builder.HasKey(e => e.Id);

            builder.ToTable("tbl_admin_profile");

            builder.Property(e => e.Id).HasColumnName("admin_id");

            builder.Property(e => e.AdminRefId).HasColumnName("admin_ref_id");

            builder.Property(e => e.ApprovedBy).HasColumnName("approved_by");

            builder.Property(e => e.ApprovedDate)
                .HasColumnType("datetime")
                .HasColumnName("approved_date");

            builder.Property(e => e.IsActive)
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasColumnName("isactive")
                .HasConversion(new BoolToStringConverter("0", "1"));

            builder.Property(e => e.ModifiedBy).HasColumnName("modified_by");

            builder.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");

            builder.Property(e => e.DivisionId)
                .HasColumnName("divis_id");

            builder.Property(e=>e.ProjectPermission)
                .HasColumnName("proj_role_type")
                .HasConversion(v => ((int)v).ToString(), v => (ProjectRoleType)Enum.Parse(typeof(ProjectRoleType), v))
                .HasMaxLength(2)
                .IsUnicode(false);

            builder.Property(e => e.DivisionPermission)
                .HasColumnName("divis_role_type")
                .HasConversion(v => ((int)v).ToString(), v => (DivisionRoleType)Enum.Parse(typeof(DivisionRoleType), v))
                .HasMaxLength(2)
                .IsUnicode(false);

            //----------------------------Navigation

            builder.HasOne("_division")
                .WithMany()
                .HasForeignKey("DivisionId")
                .HasConstraintName("FK_tbl_admin_profile_tbl_division");
        }
    }
}
