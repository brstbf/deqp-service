﻿using DEQP.Core.Domain.ProjectsAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Infrastructure.DBContext.DEQPNews.Configurations
{
    public class ProjectEntityTypeConfiguration : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.HasKey(e => e.Id);

            builder.ToTable("tbl_project");

            builder.Property(e => e.Id).HasColumnName("proj_id");

            builder.Property(e => e.CoverFileRename)
                .IsUnicode(false)
                .HasColumnName("cover_file_rename");

            builder.Property(e => e.CreatedById).HasColumnName("created_by");

            builder.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");

            builder.Property(e => e.Isactive)
                .HasColumnName("isactive")
                .HasConversion<int>();

            builder.Property(e => e.IsactiveProjectUrl)
                .HasColumnName("isactive_proj_url")
                .HasConversion<int>();

            builder.Property(e => e.IsactiveRegisterUrl)
                .HasColumnName("isactive_regis_url")
                .HasConversion<int>();

            builder.Property(e => e.ModifiedById).HasColumnName("modified_by");

            builder.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");

            builder.Property(e => e.OrderNo).HasColumnName("order_no");

            builder.Property(e => e.Content)
                .IsUnicode(false)
                .HasColumnName("proj_content");

            builder.Property(e => e.Name)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasColumnName("proj_name");

            builder.Property(e => e.ProjectUrl)
                .IsUnicode(false)
                .HasColumnName("proj_url");

            builder.Property(e => e.RegisterUrl)
                .IsUnicode(false)
                .HasColumnName("regis_url");

            builder.OwnsMany<ProjectFile>("_files", o =>
            {
                o.WithOwner()
                    .HasForeignKey(o=>o.ProjectId)
                    .HasConstraintName("FK_tbl_project_file_tbl_project"); ;

                o.HasKey(e => e.Id);

                o.ToTable("tbl_project_file");

                o.Property(e => e.Id).HasColumnName("proj_file_id");

                o.Property(e => e.OriginalName)
                    .IsUnicode(false)
                    .HasColumnName("file_ori_name");

                o.Property(e => e.Rename)
                    .IsUnicode(false)
                    .HasColumnName("file_rename");

                o.Property(e => e.FileSize).HasColumnName("file_size");

                o.Property(e => e.TypeName)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasColumnName("file_type_name");

                o.Property(e => e.OrderNo).HasColumnName("order_no");

                o.Property(e => e.ProjectId).HasColumnName("proj_id");
            });

            builder.HasOne("_creator")
                .WithMany()
                .HasForeignKey("CreatedById");

            builder.HasOne("_modifier")
                .WithMany()
                .HasForeignKey("ModifiedById");

        }
    }
}
