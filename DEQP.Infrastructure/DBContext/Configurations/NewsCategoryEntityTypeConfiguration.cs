﻿using DEQP.Core.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Infrastructure.DBContext.DEQPNews.Configurations
{
    public class NewsCategoryEntityTypeConfiguration : IEntityTypeConfiguration<NewsCategory>
    {
        public void Configure(EntityTypeBuilder<NewsCategory> builder)
        {
            builder.HasKey(e => e.Id);

            builder.ToTable("tbl_news_group");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasColumnName("news_grp_id");

            builder.Property(e => e.CategoryName)
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasColumnName("news_grp_name");

            builder.Property(e => e.Type)
                .HasConversion(v => ((int)v).ToString(), v => (NewsCategoryType)Enum.Parse(typeof(NewsCategoryType), v))
                .HasMaxLength(2)
                .IsUnicode(false)
                .HasColumnName("news_type_code");

            builder.Property(e => e.OrderNo).HasColumnName("order_no");            
        }
    }
}
