﻿using DEQP.Core.Domain.NewsAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Infrastructure.DBContext.DEQPNews.Configurations
{
    public class NewsViewCounterEntityTypeConfiguration : IEntityTypeConfiguration<NewsViewCounter>
    {
        public void Configure(EntityTypeBuilder<NewsViewCounter> builder)
        {
            builder.HasKey(e => e.Id);

            builder.ToTable("tbl_counter_news");

            builder.Property(e => e.Id).HasColumnName("count_news_id");

            builder.Property(e => e.CountDate)
                .HasColumnType("datetime")
                .HasColumnName("count_date");

            builder.Property(e => e.IpAddress)
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasColumnName("ip_address");

            builder.Property(e => e.NewsId).HasColumnName("news_id");

            builder.HasOne(typeof(News))
                .WithMany("_viewCounters")
                .HasForeignKey("NewsId");
        }
    }
}
