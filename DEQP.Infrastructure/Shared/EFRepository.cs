﻿using Dapper;
using DEQP.Core.Interfaces;
using DEQP.Core.Shared;
using DEQP.Infrastructure.DBContext.DEQPNews;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Infrastructure.Shared
{
    public class EFRepository<TEntity> : IEFRepository<TEntity> where TEntity : Entity, IAggregateRoot
    {
        DbContext db;
        protected EFRepository(DbContext db)
        {
            this.db = db;
        }
        public void Add(TEntity entity)
        {
            db.Entry(entity).State = EntityState.Added;
        }

        public void Update(TEntity entity)
        {
            db.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(TEntity entity)
        {
            db.Entry(entity).State = EntityState.Deleted;
        }

        public ICollection<TEntity> All()
        {
            return db.Set<TEntity>().ToList();
        }

        public TEntity First()
        {
            return db.Set<TEntity>().FirstOrDefault();
        }

        public TEntity First(Expression<Func<TEntity, bool>> expression)
        {
            return db.Set<TEntity>().FirstOrDefault(expression);
        }

        public TEntity GetById(int id)
        {
            return db.Set<TEntity>().FirstOrDefault(f => f.Id == id);
        }

        public IQueryable<TEntity> List(Expression<Func<TEntity, bool>> expression)
        {
            return db.Set<TEntity>().Where(expression);
        }
    }
}
