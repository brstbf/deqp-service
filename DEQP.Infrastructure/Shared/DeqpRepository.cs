﻿using Dapper;
using DEQP.Core.Interfaces;
using DEQP.Core.Shared;
using DEQP.Infrastructure.DBContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace DEQP.Infrastructure.Shared
{
    public class DeqpRepository<TEntity> : EFRepository<TEntity>, IDeqpRepository<TEntity> where TEntity : Entity, IAggregateRoot
    {
        DEQPContext db;
        ILogger<TEntity> logger;
        public DeqpRepository(DEQPContext db, ILogger<TEntity> logger) : base(db)
        {
            this.logger = logger;
            this.db = db;
        }

        public IEnumerable<T> Query<T>(string sql, object param = null)
        {
            logger.LogInformation($"Query {sql}");
            logger.LogInformation($"Param {param}");

            var list =  db.Database.GetDbConnection().Query<T>(sql, param);

            logger.LogInformation($"Query Success with {list.Count()} rows");

            return list;
        }

        public GridReader QueryMultiple(string sql, object param = null)
        {
            logger.LogInformation($"Query {sql}");
            logger.LogInformation($"Param {param}");

            var multi =  db.Database.GetDbConnection().QueryMultiple(sql, param);
            logger.LogInformation("Query Success");

            return multi;
        }
    }
}
