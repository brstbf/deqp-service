﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQP.Infrastructure.Resources
{
    public class SaveFileRequest
    {
        public string Filename { get; set; }
        public Stream Stream { get; set; }
    }
}
